/**
 * This file contains the application's colors.
 *
 * Define color here instead of duplicating them throughout the components.
 * That allows to change them more easily later on.
 */

export default {
  main: '#28A0F9',
  transparent: 'rgba(0,0,0,0)',
  white: '#ffffff',
  red: '#FF6B6B',
  black: '#000000',
  silver: '#898A8D',
  silver_lighter: '#c0c0c0',
  text: '#212529',
  primary: '#007bff',
  success: '#5CC668',
  warn: '#FFAC30',
  error: '#dc3545',
  error1: '#FF6B6B',
  bluegrey: '#607d8b',
};
