import request from '../Helpers/Request';

export const branchCalendar = () => {
    return request({
        url: '/calendar',
        method: 'GET',
    });
};

export const busyTimes = () => {
    return request({
        url: '/busytime_api',
        method: 'GET',
    })
}

export const addBusyTimes = (data: any) => {
    return request({
        url: '/busytime_api',
        method: 'POST',
        data,
    })
}

export const myCalendar = () => {
    return request({
        url: '/shift_user_api',
        method: 'GET',
    });
}

export const calendarNextWeek = (data: any) => {
    return request({
        url: '/find_shift_user_api',
        method: 'POST',
        data,
    })
}