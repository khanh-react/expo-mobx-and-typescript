import request from '../Helpers/Request';

export const fetchShifts = () => {
    return request({
        url: '/shift_branch_api',
        method: 'GET',
    });
};

export const fetchLackShifts = () => {
    return request({
        url: '/lack_shift_branch_api',
        method: 'GET',
    });
};

export const fetchMyShifts = () => {
    return request({
        url: '/shift_user_api',
        method: 'GET',
    });
};

export const checkIn = (data: { ip_address: string }) => {
    return request({
        url: '/get_ip',
        method: 'POST',
        data,
    });
}

export const checkOut = (data: {}) => {
    return request({
        url: '/save_timekeeper',
        method: 'POST',
        data,
    })
}

export const remind = () => {
    return request({
        url: '/shift_remind_work_api',
        method: 'GET',
    })
}

export const switches = () => {
    return request({
        url: '/pending_notification_api',
        method: 'GET'
    })
}

export const find = (data: any) => {
    return request({
        url: '/find_shift_user_api',
        method: 'POST',
        data,
    })
}

// export const fetchBusyTimes = () => {
//     return request({
//         url: '/busytime_api',
//         method: 'GET',
//     })
// }

// export const addBusyTimes = (data) => {
//     return request({
//         url: '/busytime_api',
//         method: 'POST',
//         data,
//     })
// }