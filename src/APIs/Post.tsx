import request from '../Helpers/Request';

export const fetchPosts = () => {
    return request({
        url: '/get_list_post_api',
        method: 'GET',
        params: {
            type: 0,
            page: 0,
            limit: 10,
        }
    });
};


export const fetchPost = (id: string) => {
    return request({
        url: '/detail_post_api',
        method: 'GET',
        params: { id: 10 },
    });
};
