import axios from 'axios';
import request from '../Helpers/Request';

export const checkInput = (input: string) => {
    return request({
        url: '/checkLogin',
        method: 'POST',
        data: { email: input },
    });
};


export const login = (email: string, password: string) => {
    return request({
        url: '/login',
        method: 'POST',
        data: { email, password },
    });
};


export const updateAvatar = async (link: any) => {
    return request({
        url: '/upload',
        method: 'POST',
        data: { link },
    });
};
