import React from 'react';
import { createStackNavigator, CardStyleInterpolators } from '@react-navigation/stack';
import ProfileScreen from '../Containers/ProfileScreen';
import HomeScreen from '../Containers/HomeScreen';
import CalendarScreen from '../Containers/CalendarScreen';
import SpeechTest from '../Containers/SpeechTest';
import PostScreen from '../Containers/PostScreen';
import DetailShiftScreen from '../Containers/DetailShiftScreen';
import NotificationScreen from '../Containers/NotificationScreen';

const Stack = createStackNavigator<never>();

export const ContainerNavigator = () =>
  <Stack.Navigator
    screenOptions={{
      headerShown: false,
      cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
    }}>
      <Stack.Screen name="Home" component={HomeScreen} />
      <Stack.Screen name="Calendar" component={CalendarScreen} />
      <Stack.Screen name="Speech" component={SpeechTest} />
      <Stack.Screen name="Post" component={PostScreen} />
      <Stack.Screen name="DetailShift" component={DetailShiftScreen} />
      <Stack.Screen name="Notification" component={NotificationScreen} />
    </Stack.Navigator>;

export default ContainerNavigator;
