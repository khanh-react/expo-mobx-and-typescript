import React, { useState, useEffect } from 'react';
import { View, Text } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { createStackNavigator, CardStyleInterpolators } from '@react-navigation/stack';
import InitScreen from '../Containers/InitScreen';
import SignUpScreen from '../Containers/Auth/SignUpScreen';
import SignInScreen from '../Containers/Auth/SignInScreen';
import SignInByPinScreen from '../Containers/Auth/SignInByPinScreen';
import SignInAfterScreen from '../Containers/Container';
import ProfileScreen from '../Containers/ProfileScreen';
import SignInSuccessScreen from '../Containers/Auth/SignInSuccess';
import HomeScreen from '../Containers/HomeScreen';

/*
 * The root screen contains the application's navigation.
 *
 * @see https://reactnavigation.org/docs/en/hello-react-navigation.html#creating-a-stack-navigator
 */

const Stack = createStackNavigator<never>();

export const Navigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
      }}>
      <Stack.Screen name="Init" component={InitScreen} />
      <Stack.Screen name="SignIn" component={SignInScreen} />
      <Stack.Screen name="SignInSuccess" component={SignInSuccessScreen} />
      {/* <Stack.Screen name="SignInByPin" component={SignInByPinScreen} /> */}
      <Stack.Screen name="SignInAfter" component={SignInAfterScreen}/>
      <Stack.Screen name="Profile" component={ProfileScreen}/>
    </Stack.Navigator>
  );
}

export default Navigator;
