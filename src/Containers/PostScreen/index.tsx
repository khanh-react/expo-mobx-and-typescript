import React from 'react';
import { View, ScrollView } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { Styles } from './styles';
import { PostItem } from './PostItem';
import { Icon } from '../../Components/Icon';
import { IText as Text } from '../../Components/Text';
import { useStores } from '../../Helpers/UseStores';

export const PostScreen = () => {
    const { posts, currentPost } = useStores(store => ({
      posts: store.post.posts,
      currentPost: store.post.currentPost,
    } as any));
    const BackIcon = require('../../Assets/back_black.png');
    const navigation = useNavigation();
    return (
        <View style={Styles.wrapper}>
            <View style={Styles.head}>
                <Icon source={BackIcon} onPress={() => navigation.navigate('Home')}/>
                <Text style={Styles.mrl10} size="medium" type="bold" value="Bảng tin"/>
            </View>
            <ScrollView style={Styles.body}>
                { posts.map((post: any) => <PostItem key={post.id} {...post}/>) }
                {/* { <PostAds /> } */}
            </ScrollView>
        </View>
    );
};

export default PostScreen;
