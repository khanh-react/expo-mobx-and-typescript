import { StyleSheet } from 'react-native';
import Helpers from '../../Theme/Helpers';

export const Styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        backgroundColor: '#FFF',
    },
    head: {
        ...Helpers.rowCross,
        width: '100%',
        height: 35,
        padding: 10,
    },
    body: {
        padding: 10,
        marginBottom: 80,
        backgroundColor: '#FFF',
    },
    mrl10: {
        marginLeft: 10,
    }
});

export default Styles;
