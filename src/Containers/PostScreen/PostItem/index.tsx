import React from 'react';
import { View, Image } from 'react-native';
import { Styles } from './styles';
import { PostItemProps as Props } from './types';
import { IText as Text } from '../../../Components/Text';

export const PostItem = ({ title, create_name, thumb, content, day_feel }: Props) => {
    return (
        <View style={Styles.wrapper}>
            <View style={Styles.head}>
                <Text size="xsmall" type="bold" value={title} />
            </View>
            <View style={Styles.body}>
                { thumb && <Image style={Styles.postImage} source={thumb as any}/> }
                <Text value={content}/>
            </View>
            <View style={Styles.foot}>
                <Text>{create_name}<Text size="smaller" value={` đã đăng ${day_feel}`}/></Text>
            </View>
        </View>
    )
};

export default PostItem;
