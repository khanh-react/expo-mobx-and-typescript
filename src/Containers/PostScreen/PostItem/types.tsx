export type PostItemProps = {
    create_name: string;
    title: string;
    content: string;
    day_feel: string;
    thumb?: string;
};
