import { StyleSheet, Dimensions } from 'react-native';
import Helpers from '../../../Theme/Helpers';

const width = Dimensions.get('screen').width;

export const Styles = StyleSheet.create({
    wrapper: {
        ...Helpers.shadow,
        width: '100%',
        borderColor: '#e0e0e0',
        borderRadius: 10,
        borderWidth: 1,
        padding: 5,
        marginBottom: 10,
        backgroundColor: '#FFF',
    },
    head: {
        width: '100%',
        marginBottom: 10,
    },
    body: {
        // ...Helpers.colCross,
    },
    foot: {
        marginTop: 10,
    },
    postImage: {
        width: width - 40,
    }
});

export default Styles;
