import React from 'react';
import { View, Image } from 'react-native';
import { Styles } from './styles';
import { IText as Text } from '../../../Components/Text';

export const PostAds = () => {
    const title = "The Coffe House tuyển dụng cuối năm 2020!"
    const thumb = require('../../../Assets/mock/coffehouse_ads.jpg');
    const content = "Nam phục vụ : 5 \r\nNữ phục vụ : 10 \r\nHình thức: Fulltime hoặc Partime\r\nĐịa điểm làm việc: 123 Nguyễn Văn Linh - ĐN"

    return (
        <View style={Styles.wrapper}>
            <View style={Styles.head}>
                <Text size="xsmall" type="bold" value={title} />
            </View>
            <View style={Styles.body}>
                <Image style={Styles.postImage} source={thumb}/>
                <Text value={content}/>
            </View>
        </View>
    )
};

export default PostAds;
