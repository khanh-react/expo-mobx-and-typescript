export type InitScreenProps = {
  isLoading: boolean;
  onUpdateInitInput: (value?) => void,
  onRequestInitUser: (value?) => void,
};

export type FilterProps = {
  by?: 'email' | 'phone';
  value?: string;
  admin?: boolean;
};
