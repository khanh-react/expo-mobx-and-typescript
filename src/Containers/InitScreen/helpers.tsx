import { FilterProps } from './types';
import { validateEmail, validatePhone } from '../../Helpers/Validate';

export const filter = (input: string): FilterProps => {
  if (validateEmail(input)) {
    const isAdmin = input === 'admin@bestaff.io';
    return { by: 'email', value: input, admin: isAdmin };
  }
  if (validatePhone(input)) {
    return { by: 'phone', value: input };
  }
  return null;
};
