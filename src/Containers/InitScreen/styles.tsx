import { StyleSheet, Dimensions } from 'react-native';
import ThemeHelpers from '../../Theme/Helpers';
import ThemeColors from '../../Theme/Colors';

export default StyleSheet.create({
  wrapper: {
    ...ThemeHelpers.fullSize,
    ...ThemeHelpers.colCross,
    ...ThemeHelpers.padding,
    backgroundColor: ThemeColors.white,
  },
  preload: {
    ...ThemeHelpers.colCenter,
    backgroundColor: '#FFFFFF',
    flex: 1,
  },
  preloadLogo: {
    width: 250,
    height: 150,
  },
  header: {
    ...ThemeHelpers.colCenter,
    width: '100%',
    height: 65,
  },
  headerLogo: {
    maxWidth: 170,
    resizeMode: "contain",
  },
  body: {
    ...ThemeHelpers.colCenter,
    width: '100%',
    height: Dimensions.get('screen').height * 0.7,
  },
  banner: {
    height: '80%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  bannerImage: {
    maxWidth: Dimensions.get('screen').width * 0.8,
    resizeMode: 'contain',
  },
  title: {
  },
  subTitle: {
    textAlign: 'center',
    marginTop: 10,
    marginBottom: 10,
  },
  pagination: {},
  footer: {
    ...ThemeHelpers.colCenter,
    flex: 1,
    position: 'absolute',
    bottom: 0,
    minHeight: 70,
    width: '100%',
    paddingBottom: 10,
    backgroundColor: '#ffffff',
  },
  nextBtn: {
    width: '100%',
    height: 70,
  },
  input: {
    marginBottom: 5,
  },
});
