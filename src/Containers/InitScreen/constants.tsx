export const introduceData = [
  {
    id: 'introduceData1',
    banner: require('../../Assets/banners/ipad.png'),
    title: 'Tất cả công cụ bạn cần đều trên 1 nền tảng duy nhất',
    subTitle: 'Xem lịch làm của bạn, xem số lương bạn đã làm được, đổi ca, khai báo lịch hằng tuần',
  },
  {
    id: 'introduceData2',
    banner: require('../../Assets/banners/iphone.png'),
    title: 'Chấm công trên điện thoại cá nhân nhân viên',
    subTitle: 'Ứng dụng xác thực chấm công qua công nghệ Wifi, định vị GPS chặt chẽ và tiện lợi',
  },
  {
    id: 'introduceData3',
    banner: require('../../Assets/banners/pc.png'),
    title: 'Xếp ca, tính lương từ bất kỳ nơi đâu có internet',
    subTitle: 'Quản lý tổng số giờ làm, tình hình trễ vắng, bổ sung nhân sự...',
  },
];
