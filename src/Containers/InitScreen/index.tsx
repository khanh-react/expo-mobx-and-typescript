// @refresh reset
import React, { useState, useEffect, useRef, useCallback } from 'react';
import { FlatList, View, Image, Dimensions, Alert, NativeSyntheticEvent, NativeScrollEvent } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import GestureRecognizer from 'react-native-swipe-gestures';
import Styles from './styles';
import { InitScreenProps as Props } from './types';
import { introduceData } from './constants';
import { filter } from './helpers';
import Pagination from '../../Components/Pagination';
import Button from '../../Components/Button';
import Input from '../../Components/Input';
import Text from '../../Components/Text';
import AnimateView from '../../Components/AnimateView';
import useStores from '../../Helpers/UseStores';
import { setInitialized, isInitialized } from '../../Helpers/LocalStorage';

export const Init = (props: Props): React.ReactElement => {
  const [step, setStep] = useState(1);
  const ref = useRef<FlatList>(null);
  const [invalid, setInValid] = useState(false);
  const [input, setInput] = useState('');
  const [isInit, setInit] = useState(true);
  const [index, setIndex] = useState<number>(step - 1);
  const indexRef = useRef(index);
  indexRef.current = index;
  const navigation = useNavigation();
  const logo = require('../../Assets/logo.png');
  const appLogo = require('../../Assets/app_logo.png');

  const { isLoading, userID, onLoading, offLoading, checkInput,
    setFullName, setAvatar, setUserID, setEmail, setPhone, getCurrentUser } =
  useStores(store => ({
    isLoading: store.auth.isLoading,
    userID: store.auth.id,
    setUserID: store.auth.updateUserId,
    setEmail: store.auth.updateEmail,
    setPhone: store.auth.updatePhone,
    setFullName: store.auth.updateFullname,
    setAvatar: store.auth.doUpdateAvatar,
    onLoading: store.auth.onLoading,
    offLoading: store.auth.offLoading,
    checkInput: store.auth.doCheckInput,
    getCurrentUser: store.auth.getCurrentUser,
  } as any));

  const handleUpdateStep = async (_step: number) => {
    if (_step <= 3) {
      return setStep(_step);
    }
    if (_step > 3) {
      // await setDescribed();
      // setCurrentUserInfo();
      const fullname = await checkInput(input);
      if (fullname) {
        await setInitialized();
        navigation.navigate('SignIn');
      } else {
        Alert.alert('Thông Báo', 'Không tìm thấy vui lòng thử lại!')
      }
    }
  };

  const handleSwipe = (way: string) => {
    if (way === 'NEXT') {
      if (step < 3) { setStep(step + 1); }
    }
    if (way === 'PREVIOUS') {
      if (step > 1) { setStep(step - 1); }
    }
  };

  const handleInputChanged = (val: string) => {
    setInput(val);
    const inValid = filter(val) !== null;
    setInValid(inValid);
  };

  const renderItem = () => {
    return (
      <View>
        <AnimateView style={Styles.banner}>
          <Image style={[Styles.bannerImage]} source={introduceData[step - 1].banner}/>
        </AnimateView>
        <Text style={Styles.title} type="bold" value={introduceData[step - 1].title}/>
        <Text style={Styles.subTitle} value={introduceData[step - 1].subTitle} />
      </View>
    )
  }

  useEffect(() => {
    getCurrentUser().then(async (after: any) => {
      if (!after) {
        setInit(false);
        const isInited = await isInitialized();
        if (isInited) setStep(3);
      }
      navigation.navigate(after);
    })
  }, [userID])

  if (isInit) return (
    <View style={Styles.preload}>
      <Image style={Styles.preloadLogo} resizeMode="stretch" source={appLogo}/>
    </View>
  )

  return (
    <GestureRecognizer
      style={Styles.wrapper}
      onSwipeRight={() => handleSwipe('PREVIOUS')}
      onSwipeLeft={() => handleSwipe('NEXT')}
      >
      <View style={Styles.header}>
        <Image style={Styles.headerLogo} source={logo} />
      </View>
      <View style={Styles.body}>
        <AnimateView style={Styles.banner}>
          <Image style={[Styles.bannerImage]} source={introduceData[step - 1].banner}/>
        </AnimateView>
        <Text style={Styles.title} type="bold" value={introduceData[step - 1].title}/>
        <Text style={Styles.subTitle} value={introduceData[step - 1].subTitle} />
        <Pagination pages={3} currentPage={step}/>
      </View>
      <View style={Styles.footer}>
        <Input
          value={input}
          style={[Styles.input]}
          placeholder="Nhập email / SĐT"
          isHidden={step < 3}
          isDynamicHeight
          onChangeText={handleInputChanged}/>
        <Button
          isLoading={isLoading}
          disabled={step === 3 && !invalid}
          text="Tiếp"
          onPress={() => handleUpdateStep(step + 1)}
          />
      </View>
    </GestureRecognizer>
  );
};

export default Init;
