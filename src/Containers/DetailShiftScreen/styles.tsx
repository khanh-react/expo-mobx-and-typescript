import { StyleSheet } from 'react-native';
import Helpers from '../../Theme/Helpers';

export const Styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        backgroundColor: '#FFF'
    },
    head: {
        ...Helpers.row,
        height: 35,
        paddingLeft: 10,
    },
    headText: {
        marginLeft: 10,
    },
    body: {
        paddingLeft: 10,
    },
    row: {
        ...Helpers.colMain,
        position: 'relative',
        minHeight: 48,
        marginLeft: 10,
        marginRight: 10,
        borderBottomColor: '#F0F0F0',
        borderBottomWidth: 1,
    },
    coverBanner: {
        ...Helpers.colMain,
        position: 'absolute',
        left: 0,
        width: 150,
        height: 48,
        backgroundColor: '#FFF',
        zIndex: 1,
    },
    locationBanner: {
        position: 'absolute',
        right: 0,
    },
    usersInShift: {
        ...Helpers.row,
        flexWrap: 'wrap',
    },
    userInShiftItem: {
        ...Helpers.colMain,
        width: '48%',
        height: 35,
        marginRight: 5,
        marginBottom: 5,
    },
    foot: {
        ...Helpers.rowCenter,
        width: '100%',
        height: 80,
        position: 'absolute',
        bottom: 0,
        borderTopColor: '#eeeeee',
        borderTopWidth: 1,
    },
    footBtn: {
        width: '31%',
    },
    mrt10: {
        marginTop: 10,
    },
    mrr3: {
        marginRight: 5,
    }
});

export default Styles;
