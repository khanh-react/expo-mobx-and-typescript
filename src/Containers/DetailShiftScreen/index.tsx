import React, { useEffect, useState } from 'react';
import { View, Image } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { LinearGradient } from 'expo-linear-gradient';
import { Styles } from './styles';
import { PopupChangeShift } from './PopupChangeShift';
import { UserThumb } from '../../Components/UserThumb';
import { useStores } from '../../Helpers/UseStores';
import { IText as Text } from '../../Components/Text';
import { Icon } from '../../Components/Icon';
import { Button } from '../../Components/Button';
import Colors from '../../Theme/Colors';

export const DetailShiftScreen = () => {
    const navigation = useNavigation();
    const [currentChangeShift, setChangeCurrentShift] = useState('');
    const { setCurrentShift } = useStores(store => ({
        setCurrentShift: store.shift.setCurrentShift,
    } as any));

    useEffect(() => {
        return () => { setCurrentShift(null); };
    }, []);

    return (
        <View style={Styles.wrapper}>
            <View style={Styles.head}>
                <Icon source={require('../../Assets/back_black.png')} onPress={() => navigation.navigate('Home')}/>
                <Text style={Styles.headText} size="xsmall" type="bold" value="Chi tiết ca"/>
            </View>
            <View style={Styles.body}>
                <View style={Styles.row}>
                    <View style={Styles.coverBanner}>
                        <Text>30 Lê Thanh Nghị</Text>
                    </View>
                    <Image style={Styles.locationBanner} source={require('../../Assets/location_banner.png')}/>
                </View>
                <View style={Styles.row}>
                    <Text value="Ca sáng - 12"/>
                </View>
                <View style={Styles.row}>
                    <Text value="6:30 am - 12:00 pm"/>
                </View>
                <View style={Styles.row}>
                    <Text size="smaller" color={Colors.main} value="Trạng thái ca"/>
                    <Text size="smaller" color={Colors.main} value="Trạng thái ca Trạng thái ca Trạng thái ca Trạng thái ca"/>
                </View>
                <View style={Styles.row}>
                    <Text value="6:30 am - 12:00 pm"/>
                </View>
                <View style={Styles.row}>
                    <Text value="Ghi chú của Ca: “Hôm nay có khách đoàn đông hơn thông thường chút nên mọi người ăn uống đầy đủ trước đi đi làm nhé”"/>
                </View>
                <View style={[Styles.row, Styles.mrt10]}>
                    <Text value="Đồng nghiệp có mặt"/>
                    <View style={Styles.usersInShift}>
                        <View style={Styles.userInShiftItem}>
                            <UserThumb id={1} name="Nguyen Quang Khanh" isShowShortName/>
                        </View>
                        <View style={Styles.userInShiftItem}>
                            <UserThumb id={1} name="Nguyen Quang Khanh" isShowShortName/>
                        </View>
                        <View style={Styles.userInShiftItem}>
                            <UserThumb id={1} name="Nguyen Quang Khanh" isShowShortName/>
                        </View>
                        <View style={Styles.userInShiftItem}>
                            <UserThumb id={1} name="Nguyen Quang Khanh" isShowShortName/>
                        </View>
                        <View style={Styles.userInShiftItem}>
                            <UserThumb id={1} name="Nguyen Quang Khanh" isShowShortName/>
                        </View>
                        <View style={Styles.userInShiftItem}>
                            <UserThumb id={1} name="Nguyen Quang Khanh" isShowShortName/>
                        </View>
                    </View>
                </View>
            </View>
            <View style={Styles.foot}>
                <Button
                    style={[Styles.footBtn, Styles.mrr3]}
                    icon={require('../../Assets/clock.png')}
                    text="CHECKIN" size="medium"
                    onPress={() => {}}
                    />
                <Button
                    style={[Styles.footBtn, Styles.mrr3]}
                    icon={require('../../Assets/replace.png')}
                    type="outline-primary"
                    text="Đổi ca"
                    size="medium"
                    onPress={() => setChangeCurrentShift('CHANGE')}
                    />
                <Button
                    icon={require('../../Assets/login2.png')}
                    style={Styles.footBtn}
                    type="outline-primary"
                    text="Nhượng ca"
                    size="medium"
                    onPress={() => setChangeCurrentShift('GIVE')}
                    />
            </View>
            <PopupChangeShift
                type={currentChangeShift as any}
                onClosed={() => setChangeCurrentShift('')}
            />
        </View>
    )
};

export default DetailShiftScreen;
