import React from 'react';
import { View } from 'react-native';
import { Styles } from './styles';
import { PopupChangeShiftProps as Props } from './types';
import { Popup } from '../../../Components/Popup';
import { IText as Text } from '../../../Components/Text';
import { Textarea } from '../../../Components/Textarea';
import { Button } from '../../../Components/Button';
import { ICheckbox as Checkbox } from '../../../Components/Checkbox';
import { UserThumb } from '../../../Components/UserThumb';

export const PopupChangeShift = ({ type, onClosed }: Props) => {
    if (!type) return null;
    const SampleAvatar = require('../../../Assets/mock/avatar1.png');
    return (
        <Popup openAt={new Date().getTime()} onClosed={onClosed}>
            <View style={Styles.wrapper}>
                <View style={Styles.head}>
                    <Text type="bold" size="xsmall" value={`Chọn đồng nghiệp muốn ${type === 'CHANGE' ? 'đổi ca' : 'nhượng ca'}`}/>
                </View>
                <View style={Styles.body}>
                    <View style={Styles.row}>
                        <UserThumb id={1} avatar={SampleAvatar} name="Nguyễn Quang Khánh" isShowFullName/>
                        <Checkbox style={Styles.checkbox} isChecked={true} onPress={() => {}}/>
                    </View>
                    <View style={Styles.row}>
                        <UserThumb id={1} avatar={SampleAvatar} name="Lê Quốc Hùng" isShowFullName/>
                        <Checkbox style={Styles.checkbox} isChecked={false} onPress={() => {}}/>
                    </View>
                    <View style={Styles.row}>
                        <UserThumb id={1} avatar={SampleAvatar} name="Trần Quốc Toản" isShowFullName/>
                        <Checkbox style={Styles.checkbox} isChecked={false} onPress={() => {}}/>
                    </View>
                    <View>
                        <Textarea placeholder="Ghi chú" size="small"/>
                    </View>
                </View>
                <View style={Styles.foot}>
                    <Button style={Styles.footBtn} text="Gửi"/>
                </View>
            </View>
        </Popup>
    )
};

export default PopupChangeShift;
