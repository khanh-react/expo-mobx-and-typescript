export type PopupChangeShiftProps = {
    type: 'CHANGE' | 'GIVE' | '';
    onClosed: (e?: any) => void;
};
