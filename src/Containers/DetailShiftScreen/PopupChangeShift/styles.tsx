import { StyleSheet } from 'react-native';
import Helpers from '../../../Theme/Helpers';

export const Styles = StyleSheet.create({
    wrapper: {
        ...Helpers.colCross,
        flex: 1,
        paddingLeft: 20,
        paddingRight: 20,
    },
    head: {
        ...Helpers.center,
        width: '100%',
        height: 50,
        marginBottom: 10,
        borderBottomColor: '#F0F0F0',
        borderBottomWidth: 1,
    },
    body: {
        width: '100%'
    },
    foot: {
        ...Helpers.center,
        width: '100%',
        height: 50,
        position: 'absolute',
        bottom: 0,
    },
    footBtn: {
        width: 120,
    },
    row: {
        ...Helpers.rowCross,
        width: '100%',
        height: 50,
        position: 'relative',
    },
    checkbox: {
        position: 'absolute',
        right: 0,
    }
});

export default Styles;
