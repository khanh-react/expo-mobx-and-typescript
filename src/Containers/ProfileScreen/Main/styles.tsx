import { StyleSheet, Dimensions } from 'react-native';
import Helpers from '../../../Theme/Helpers';

const height = Dimensions.get('screen').height;

export const Styles = StyleSheet.create({
    wrapper: {
        ...Helpers.column,
        flex: 1,
        backgroundColor: '#FFF',
        position: 'relative',
    },
    head: {
        ...Helpers.rowCross,
        height: 30,
        marginLeft: 10,
    },
    headThumb: {
        height: 50,
        position: 'relative',
    },
    headThumbInfo: {
        ...Helpers.rowCross,
        height: 50,
    },
    headThumbBg: {
        width: '100%',
        overflow: 'hidden',
        position: 'absolute',
    },
    headThumbBgHide: {
        marginTop: 50,
        width: '100%',
        height: 500,
        backgroundColor: '#FFF',
        position: 'absolute',
    },
    headAvatar: {
        marginLeft: 17,
        marginRight: 10,
    },
    headTag: {
        position: 'absolute',
        right: 10,
    },
    body: {
        width: '100%',
        maxHeight: height - 220,
    },
    foot: {
        position: 'absolute',
        bottom: 0,
        width: '100%',
        height: 220,
        backgroundColor: '#F1F1F1'
    },
    footBanner: {
        ...Helpers.center,
        marginTop: 10,
    },
    footVerlog: {
        ...Helpers.row,
        position: 'absolute',
        width: '100%',
        bottom: 10,
    },
    footVerLogOut: {
        ...Helpers.rowCenter,
        position: 'absolute',
        right: 10,
    },
    footVerlogVersion: {
        left: 10,
    },
    mrl10: {
        marginLeft: 10,
    },
    mrl5: {
        marginLeft: 5,
    }
})

export default Styles;
