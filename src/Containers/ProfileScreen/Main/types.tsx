export type MainProps = {
    fullName: string;
    avatar?: string;
    email?: string;
    phone?: string;
    onGetInfoDetail: () => void;
    onBack: () => void;
    onLogout: () => void;
}