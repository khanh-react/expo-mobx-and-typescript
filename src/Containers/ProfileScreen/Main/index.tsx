import React, { useState } from 'react';
import { View, ScrollView, Image, TouchableOpacity } from 'react-native';
import { Styles } from './styles';
import { MainProps as Props } from './types';
import RatingPopup from './Rating';
import { useStores } from '../../../Helpers/UseStores';
import { logout } from '../../../Helpers/LocalStorage';
import Item from '../../../Components/MasterItem';
import Text from '../../../Components/Text';
import Icon from '../../../Components/Icon';
import Tag from '../../../Components/Tag';
import { UserThumb } from '../../../Components/UserThumb';
import ChangeSvg from '../../../Assets/icons/change';
import NotiSvg from '../../../Assets/icons/notification';
import SettingSvg from '../../../Assets/icons/setting';
import HelpSvg from '../../../Assets/icons/help';
import ReportSvg from '../../../Assets/icons/report';
import OtherSvg from '../../../Assets/icons/other';
import LogoutSvg from '../../../Assets/icons/logout';

export const Main = ({ fullName, avatar, email, phone, onGetInfoDetail, onBack, onLogout }: Props) => {
    const [isRating, setRating] = useState(false);

    const back = require('../../../Assets/back_black.png');
    const background = require('../../../Assets/profile_bg.png');
    const setting = require('../../../Assets/setting_setting.png');
    const transfer = require('../../../Assets/setting_transfer.png');
    const noti = require('../../../Assets/setting_noti.png');
    const help = require('../../../Assets/setting_help.png');
    const report = require('../../../Assets/setting_report.png');
    const more = require('../../../Assets/setting_more.png');
    const gift = require('../../../Assets/gift.png');
    const logout = require('../../../Assets/login.png');
    const banner = require('../../../Assets/evaluation_banner.png');

    return (
        <View style={Styles.wrapper}>
            <View style={Styles.head}>
                <Icon source={back} onPress={onBack}/>
                <Text style={Styles.mrl10} size="xsmall" type="bold" value="Tài Khoản"/>
            </View>
            <View style={Styles.headThumb}>
                <Image style={Styles.headThumbBg} source={background}/>
                <View style={Styles.headThumbBgHide}/>
                <View style={Styles.headThumbInfo} >
                    <UserThumb style={Styles.headAvatar} id={1} uri={avatar}/>
                    <View>
                        <Text type="bold" color="#FFF">{fullName}</Text>
                        <Text color="#FFF" size="smaller" value={email || phone}/>
                        {/* <Text color="#FFF" size="smaller" value="Xem hồ sơ của bạn"/> */}
                    </View>
                    {/* <Tag style={Styles.headTag} value="chưa xác thực"/> */}
                </View>
            </View>
            <ScrollView style={Styles.body} onTouchEnd={() => isRating && setRating(false)}>
                <Item icon={<ChangeSvg />} value="Quản lý yêu cầu" label="Đổi ca, thêm giờ, xin phép ..."  type="setting" isBlocked/>
                <Item icon={<NotiSvg />} value="Cài đặt thông báo" type="setting" isBlocked/>
                <Item icon={<SettingSvg />} value="Cài đặt chung" label="Trang chủ, Ca làm việc, Đăng nhập"  type="setting" isBlocked/>
                <Item icon={<HelpSvg />} value="Hỗ trợ" label="HDSD, FAQ, Video"  type="setting" isBlocked/>
                <Item icon={<ReportSvg />} value="Báo lỗi phần mềm"  type="setting" isBlocked/>
                <Item icon={<OtherSvg />} value="Khác" label="Thông tin ứng dụng, Chính sách bảo mật"  type="setting" isBlocked/>
                <Item icon={<Icon source={gift} />} color="#800000" value="Giới thiệu app nhận ngay 399K" type="setting" isBlocked/>
            </ScrollView>
            <View style={Styles.foot}>
                <TouchableOpacity style={Styles.footBanner} onPress={() => setRating(true)}>
                    <Image source={banner}/>
                </TouchableOpacity>
                <View style={Styles.footVerlog}>
                    <Text style={Styles.footVerlogVersion} size="smaller" value="Phiên bản 1.0.1"/>
                    <TouchableOpacity style={Styles.footVerLogOut} onPress={onLogout}>
                        <LogoutSvg fill="#000" />
                        <Text style={Styles.mrl5} size="smaller">Đăng xuất</Text>
                    </TouchableOpacity>
                </View>
            </View>
            <RatingPopup isHidden={!isRating} onPopupClosed={() => setRating(false)}/>
        </View>
    )
}

export default Main;
