import React, { useState } from 'react';
import { View, Image } from 'react-native';
import { Styles } from './styles';
import { RatingProps as Props } from './types';
import { IRating } from '../../../../Components/Rating';
import { Textarea } from '../../../../Components/Textarea';
import { IText as Text } from '../../../../Components/Text';
import { Button } from '../../../../Components/Button';
import { Popup } from '../../../../Components/Popup';

export const RatingPopup = ({ onChange, isHidden, onPopupClosed }: Props) => {
    if (isHidden) return null;
    const [isLoading, setLoaded] = useState(false);
    const [sent, setSent] = useState(false);
    const [rating, setRating] = useState(2);
    const [note, setNote] = useState('');
    const placeholder = "Để lại nội dung góp ý cho Bestaff càng cụ thể càng tốt bạn nhé!";
    const handleSend = () => {
        setLoaded(true);
        setTimeout(() => {
            setLoaded(false);
            setSent(true);
        }, 1500);
    }
    return (
        <Popup openAt={new Date().getTime()} onClosed={onPopupClosed}>
            {
                !sent &&
                <View style={Styles.wrapper}>
                    <IRating onChange={setRating}/>
                    <Textarea placeholder={placeholder} onChange={setNote}/>
                    <Button style={Styles.closeBtn} text="Gửi" onPress={handleSend} isLoading={isLoading}/>
                </View>
            }
            {
                sent &&
                <View style={[Styles.wrapper, Styles.thanksWrap]}>
                    <Image source={require('../../../../Assets/thank_you.png')}/>
                    <Text style={Styles.mrt10} value="Bestaff đã ghi nhận phản hồi của bạn. Mong nhận được thêm ý kiến góp ý của bạn trong tương lai."/>
                    <Button style={Styles.closeBtn} text="Đóng" onPress={onPopupClosed}/>
                </View>
            }
        </Popup>
    )
}

export default RatingPopup;
