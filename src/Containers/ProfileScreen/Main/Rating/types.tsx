import { boolean } from "mobx-state-tree/dist/internal";

export type RatingProps = {
    isHidden?: boolean;
    onChange?: (rating: number) => void;
    onPopupClosed: () => void;
}