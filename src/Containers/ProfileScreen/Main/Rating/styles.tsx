import { StyleSheet } from 'react-native';
import ThemeHelpers from '../../../../Theme/Helpers';

export const Styles = StyleSheet.create({
    wrapper: {
        width: '100%',
        ...ThemeHelpers.colCross,
    },
    thanksWrap: {
        marginTop: 20,
    },
    closeBtn: {
        position: 'absolute',
        bottom: 0,
    },
    mrt10: {
        marginTop: 10,
    }
})