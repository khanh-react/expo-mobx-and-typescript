import React, { useState, useEffect } from 'react';
import { View, Text, Image } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import Styles from './styles';
import ImageBackDrop from '../BackDrop';
import Icon from '../../../Components/Icon';
import { useStores } from '../../../Helpers/UseStores';
import { setCurrentUserInfo, getCurrentUserID } from '../../../Helpers/LocalStorage';

export const Profile = (): React.ReactElement => {
  const [step, setStep] = useState(0);
  const [isLoading, setLoading] = useState(false);
  const [userID, setUserID] = useState('');
  const navigation = useNavigation();
  const sampleIcon = require('../../Assets/none-avatar.png');
  const background = require('../../Assets/profile_bg.png');
  const cameraIcon = require('../../Assets/camera.png');

  const { fullname, avatar, updateAvatar } =
  useStores(store => ({
    fullname: store.auth.fullname,
    avatar: store.auth.avatar,
    updateAvatar: store.auth.doUpdateAvatar,
  } as any));

  const handleUriChanged = async (val: any) => {
    if (val.uri) {
      const response = await fetch(val.uri);
      // keep avatar to localstorage
      // setCurrentUserInfo(userID, [
      //   {
      //     key: 'avatar',
      //     value: val.uri,
      //   }
      // ])

      // send avatar to server
      // updateAvatar(val);
    }
  };
  useEffect(() => {
    getCurrentUserID().then(id => setUserID(id))
  })

  return (
    <View style={Styles.wrapper}>
      <View style={Styles.head}>
        <Image style={Styles.headBackground} source={background} />
        <View style={Styles.headNarbav}>
          <View style={Styles.avatar}>
            {
              avatar ?
              <Image source={{ uri: avatar }} resizeMode={'stretch'} style={Styles.avatarPicked}/> :
              <Image source={sampleIcon} resizeMode={'stretch'} />
            }
            <Icon style={Styles.camera} source={cameraIcon} />
          </View>
        </View>
        <View style={Styles.nameWrapper}>
          <Text style={Styles.name}>{fullname}</Text>
        </View>
      </View>
      {/* <View style={Styles.body}>
      </View> */}
      <ImageBackDrop isHidden={step === 1} isLoading={isLoading} onChange={handleUriChanged}/>
    </View>
  );
};

export default Profile;
