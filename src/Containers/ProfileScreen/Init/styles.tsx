import { StyleSheet } from 'react-native';
import ThemeHelpers from '../../../Theme/Helpers';

export default StyleSheet.create({
  wrapper: {
    ...ThemeHelpers.fillColCross,
  },
  head: {
    width: '100%',
    height: 300,
    backgroundColor: '#ffffff',
  },
  headBackground: {
    width: '100%',
    position: 'absolute',
  },
  headNarbav: {
    ...ThemeHelpers.colCross,
    width: '100%',
    height: 150,
    position: 'relative',
  },
  headSettings: {
    position: 'absolute',
    left: 10,
    top: 10,
  },
  headLogout: {
    position: 'absolute',
    right: 10,
    top: 10,
  },
  headTextOptions: {
    fontSize: 18,
    color: '#ffffff',
    fontWeight: '600',
  },
  headTextTitle: {
    fontSize: 34,
    color: '#ffffff',
    fontWeight: 'bold',
    letterSpacing: 1.5,
  },
  body: {
    width: '100%',
    marginTop: 10,
    padding: 10,
  },
  bodyOptions: {
    ...ThemeHelpers.rowCenter,
    width: '100%',
    height: 35,
    justifyContent: 'center',
    borderRadius: 20,
    backgroundColor: '#E8E8E8',
  },
  bodyOptionsItem: {
    ...ThemeHelpers.colCenter,
    width: '33.33%',
    height: '100%',
  },
  bodyOptionsItemActive: {
    backgroundColor: '#ffffff',
    borderRadius: 20,
  },
  bodyOptionsItemText: {
    fontSize: 18,
  },
  bodyOptionsItemTextActive: {
    color: '#28A0F9',
    fontWeight: 'bold',
  },
  bodyDetails: {
    ...ThemeHelpers.column,
    marginTop: 50,
  },
  foot: {},
  avatar: {
    ...ThemeHelpers.colCenter,
    ...ThemeHelpers.shadow,
    width: 150,
    height: 150,
    borderRadius: 75,
    borderWidth: 1,
    backgroundColor: '#ffffff',
    borderColor: 'silver',
    position: 'absolute',
    top: 75,
  },
  avatarPicked: {
    width: 120,
    height: 120,
    borderRadius: 60,
  },
  camera: {
    padding: 3,
    borderRadius: 50,
    borderColor: 'silver',
    borderWidth: 1,
    right: 0,
    bottom: 20,
    backgroundColor: '#ffffff',
    position: 'absolute',
  },
  nameWrapper: {
    width: '100%',
    position: 'absolute',
    bottom: 20,
    alignItems: 'center',
  },
  name: {
    ...ThemeHelpers.shadow,
    color: '#ffffff',
    fontWeight: 'bold',
    fontSize: 30,
  },
});
