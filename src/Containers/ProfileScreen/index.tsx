import React, { useState, useEffect } from 'react';
import { View, Image, Alert } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import * as DocumentPicker from 'expo-document-picker';
import Styles from './styles';
import ImageBackDrop from './BackDrop';
import Main from './Main';
import Tabs from './Tabs';
import Icon from '../../Components/Icon';
import { IText as Text } from '../../Components/Text';
import { useStores } from '../../Helpers/UseStores';
import { logout, getCurrentUserInfo, setCurrentUserInfo } from '../../Helpers/LocalStorage';
import { getUserAvatar } from '../../Helpers/FirebaseService';
import * as FirebaseStorage from 'firebase';

export const Profile = (): React.ReactElement => {
  const [isInited, setInit] = useState(false);
  const [isShowDetail, setShowDetail] = useState(false);
  const navigation = useNavigation();
  const sampleIcon = require('../../Assets/none-avatar.png');
  const background = require('../../Assets/profile_bg.png');
  const cameraIcon = require('../../Assets/camera.png');
  const backIcon = require('../../Assets/back_white.png');

  const { userID, fullname, avatar, email, phone, updateAvatar, isLoading, onLoading, offLoading, onLogout } =
  useStores(store => ({
    userID: store.auth.id,
    fullname: store.auth.fullname,
    avatar: store.auth.avatar,
    email: store.auth.email,
    phone: store.auth.phone,
    updateAvatar: store.auth.doUpdateAvatar,
    isLoading: store.auth.isLoading,
    onLoading: store.auth.onLoading,
    offLoading: store.auth.offLoading,
    onLogout: store.auth.doLogout,
  } as any));

  useEffect(() => {
    if (avatar) {
      setShowDetail(false);
    } else {
      setShowDetail(true);
    }
  }, [isShowDetail, avatar]);

  const handleUriChanged = async (val: any) => {
    onLoading();
    setTimeout(async () => {
      offLoading();
      try {
        const storageRef = FirebaseStorage.storage().ref();
        const ref = storageRef.child(`avatar_${userID}.png`);
        const file = await fetch(val.uri);
        const blob = await file.blob() as any;
        await ref.put(blob);
        await updateAvatar(getUserAvatar(userID));
        await navigation.navigate('SignInAfter');
      } catch (error) {
        Alert.alert('Đã xảy ra lỗi, vui lòng thử lại!');
      }
    }, 1500);
  };

  const openPhotoLib = async () => {
    const res = await DocumentPicker.getDocumentAsync() as any;
    updateAvatar(res);
    // await setProfileImage(res.uri);
  };

  const handleLogout = async () => {
    await onLogout();
    navigation.navigate('Init');
  }

  if (!isShowDetail) {
    return <Main
      fullName={fullname}
      avatar={avatar}
      email={email}
      phone={phone}
      onBack={() => navigation.navigate('SignInAfter')}
      onGetInfoDetail={() => setShowDetail(true)}
      onLogout={handleLogout}
    />
  }

  return (
    <View style={Styles.wrapper}>
      <View style={Styles.head}>
        <Icon
          style={Styles.headBack}
          isHidden={!isInited}
          source={backIcon}
          onPress={() => setShowDetail(false)}/>
        <Image style={Styles.headBackground} source={background} />
        <View style={Styles.headNarbav}>
          <View style={Styles.avatar}>
            {
              avatar ?
              <Image source={{ uri: avatar }} resizeMode={'stretch'} style={Styles.avatarPicked} /> :
              <Image source={sampleIcon} resizeMode={'stretch'} />
            }
            <Icon style={Styles.camera} source={cameraIcon} onPress={openPhotoLib}/>
          </View>
        </View>
        <View style={Styles.nameWrapper}>
          <Text style={Styles.name}>{fullname}</Text>
        </View>
      </View>
      {/* <View style={Styles.body}>
        <Tabs isHidden={!isInited} />
      </View> */}
      <ImageBackDrop isHidden={isInited} isLoading={isLoading} onChange={handleUriChanged}/>
    </View>
  );
};

export default Profile;
