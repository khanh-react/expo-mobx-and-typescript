import React from 'react';
import { ScrollView } from 'react-native';
import Styles from './styles';
import { TabProps as Props } from './types';
import Item from '../../../Components/MasterItem';

const TabBasic = ({ name, isHidden, onGetItem }: Props) => {
  if (name !== 'BASIC') return null;
  if (isHidden) return null;
  const phoneIcon = require('../../../Assets/profile_phone.png');
  const emailIcon = require('../../../Assets/profile_email.png');
  const passwordIcon = require('../../../Assets/profile_password.png');
  const addressIcon = require('../../../Assets/profile_home.png');
  const idIcon = require('../../../Assets/profile_id.png');
  const birthdayIcon = require('../../../Assets/profile_birthday.png');
  const locationIcon = require('../../../Assets/profile_location.png');
  const positionIcon = require('../../../Assets/profile_position.png');
  const branchIcon = require('../../../Assets/profile_branch.png');

  return (
    <ScrollView style={Styles.body}>
      <Item
        icon={phoneIcon}
        label="Di động"
        value="+84935055695"
        onPress={() => onGetItem('phone')}
        isRequired/>
      <Item
        icon={emailIcon}
        label="Email"
        value="khanhdtu1993@gmail.com"
        onPress={() => onGetItem('email')}
        isRequired/>
      <Item
        icon={passwordIcon}
        label="Mật khẩu"
        value="khanhdtu1993@gmail.com"
        type="password"
        onPress={() => onGetItem('password')}
        isRequired/>
      <Item icon={addressIcon}
        label="Địa chỉ thường trú"
        value="16 Nguyễn Hành, Q.Hải Châu, TP. Đà Nẵng"
        onPress={() => onGetItem('address')}
        isRequired/>
      <Item
        icon={idIcon}
        label="CMND"
        value=""
        onPress={() => onGetItem('CMND')}
        isRequired/>
      <Item
        icon={birthdayIcon}
        label="Ngày sinh"
        value=""
        onPress={() => onGetItem('birthday')}
        isRequired/>
      <Item
        icon={locationIcon}
        label="Địa điểm làm việc và mã pin"
        value="K49/H34/N22 Phan Châu Trinh"
        onPress={() => onGetItem('location')}/>
      <Item
        icon={positionIcon}
        label="Vị trí"
        value="Pha chế, Lập trình"
        onPress={() => onGetItem('position')}
        isRequired/>
      <Item
        icon={branchIcon}
        label="Phòng ban"
        value="Hành chính"
        onPress={() => onGetItem('branch')}
        />
    </ScrollView>
  );
};

export default TabBasic;
