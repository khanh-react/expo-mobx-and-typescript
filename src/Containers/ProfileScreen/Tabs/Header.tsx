import React, { useState } from 'react';
import { View, TouchableOpacity, Image } from 'react-native';
import Styles from './styles';
import { TabProps as Props, TabNames } from './types';
import Text from '../../../Components/Text';

const TabHeader = ({ isHidden, onChangeTab }: Props) => {
  const [tab, setTab] = useState<TabNames>('BASIC');

  const handleTabChanged = (tabName: TabNames) => {
    onChangeTab(tabName);
    setTab(tabName);
  };

  if (isHidden) return null;

  return (
    <View style={Styles.head}>
      <View style={[Styles.headItem]}>
        <TouchableOpacity onPress={() => handleTabChanged('BASIC')}>
          <Text
            color={tab === 'BASIC' ? '#31A7FF' : '#CBCBCB'}
            size="medium" type="bold">T.TIN CƠ BẢN</Text>
        </TouchableOpacity>
      </View>
      <View style={Styles.headItem}>
        <TouchableOpacity onPress={() => handleTabChanged('OTHER')}>
          <Text
            color={tab === 'OTHER' ? '#31A7FF' : '#CBCBCB'}
            size="medium" type="bold">KHÁC</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default TabHeader;
