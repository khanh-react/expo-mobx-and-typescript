export type TabNames = 'BASIC' | 'OTHER';

export type TabProps = {
  name?: TabNames;
  isHidden?: boolean;
  onGetItem?: (itemName) => void;
  onChangeTab?: (tabName) => void;
};

export type EditorProps = {
  itemName: string;
  onSave: (value) => void;
  onCancel: () => void;
};
