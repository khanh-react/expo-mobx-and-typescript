import { StyleSheet } from 'react-native';
import ThemeHelpers from '../../../Theme/Helpers';

export default StyleSheet.create({
  wrapper: {
    backgroundColor: '#ffffff',
    width: '100%',
    height: '100%',
    borderWidth: 1,
    borderColor: 'silver',
    borderRadius: 20,
    marginTop: -20,
  },
  head: {
    ...ThemeHelpers.row,
    height: 50,
  },
  headItem: {
    ...ThemeHelpers.colCenter,
    width: '50%',
  },
  body: {
    paddingLeft: 10,
    paddingRight: 10,
    maxHeight: 350,
  },
  bodyItem: {
    ...ThemeHelpers.row,
    height: 50,
    marginBottom: 7,
  },
  bodyItemIconWrapper: {
    ...ThemeHelpers.colCenter,
    width: 60,
  },
  bodyItemIcon: {
    width: 40,
    height: 40,
  },
  bodyItemIconWarn: {
    position: 'absolute',
    width: 20,
    height: 20,
    zIndex: 1,
    bottom: 0,
    right: 7,
  },
  bodyItemContent: {
    ...ThemeHelpers.colMain,
    width: '80%',
    borderBottomWidth: 1,
    borderBottomColor: 'silver',
  },
  otherTitle: {
    marginLeft: 15,
    marginTop: 10,
  },
  editorWrap: {
    height: '100%',
    backgroundColor: '#ffffff',
    borderRadius: 20,
    marginTop: -50,
    padding: 10,
  },
  editorBtnWrap: {
    ...ThemeHelpers.rowCenter,
  },
  editorBtn: {
    maxWidth: '49%',
  },
  editorBtnMr: {
    marginRight: 3,
  },
});
