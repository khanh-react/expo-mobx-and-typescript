import React, { useState, useRef, useEffect } from 'react';
import { View, Text } from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import Styles from './styles';
import { EditorProps as Props } from './types';
import Input from '../../../Components/Input';
import Button from '../../../Components/Button';

const TabEditor = ({ itemName , onSave, onCancel }: Props) => {
  const [value, setValue] = useState('');
  const [date, setDate] = useState(new Date());

  useEffect(() => {
    setValue('');
  }, [itemName]);

  if (!itemName) return null;

  const toIsoSimple = (timestamp: number) => {
    return new Date(timestamp).toISOString().slice(0, 10);
  };

  return (
    <View style={Styles.editorWrap}>
      <Input
        isPrimary
        isFocused={itemName !== 'birthday'}
        placeholder={`Nhập ${itemName} mới`}
        value={value}
        onChangeText={setValue}/>
      { itemName === 'birthday' &&
        <DateTimePicker
          mode="countdown"
          value={date}
          onChange={e => setValue(toIsoSimple((e.nativeEvent as any).timestamp))}/> }
      <View style={Styles.editorBtnWrap}>
        <Button style={[Styles.editorBtn, Styles.editorBtnMr]} type="default" text="Hủy" onPress={onCancel}/>
        <Button style={[Styles.editorBtn]} text="Lưu" onPress={() => onSave(value)}/>
      </View>
    </View>
  );
};

export default TabEditor;
