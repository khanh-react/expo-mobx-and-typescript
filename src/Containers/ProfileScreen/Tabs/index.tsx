import React, { useState } from 'react';
import { View } from 'react-native';
import Styles from './styles';
import { TabProps as Props, TabNames } from './types';
import TabHeader from './Header';
import TabBasic from './Basic';
import TabOther from './Other';
import Editor from './Editor';

const Tabs = ({ isHidden }: Props) => {
  const [tab, setTab] = useState<TabNames>('BASIC');
  const [currentItem, setItem] = useState<string>('');

  if (isHidden) return null;

  return (
    <View style={Styles.wrapper}>
      <TabHeader isHidden={currentItem !== ''} onChangeTab={tabName => setTab(tabName)}/>
      <TabBasic isHidden={currentItem !== ''} name={tab} onGetItem={item => setItem(item)}/>
      <TabOther isHidden={currentItem !== ''} name={tab} onGetItem={item => setItem(item)}/>
      <Editor
        itemName={currentItem}
        onSave={val => setItem('')}
        onCancel={() => setItem('')}/>
    </View>
  );
};

export default Tabs;
