import React, { useState } from 'react';
import { ScrollView, View, TouchableOpacity, Image } from 'react-native';
import Styles from './styles';
import { TabProps as Props } from './types';
import Text from '../../../Components/Text';
import Item from '../../../Components/MasterItem';

const TabOther = ({ name, isHidden, onGetItem }: Props) => {
  if (name !== 'OTHER') return null;
  if (isHidden) return null;
  const experienceIcon = require('../../../Assets/profile_exp.png');
  const certificateIcon = require('../../../Assets/profile_cert.png');
  const degreeIcon = require('../../../Assets/profile_degree.png');

  return (
    <ScrollView style={Styles.body}>
      {/* WORK EXPERIENCE */}
      <Text style={Styles.otherTitle} size="medium" type="bold" value="Kinh nghiệm" />
      <Item
        icon={experienceIcon}
        label="Kinh nguyệt làm việc"
        value=""
        onPress={() => onGetItem('kinh nguyệt làm việc')}
        />
      <Item
        icon={experienceIcon}
        label="16 tháng 3, 2011 - 6 tháng 5, 2020"
        value="Bếp trưởng tại Hayatt Resort, Đà Nẵng"
        onPress={() => onGetItem('kinh nguyệt làm việc')}
        />

      {/* CERTIFICATE */}
      <Text style={Styles.otherTitle} size="medium" type="bold" value="Bằng cấp" />
      <Item
        icon={certificateIcon}
        label="Bằng cấp / chứng chỉ"
        value=""
        onPress={() => onGetItem('bằng cấp / chứng chỉ')}
        />
      <Item
        icon={certificateIcon}
        label="16 tháng 3, 2011"
        value="MBA - Loại Giỏi - NUS"
        onPress={() => onGetItem('bằng cấp / chứng chỉ')}
        />

      {/* EDUCATION */}
      <Text style={Styles.otherTitle} size="medium" type="bold" value="Học vấn" />
      <Item
        icon={degreeIcon}
        label="Học vấn"
        value=""
        onPress={() => onGetItem('học vấn')}
      />
      <Item
        icon={degreeIcon}
        label="16 tháng 3, 2011 - 6 tháng 5, 2020"
        value="QTKD tại Đại học Kinh Tế - HCM"
        onPress={() => onGetItem('học vấn')}
        />
    </ScrollView>
  );
};

export default TabOther;
