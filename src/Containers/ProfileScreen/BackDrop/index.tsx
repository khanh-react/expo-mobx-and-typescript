import React, { useState, useRef } from 'react';
import { View, Text, TouchableOpacity, Alert } from 'react-native';
// import * as Permissions from 'expo-permissions';
import * as DocumentPicker from 'expo-document-picker';
// import { Camera as ExpoCamera, CameraCapturedPicture } from 'expo-camera';
import Styles from './styles';
import { ImageBackDropProps as Props } from './types';
import Button from '../../../Components/Button';
import Camera from '../../../Components/Camera';
import { Popup } from '../../../Components/Popup';
import { setInitialized } from '../../../Helpers/LocalStorage';

export const ImageBackDrop = ({ isHidden, isLoading, onChange }: Props) => {
  // const usePermission = Permissions.usePermissions;
  // const [camera, askForCamera] = usePermission(Permissions.CAMERA, { ask: true });
  const [camerax, setCamerax] = useState(false);
  const [openAt, setOpenAt] = useState(new Date().getTime());
  const openPhotoLib = async () => {
    const res = await DocumentPicker.getDocumentAsync() as any;
    onChange && onChange(res);
    await setInitialized();
  };

  if (isHidden) return null;
  return (
    <Popup openAt={openAt} onClosed={() => setOpenAt(new Date().getTime())}>
      <View style={Styles.wrapper}>
        <Text style={Styles.title}>Hãy để các đồng nghiệp nhận diện bạn dễ dàng</Text>
        <Text style={Styles.subTitle}>Chụp hoặc tải lên chân dung rõ nét nhất của bạn</Text>
        <Button
          style={Styles.takePhotoBtn}
          text="Chọn ảnh từ thư viện"
          isLoading={isLoading}
          onPress={() => openPhotoLib()}
          />
        <Camera isVisible={camerax}/>
      </View>
    </Popup>
  );
};

export default ImageBackDrop;
