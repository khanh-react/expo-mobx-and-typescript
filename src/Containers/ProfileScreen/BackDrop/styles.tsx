import { StyleSheet, Dimensions } from 'react-native';
import ThemeHelpers from '../../../Theme/Helpers';

const width = Dimensions.get('screen').width;

export default StyleSheet.create({
  wrapper: {
    ...ThemeHelpers.fillColCenter,
    width: width,
    height: 350,
    padding: 20,
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,
    borderWidth: 1,
    borderColor: 'silver',
    backgroundColor: '#ffffff',
    position: 'absolute',
    bottom: 0,
  },
  title: {
    fontSize: 30,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  subTitle: {
    fontSize: 18,
    lineHeight: 20,
    textAlign: 'center',
    marginTop: 10,
    color: '#666666',
  },
  takePhotoBtn: {
    marginTop: 20,
  },
  chosePhoto: {
    marginTop: 10,
    fontSize: 20,
    fontWeight: 'bold',
    color: '#28A0F9',
  },
});
