export type ImageBackDropProps = {
  isHidden?: boolean;
  isLoading?: boolean;
  onChange?: (val) => void;
};
