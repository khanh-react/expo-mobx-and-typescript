import Storage from '@react-native-community/async-storage';
import { PROFILE_PAGE_KEY } from './types';

export const getProfileInitialed = async () => {
  const res = await Storage.getItem(PROFILE_PAGE_KEY) as any;
  if (res) return true;
  return false;
};

export const setProfileInitialed = async () => {
  const set = await Storage.setItem(PROFILE_PAGE_KEY, new Date().getTime().toString());
  return set;
};
