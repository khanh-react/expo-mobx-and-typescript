export type SignUpProps = {
  email: string,
  phone: string,
  fullname: string,
  password: string,
  isLoading?: boolean,
  onUpdateInput?: (e) => void,
  onRequestSignUp?: (e) => void,
};
