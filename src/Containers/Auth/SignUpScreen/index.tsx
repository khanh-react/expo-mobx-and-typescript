import React, { useState, useEffect, memo } from 'react';
import { View, TouchableOpacity, Keyboard } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import SuccessPage from '../SignInSuccess';
import Styles from './styles';
import { SignUpProps as Props } from './types';
import ThemeColors from '../../../Theme/Colors';
import Button from '../../../Components/Button';
import Text from '../../../Components/Text';
import Input from '../../../Components/Input';
import Checkbox from '../../../Components/Checkbox';
import useStores from '../../../Helpers/UseStores';

export const SignUp = (props: Props) => {
  const [isKeyboarding, setKeyboarding] = useState(true);
  const [isSuccess, setSuccess] = useState(false);
  const [isSeeGuide, setSeeGuide] = useState(true);
  const navigation = useNavigation();

  const { fullname, phone, email, password, isLoading,
    updateFullname, updateEmail, updatePhone, updatePassword, onLoading } =
    useStores(store => ({
      phone: store.auth.phone,
      email: store.auth.email,
      password: store.auth.password,
      fullname: store.auth.fullname,
      isLoading: store.auth.isLoading,
      updateFullname: store.auth.updateFullname,
      updateEmail: store.auth.updateEmail,
      updatePhone: store.auth.updatePhone,
      updatePassword: store.auth.updatePassword,
      onLoading: store.auth.onLoading,
    } as any));

  const handleNext = () => {
    onLoading();
    setTimeout(() => {
      setSuccess(true);
      onLoading(false);
    }, 2000);
  };

  useEffect(() => {
    Keyboard.addListener('keyboardDidShow', () => setKeyboarding(false));
    Keyboard.addListener('keyboardDidHide', () => setKeyboarding(true));
    return () => {
      Keyboard.removeListener('keyboardDidShow', () => setKeyboarding(true));
      Keyboard.removeListener('keyboardDidHide', () => setKeyboarding(true));
    };
  }, []);
  if (isSuccess) return <SuccessPage />;
  return (
    <View style={Styles.wrapper}>
      <View style={Styles.head}>
        <Text type="bold" size="large" value="Đăng Ký" />
        <Text color={ThemeColors.red} value="Bạn chưa có tài khoản? Vui lòng đăng ký ở bên dưới" />
        <TouchableOpacity style={Styles.options} onPress={() => navigation.navigate('SignIn')}>
          <Text color={ThemeColors.main} style={[Styles.optionsText]} value="Login" />
        </TouchableOpacity>
      </View>
      <View style={Styles.body}>
        <Input
          isAlignLeft
          value={fullname}
          placeholder="Họ và tên *"
          onChangeText={value => updateFullname(value)}
        />
        <Input
          isAlignLeft
          value={phone}
          type="number"
          placeholder="Di động *"
          onChangeText={value => updatePhone(value)}
        />
        <Input
          isAlignLeft
          value={email}
          placeholder="Email *"
          onChangeText={value => updateEmail(value)}
        />
        <Input
          isAlignLeft
          value={password}
          placeholder="Mật khẩu *"
          type="password"
          onChangeText={value => updatePassword(value)}
        />
        <Text color="silver" style={Styles.passwordValid} value="Your password needs to be at least 6 characters." />
        <View style={Styles.quickView}>
          <Checkbox isChecked={isSeeGuide} onPress={() => setSeeGuide(!isSeeGuide)} />
          <Text value="Xem nhanh hướng dẫn sử dụng" />
        </View>
      </View>
      {isKeyboarding && <View style={Styles.foot}>
        <Button
          text={'Đăng Ký'}
          isLoading={isLoading}
          disabled={!fullname || !phone || !email || !password}
        onPress={() => handleNext()}
        ></Button>
      </View>}
    </View>
  );
};

export default SignUp;
