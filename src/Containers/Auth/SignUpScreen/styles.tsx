import { StyleSheet } from 'react-native';
import ThemeHelpers from '../../../Theme/Helpers';
import ThemeColors from '../../../Theme/Colors';

export default StyleSheet.create({
  wrapper: {
    ...ThemeHelpers.crossCenter,
    ...ThemeHelpers.fill,
    ...ThemeHelpers.padding,
  },
  head: {
    ...ThemeHelpers.colCross,
    width: '100%',
    minHeight: 60,
  },
  headOptions: {
    width: '100%',
    height: 40,
    position: 'relative',
  },
  options: {
    width: 80,
    height: '100%',
    position: 'absolute',
    right: 10,
    top: 5,
    zIndex: 10,
  },
  optionsText: {
    textAlign: 'right',
    color: ThemeColors.main,
    textDecorationLine: 'underline',
  },
  body: {
    width: '100%',
  },
  foot: {
    width: '100%',
    minHeight: 100,
    position: 'absolute',
    bottom: 0,
  },
  passwordValid: {
    marginTop: -5,
  },
  quickView: {
    ...ThemeHelpers.row,
    alignItems: 'center',
  },
  quickViewText: {
    fontSize: 16,
  },
  red: {
    color: ThemeColors.red,
  },
  silver: {
    color: ThemeColors.silver,
  },
});
