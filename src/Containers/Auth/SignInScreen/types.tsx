export type SignInProps = {
  id: string;
  isLoading?: boolean;
  onUpdatePassword?: (e) => void;
  onUpdateLoading?: (e) => void;
};
