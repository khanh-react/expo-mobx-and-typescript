import React, { useState, useEffect } from 'react';
import { View, Image, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import AsyncStorage from '@react-native-community/async-storage';
import Checkbox from 'react-native-check-box';
import Styles from './styles';
import { SignInProps } from './types';
import ThemeColors from '../../../Theme/Colors';
import Button from '../../../Components/Button';
import Text from '../../../Components/Text';
import Input from '../../../Components/Input';
import BackDrop from '../../../Components/BackDrop';
import { useStores } from '../../../Helpers/UseStores';
import { getProfileImage, setProfileImage } from '../../../Helpers/LocalStorage';

export const SignIn = (): React.ReactElement => {
  const [remembered, setRemembered] = useState(false);
  const [opened, setOpened] = useState(false);
  const navigation = useNavigation();
  const { isLoading, fullname, email, phone, password, avatar,
    setEmail, setPassword, setLoading, updateCurrentPassword,
    login,
  } =
  useStores(store => ({
    isLoading: store.auth.isLoading,
    avatar: store.auth.avatar,
    fullname: store.auth.fullname,
    email: store.auth.email,
    phone: store.auth.phone,
    password: store.auth.password,
    setEmail: store.auth.updateEmail,
    setPassword: store.auth.updatePassword,
    setLoading: store.auth.onLoading,
    updateCurrentPassword: store.auth.updateCurrentPassword,
    login: store.auth.doLogin,
  } as any));

  const keyboardIcon = require('../../../Assets/keyboard_icon.png');

  useEffect(() => {
    AsyncStorage.getItem('password')
      .then((_password) => {
        if (_password) {
          setRemembered(true);
          setPassword(_password);
        }
      });
  }, [remembered]);

  const handleLogin = async () => {
    const forward = await login();
    if (forward) {
      navigation.navigate(forward);
    }
  };

  const handleCheckRemember = async () => {
    if (remembered) {
      setRemembered(false);
      await AsyncStorage.removeItem('password');
      return;
    }
    await AsyncStorage.setItem('password', password);
    setRemembered(true);
  };

  const friendlyFullName = () => {
    let friendlyName = '';
    const splitted = fullname.split(' ');
    if (splitted.length >= 3) {
      splitted.splice(0, 1);
    }
    splitted.forEach((e: string, index: number) => {
      if (index === splitted.length - 1) {
        friendlyName += e;
      } else {
        friendlyName += e + ' ';
      }
    });
    return friendlyName;
  }

  return (
    <View style={Styles.wrapper}>
      <View style={Styles.head}>
        <Text type="bold" size="large" value="Đăng Nhập Bestaff" color={ThemeColors.main} />
        <TouchableOpacity style={Styles.options} onPress={() => setOpened(true)}>
          <Text style={[Styles.textOptions, opened && Styles.modalOpened]} value="..." />
        </TouchableOpacity>
      </View>
      <View style={Styles.body}>
        <Text style={Styles.mrb} size="xsmall" type="bold" value={`Chào ${friendlyFullName()}, vui lòng nhập mật khẩu !`} />
        <Input
          isAlignLeft
          isFocused
          type="password"
          value={password}
          placeholder="Mật khẩu"
          onChangeText={value => setPassword(value)} />
        <View style={Styles.remember}>
          <Checkbox
            onClick={() => handleCheckRemember()}
            value={remembered}
            isChecked={remembered}
            uncheckedCheckBoxColor={ThemeColors.black}
            checkedCheckBoxColor={ThemeColors.main} />
          <Text value="Ghi nhớ mật khẩu" />
        </View>
      </View>
      <View style={Styles.foot}>
        <Button isLoading={isLoading} text="Đăng nhập" onPress={() => handleLogin()}></Button>
        <TouchableOpacity>
          <Text style={Styles.forget} value="Quên mật khẩu ?" />
        </TouchableOpacity>
      </View>
      <BackDrop isOpened={opened} onPress={() => setOpened(false)}>
        <View style={Styles.headOptions}></View>
        <View style={Styles.optionsBody}>
          <TouchableOpacity onPress={() => navigation.navigate('SignInByPin')}>
            <View style={Styles.optionsItem}>
              <Text style={Styles.optionsItemText} value="Chấm công bằng mã pin" />
              <Image source={keyboardIcon} width={20} height={20} />
            </View>
          </TouchableOpacity>
        </View>
      </BackDrop>
    </View>
  );
};

export default SignIn;
