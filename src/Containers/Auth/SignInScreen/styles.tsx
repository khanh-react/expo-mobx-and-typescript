import { StyleSheet } from 'react-native';
import ThemeHelpers from '../../../Theme/Helpers';
import ThemeColors from '../../../Theme/Colors';

export default StyleSheet.create({
  wrapper: {
    ...ThemeHelpers.crossCenter,
    ...ThemeHelpers.fill,
    ...ThemeHelpers.padding,
    backgroundColor: '#fff',
  },
  head: {
    ...ThemeHelpers.colCenter,
    width: '100%',
    height: 40,
  },
  headOptions: {
    width: '100%',
    height: 40,
    position: 'relative',
  },
  options: {
    position: 'absolute',
    right: -10,
    width: 40,
    height: '100%',
    zIndex: 10,
    justifyContent: 'center',
    borderRadius: 20,
  },
  textOptions: {
    color: '#000000',
    fontSize: 18,
    fontWeight: 'bold',
    marginTop: -13,
    textAlign: 'right',
  },
  contentOptions: {
    height: 100,
  },
  optionsBody: {
    width: '100%',
    alignItems: 'flex-end',
  },
  optionsItem: {
    minWidth: 200,
    padding: 10,
    backgroundColor: '#ffffff',
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
  },
  optionsItemText: {
    marginRight: 10,
  },
  body: {
    width: '100%',
    height: 400,
    marginTop: 40,
  },
  foot: {
    width: '100%',
    minHeight: 100,
    position: 'absolute',
    bottom: 0,
  },
  remember: {
    ...ThemeHelpers.rowCross,
    marginTop: 10,
  },
  forget: {
    textAlign: 'center',
    marginTop: 10,
  },
  close: {
    position: 'absolute',
    right: 10,
    top: 5,
    fontSize: 24,
    color: '#28A0F9',
  },
  modalOpened: {
    color: ThemeColors.main,
  },
  mainColor: {
    color: ThemeColors.main,
  },
  mrb: {
    marginBottom: 10,
  }
});
