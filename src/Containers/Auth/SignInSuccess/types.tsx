export type SliderProps = {
  isVisible: boolean,
  currentPage: number,
};
