import React from 'react';
import { StyleSheet, View } from 'react-native';
import Text from '../../../Components/Text';

const styles = StyleSheet.create({
  wrapper: {
    width: '80%',
    marginTop: -150,
  },
  title: {
    marginBottom: 5,
  },
});

type SuccessInitProps = {
  isVisible: boolean;
};

export const SuccessInit = ({ isVisible }: SuccessInitProps) => {
  if (!isVisible) return null;
  return (
    <View style={styles.wrapper}>
      <Text size="xxlarge" type="xbold" style={styles.title} value="Chúc mừng bạn đã tạo tài khoản thành công!" />
      <Text size="medium" value="Chúng tôi xin chân thành xin phép tạo quảng cáo!" />
    </View>
  );
};

export default SuccessInit;
