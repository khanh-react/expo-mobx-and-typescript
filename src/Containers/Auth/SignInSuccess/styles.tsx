import { StyleSheet } from 'react-native';
import ThemeHelpers from '../../../Theme/Helpers';

export default StyleSheet.create({
  wrapper: {
    ...ThemeHelpers.fillColCenter,
    ...ThemeHelpers.padding,
    backgroundColor: '#ffffff',
  },
  foot: {
    width: '100%',
    height: 120,
    position: 'absolute',
    bottom: 0,
  },
  footWrapper: {
    ...ThemeHelpers.column,
    width: '100%',
    height: '100%',
  },
  footPaginate: {
    marginBottom: 15,
  },
});
