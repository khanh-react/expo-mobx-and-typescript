import React, { useState, useEffect } from 'react';
import { View } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import GestureRecognizer from 'react-native-swipe-gestures';
import SignUpSuccessStyles from './styles';
import InitPage from './Init';
import SliderPage from './Slider';
import Button from '../../../Components/Button';
import Pagination from '../../../Components/Pagination';
import { getCurrentUserID, getToken, getCurrentUserInfo } from '../../../Helpers/LocalStorage';
import { useStores } from '../../../Helpers/UseStores';

export const SignUpSuccess = () => {
  const [step, setStep] = useState(1);
  const navigation = useNavigation();

  const handleNext = (newStep: number) => {
    if (newStep === 5) {
      return navigation.navigate('Profile');
    }
    setStep(newStep);
  };

  const handleSwipe = (way: string) => {
    if (way === 'NEXT') {
      if (step < 4) { setStep(step + 1); }
    }
    if (way === 'BACK') {
      if (step > 1) { setStep(step - 1); }
    }
  };

  useEffect(() => {
    // getToken().then(e => alert(e)).catch(e => alert(JSON.stringify(e)))
  })

  return (
    <GestureRecognizer
      style={SignUpSuccessStyles.wrapper}
      onSwipeLeft={() => handleSwipe('NEXT')}
      onSwipeRight={() => handleSwipe('BACK')}>
      <InitPage isVisible={step === 1} />
      <SliderPage isVisible={step > 1 && step < 5} currentPage={step - 2} />
      <View style={SignUpSuccessStyles.foot}>
        <View style={SignUpSuccessStyles.footWrapper}>
          <View style={SignUpSuccessStyles.footPaginate}><Pagination pages={4} currentPage={step} /></View>
          <Button text="Tiếp" onPress={() => handleNext(step + 1)} />
        </View>
      </View>
    </GestureRecognizer>
  );
};

export default SignUpSuccess;
