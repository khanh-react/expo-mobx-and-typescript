import { StyleSheet } from 'react-native';
import ThemeHelpers from '../../../Theme/Helpers';

export default StyleSheet.create({
  wrapper: {
    ...ThemeHelpers.fillColCross,
    width: '100%',
  },
  head: {
    ...ThemeHelpers.colCenter,
    width: '100%',
    height: 65,
  },
  headTitle: {
    textAlign: 'center',
  },
  body: {
    ...ThemeHelpers.colCenter,
    width: '100%',
    height: 350,
  },
  bodyBanner: {
    maxWidth: '80%',
  },
  foot: {
    ...ThemeHelpers.colCenter,
    padding: 20,
  },
  footTitle: {
    fontWeight: 'bold',
  },
  footSubTitle: {
    marginTop: 20,
  },
});
