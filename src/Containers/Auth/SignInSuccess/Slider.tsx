import React from 'react';
import { View, Image } from 'react-native';
import { SliderProps } from './types';
import Styles from './SliderStyles';
import Text from '../../../Components/Text';

export const SliderPage = ({ isVisible, currentPage }: SliderProps) => {
  if (!isVisible) return null;
  const banner1 = require('../../InitScreen/Images/banner_1.png');
  const banner2 = require('../../InitScreen/Images/banner_2.png');
  const banner3 = require('../../InitScreen/Images/banner_3.png');

  const DUMP_DATA = [
    {
      banner: banner1,
      title: 'Khai báo lịch bận',
      subTitle: 'Khai báo lịch bận của bạn và hệ thống sẽ tự động sắp xếp cho phù hợp',
    },
    {
      banner: banner2,
      title: 'Chấm công',
      subTitle: 'Chấm công là bạn phải thực hiện chấm công theo nghị định của chính phủ Mỹ',
    }, {
      banner: banner3,
      title: 'Đổi ca, nhận ca',
      subTitle: 'Bạn bận việc đột ngột? Đừng lo hãy thực hiện khai báo đổi ca ngay và luôn',
    },
  ];

  return (
    <View style={Styles.wrapper}>
      <View style={Styles.head}>
        <Text style={Styles.headTitle} type="bold" value="HƯỚNG DẪN SỬ DỤNG NHANH BESTAFF" />
      </View>
      <View style={Styles.body}>
        <Image style={Styles.bodyBanner} source={DUMP_DATA[currentPage].banner} />
      </View>
      <View style={Styles.foot}>
        <Text type="xbold" value={DUMP_DATA[currentPage].title}></Text>
        <Text value={DUMP_DATA[currentPage].subTitle}></Text>
      </View>
    </View>
  );
};

export default SliderPage;
