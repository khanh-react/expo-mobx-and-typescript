import { StyleSheet } from 'react-native';
import ThemeHelpers from '../../../Theme/Helpers';
import ThemeColors from '../../../Theme/Colors';

export default StyleSheet.create({
  wrapper: {
    ...ThemeHelpers.fillColCross,
  },
  head: {
    ...ThemeHelpers.colCenter,
    width: '100%',
    height: 100,
  },
  headDescription: {
    fontSize: 18,
    lineHeight: 22,
    fontWeight: '600',
    color: '#28A0F9',
  },
  headOptions: {
    width: '100%',
    height: 40,
    position: 'relative',
  },
  options: {
    justifyContent: 'center',
    position: 'absolute',
    right: 10,
    top: 0,
    width: 40,
    height: 40,
    borderRadius: 20,
    zIndex: 10,
  },
  textOptions: {
    color: '#000000',
    fontSize: 20,
    fontWeight: 'bold',
    marginTop: -13,
    textAlign: 'right',
  },
  contentOptions: {
    height: 100,
  },
  optionsBody: {
    width: '100%',
    alignItems: 'flex-end',
  },
  optionsItem: {
    minWidth: 200,
    padding: 10,
    backgroundColor: '#ffffff',
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
  },
  optionsItemText: {
    marginRight: 10,
  },
  cameraArea: {
    ...ThemeHelpers.colCenter,
    width: '100%',
    height: 280,
  },
  inputPinWrapper: {
    alignItems: 'center',
  },
  inputPinTitle: {
    marginBottom: 10,
  },
  inputPinError: {
    marginTop: 10,
  },
  modalOpened: {
    color: ThemeColors.main,
  },
  color: {
    color: ThemeColors.main,
  },
  red: {
    color: ThemeColors.red,
  },
});
