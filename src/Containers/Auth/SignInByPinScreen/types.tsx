export type SignInByPinProps = {
};

export type PinCodeProps = {
  pin: number,
  onUpdatePin: (val) => void,
};
