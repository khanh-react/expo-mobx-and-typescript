import React, { useRef } from 'react';
import { StyleSheet, Text, Dimensions } from 'react-native';
import { PinCodeProps as Props } from './types';
import SmoothPinCodeInput from 'react-native-smooth-pincode-input';
import ThemeColors from '../../../Theme/Colors';

const styles = StyleSheet.create({
  cell: {
    borderWidth: 2,
    borderColor: 'silver',
    borderRadius: 8,
    backgroundColor: '#eeeeee',
    marginRight: 5,
  },
  cellFocused: {
    borderColor: ThemeColors.main,
  },
  mask: {
    width: 15,
    height: 15,
    borderRadius: 7,
    fontWeight: 'bold',
    backgroundColor: '#000000',
  },
});

export const PinCode = ({ pin, onUpdatePin }: Props): React.ReactElement => {
  const pinValue = pin ? pin.toString() : '';
  return (
    <React.Fragment>
      <SmoothPinCodeInput
        password
        mask={<Text style={styles.mask} />}
        cellSize={(Dimensions.get('screen').width / 7) - 10}
        cellStyle={styles.cell}
        cellStyleFocused={styles.cellFocused}
        codeLength={7}
        value={pinValue}
        onTextChange={value => onUpdatePin(value)} />
    </React.Fragment>
  );
};

export default PinCode;
