import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import ThemeColors from '../../../Theme/Colors';

const styles = StyleSheet.create({
  wrapper: {
    width: 240,
    height: 240,
    borderWidth: 3,
    borderRadius: 120,
    borderColor: ThemeColors.main,
    alignItems: 'center',
    justifyContent: 'center',
  },
  childWrapper: {
    width: 225,
    height: 225,
    borderWidth: 1,
    borderRadius: 120,
    borderColor: ThemeColors.main,
  },
});

export const Camera = () => {
  return (
    <View style={styles.wrapper}>
      <View style={styles.childWrapper} />
    </View>
  );
};

export default Camera;
