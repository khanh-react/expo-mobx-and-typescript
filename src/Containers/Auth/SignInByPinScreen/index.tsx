import React, { useState } from 'react';
import { View, TouchableOpacity, Image } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import PinCode from './PinCode';
import Styles from './styles';
import { SignInByPinProps } from './types';
import Camera from './Camera';
import Text from '../../../Components/Text';
import BackDrop from '../../../Components/BackDrop';

export const SignInByPin = (props: SignInByPinProps): React.ReactElement => {
  const [pincode, setPincode] = useState(null);
  const [valid, setValid] = useState(false);
  const [opened, setOpened] = useState(false);
  const navigation = useNavigation();
  const keyboardIcon = require('../../../Assets/keyboard_icon.png');

  const handlePinCodeChange = (value: string) => {
    // tslint:disable-next-line:radix
    setPincode(parseInt(value));
    if (value && value.length === 7) {
      if (value === '1111111') {
        return navigation.navigate('SignUp');
      }
      setTimeout(() => {
        setPincode(null);
        setValid(true);
      }, 500);
    } else {
      setValid(false);
    }
  };
  return (
    <View style={Styles.wrapper}>
      <View style={Styles.head}>
        <Text size="large" type="bold" value="Leevins Coffee" />
        <Text type="bold" value="10 Trần Phú, Đà Nẵng" />
        <Text color={Styles.color} value="06:55 - Thứ 6, ngày 13, tháng 12" />
        <TouchableOpacity style={Styles.options} onPress={() => setOpened(true)}>
          <Text style={[Styles.textOptions, opened && Styles.modalOpened]} value="..." />
        </TouchableOpacity>
      </View>
      <View style={Styles.cameraArea}>
        <Camera />
      </View>
      <View style={Styles.inputPinWrapper}>
        <Text style={Styles.inputPinTitle} value="Nhập mã Pin của bạn:" />
        <PinCode pin={pincode} onUpdatePin={value => handlePinCodeChange(value)} />
        {valid && <Text color={Styles.red} style={Styles.inputPinError} value="Mã pin không đúng vui lòng thử lại" />}
      </View>

      <BackDrop isOpened={opened} onPress={() => setOpened(false)} marginTop={100}>
        <View style={Styles.headOptions}></View>
        <View style={Styles.optionsBody}>
          <TouchableOpacity onPress={() => navigation.navigate('SignIn')}>
            <View style={Styles.optionsItem}>
              <Text style={Styles.optionsItemText} value="Đăng nhập bằng Email / SĐT" />
              <Image source={keyboardIcon} width={20} height={20} />
            </View>
          </TouchableOpacity>
        </View>
      </BackDrop>
    </View>
  );
};

export default SignInByPin;
