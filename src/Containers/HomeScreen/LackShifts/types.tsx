export type LackShiftProps = {
    shifts: [];
    onGetCurrentShiftIndex: (index: number) => void;
};
