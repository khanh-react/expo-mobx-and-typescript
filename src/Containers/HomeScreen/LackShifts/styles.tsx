import { StyleSheet } from 'react-native';
import ThemeHelpers from '../../../Theme/Helpers';
import { screenWidth } from '../Dimentions';

export const Styles = StyleSheet.create({
  wrapper: {
    width: '100%',
    maxHeight: 200,
    marginBottom: 10,
    marginTop: 19,
  },
  head: {
    width: '100%',
    position: 'relative',
    justifyContent: 'center',
    marginBottom: 10,
  },
  seeAll: {
    position: 'absolute',
    right: 5,
  },
  body: {
    ...ThemeHelpers.row,
    width: screenWidth,
    height: 300,
    marginTop: 5,
  },
});

export default Styles;
