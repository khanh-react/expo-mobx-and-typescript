export type LackShiftItemProps = {
    index: number;
    shift: {
        day: string;
        name_day: string;
        data_item: any[];
    }
    onGetShiftItemIndex: (index: number) => void;
};
