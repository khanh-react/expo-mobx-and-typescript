import React from 'react';
import PropTypes from 'prop-types';
import { View, TouchableOpacity, Image } from 'react-native';
import { Styles } from './styles';
import { LackShiftItemProps as Props } from './types';
import { IText as Text } from '../../../../Components/Text';
import { Icon } from '../../../../Components/Icon';
import { friendlyDayName, shortDayName, dateMonthFriendly, isFriendlyDay } from '../../../../Helpers/Converter';

export const LackShiftItem = ({ shift, index, onGetShiftItemIndex }: Props) => {
  const clockIcon = require('../../../../Assets/mock/clock_red_symboi.png');
  const locationIcon = require('../../../../Assets/mock/location_red_symboi.png');

  return (
    <>
    {
      shift.data_item.map((item, index) => {
        if (friendlyDayName(shift.name_day) === 'Hôm qua') return null;
        return (
          <TouchableOpacity key={`lack_shift_${index}`} style={Styles.wrapper} onPress={() => onGetShiftItemIndex(index)}>
            <View style={Styles.date}>
              { isFriendlyDay(shift.name_day) &&
                <>
                  <Text color="#FFF" style={Styles.dateText} size="medium" value={friendlyDayName(shift.name_day)} />
                  <View style={Styles.breakLine} />
                </>
              }
              <View style={[Styles.dateTextWrap, !isFriendlyDay(shift.name_day) && Styles.mrt25]}>
                <Text color="#FFF" style={Styles.dateText} size="xxlarge" value={shortDayName(shift.name_day)} />
                <Text color="#FFF" style={Styles.dateText} size="xlarge" value={dateMonthFriendly(shift.day)} />
              </View>
            </View>
            <View style={Styles.content}>
              <View style={Styles.contentItem}>
                <Image style={{ marginRight: 5 }} source={clockIcon} />
                <Text style={{ marginRight: 5, color: '#FF6B6B' }} size="xsmall" value={`${item['start_time']} - ${item['end_time']}`} />
              </View>
              <View style={Styles.contentItem}>
                <Image style={{ marginRight: 5 }} source={locationIcon} />
                <Text style={{ marginRight: 5, color: '#FF6B6B' }} size="xsmall" value="134 N.T.M.Khai" />
              </View>
              <View style={Styles.contentItem}>
                <View style={Styles.roleIcon} />
                <Text style={{ marginRight: 5, color: 'green' }} size="xsmall" value={item.data_position[0]['name_position']} />
              </View>
            </View>
          </TouchableOpacity>
        )
      })
    }
    </>
  );
};

LackShiftItem.propTypes = {
  title: PropTypes.string,
};

export default LackShiftItem;
