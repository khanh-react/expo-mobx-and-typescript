import { StyleSheet, Dimensions } from 'react-native';
import ThemeHelpers from '../../../../Theme/Helpers';
import ThemeColors from '../../../../Theme/Colors';

const { width } = Dimensions.get('screen');
const itemWidth = width * 0.7;
const itemThumbWidth = width * 0.24;

export const Styles = StyleSheet.create({
  wrapper: {
    ...ThemeHelpers.row,
    ...ThemeHelpers.shadow,
    width: itemWidth,
    height: 145,
    marginRight: 10,
    borderRadius: 10,
    backgroundColor: '#eeeeee',
  },
  date: {
    ...ThemeHelpers.colCross,
    width: itemThumbWidth,
    height: '100%',
    backgroundColor: '#FF6B6B',
    borderRadius: 10,
    paddingTop: 5,
  },
  dateTextWrap: {
    ...ThemeHelpers.colCenter,
  },
  dateText: {
    color: '#ffffff',
  },
  breakLine: {
    width: 60,
    height: 1,
    borderWidth: 0.1,
    borderColor: '#ffffff',
    backgroundColor: '#ffffff',
    color: '#ffffff',
  },
  content: {
    width: '60%',
    height: '100%',
    padding: 10,
  },
  contentItem: {
    ...ThemeHelpers.rowCross,
    marginBottom: 10,
  },
  roleIcon: {
    width: 8,
    height: 8,
    marginRight: 5,
    backgroundColor: 'green',
    borderRadius: 7,
  },
  white: {
    color: ThemeColors.white,
  },
  mrt25: {
    marginTop: 25,
  }
});

export default Styles;
