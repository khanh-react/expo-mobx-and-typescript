import React from 'react';
import { View, ScrollView } from 'react-native';
import { Styles } from './styles';
import { LackShiftProps as Props } from './types';
import { LackShiftItem } from './LackShiftItem';
import { screenWidth, ratio } from '../Dimentions';
import { IText as Text} from '../../../Components/Text';
import { getDayNameFromDayNumber } from '../../../Helpers/Converter';

export const LackShifts = ({ shifts, onGetCurrentShiftIndex }: Props) => {
  const items = [] as any;
  const shiftItems = [] as any;

  // remove shifts in the past
  const today = new Date().getDay();
  const todayName = getDayNameFromDayNumber(!today ? 7 : today);
  const currentIndex = shifts.findIndex((i: any) => i.name_day === todayName);
  const _shifts = [...shifts].splice(currentIndex, shifts.length);

  _shifts.map((item, index) => {
    if ((item as any).data_item.length) {
      shiftItems.push(1);
    }
    items.push(<LackShiftItem key={`lackshift_${index}`} shift={item} index={index} onGetShiftItemIndex={onGetCurrentShiftIndex}/> )
  });
  return (
    <View style={Styles.wrapper}>
      <View style={Styles.head}>
        <Text size="large" type="bold" value="Ca thiếu người"/>
        <View style={Styles.seeAll}>
          {/* <Text size="smaller" value="Xem tất cả" /> */}
        </View>
      </View>
      {
        shiftItems.length ?
        <ScrollView style={Styles.body} horizontal={true} snapToInterval={screenWidth * ratio}>
          {items}
        </ScrollView>
        : <Text value="Chưa có lịch"/>
      }
    </View>
  );
};

export default LackShifts;
