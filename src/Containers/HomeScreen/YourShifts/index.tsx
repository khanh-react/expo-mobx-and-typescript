import React from 'react';
import { View, ScrollView } from 'react-native';
import Styles from './styles';
import { YourShiftProps as Props } from './types';
import { ShiftItem } from './ShiftItem';
import { CheckInCheckOut } from './CheckInCheckOut';
import { screenWidth, ratio } from '../Dimentions';
import { IText as Text } from '../../../Components/Text';
import { getDayNameFromDayNumber } from '../../../Helpers/Converter';

export const YourShifts = ({ id, token, branchId, address, shifts, minDay }: Props) => {
  const items = [] as any;
  const shiftItems = [] as any;

  // remove shifts in the past
  const today = new Date().getDay();
  const todayName = getDayNameFromDayNumber(!today ? 7 : today);
  const currentIndex = shifts.findIndex((i: any) => i.name_day === todayName);
  const _shifts = [...shifts].splice(currentIndex, shifts.length);

  _shifts.map((item, index) => {
    if ((item as any).data_item.length) {
      shiftItems.push(1);
    }
    items.push(<ShiftItem key={`shift_${index}`} shift={item} myID={id} address={address}/> )
  });

  return (
    <View style={Styles.wrapper}>
      <View style={Styles.head}>
        <View style={Styles.headX}>
          <Text size="large" type="bold" value="Ca làm của bạn" />
          <CheckInCheckOut userId={id} token={token} branchId={branchId}/>
        </View>
        <View style={Styles.seeAll}>
          {/* <Text size="smaller" value="Xem tất cả" /> */}
        </View>
      </View>
      {
        shiftItems.length ?
        <ScrollView style={Styles.body} horizontal={true} snapToInterval={screenWidth * ratio}>
          {items}
        </ScrollView> :
        <Text value="Chưa có lịch"/>
      }
    </View>
  );
};

export default YourShifts;
