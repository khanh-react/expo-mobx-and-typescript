export type YourShiftProps = {
    id: number;
    token: string;
    branchId: string;
    address: string;
    minDay: number;
    shifts: [];
}