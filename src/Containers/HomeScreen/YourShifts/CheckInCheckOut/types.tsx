export type CheckinProps = {
    userId: string;
    token: string;
    branchId: string;
    value?: 'CHECKIN' | 'CHECKOUT',
}

export type CHECKIN_STATUS = 'CHECKIN' | 'CHECKOUT';
