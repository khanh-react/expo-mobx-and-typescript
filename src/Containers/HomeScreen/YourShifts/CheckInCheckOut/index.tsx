import React, { useState, useEffect } from 'react';
import { TouchableOpacity, Image } from 'react-native';
import { Styles } from './styles';
import { CheckinProps } from './types';
import { CHECKIN_STATUS } from './types';
import { IText as Text } from '../../../../Components/Text';
import { Icon } from '../../../../Components/Icon';
import { useStores } from '../../../../Helpers/UseStores';
import CheckInSvg from '../../../../Assets/icons/checkin';
import CheckOutSvg from '../../../../Assets/icons/logout';

export const CheckInCheckOut = ({ userId, token, branchId }: CheckinProps) => {
    const { checkIn, checkOut, isLoading, checkInStatus, getCurrentCheckInStatus } = useStores(store => ({
      checkIn: store.shift.doCheckIn,
      checkOut: store.shift.doCheckOut,
      isLoading: store.shift.checkInLoading,
      checkInStatus: store.shift.currentCheckInStatus,
      getCurrentCheckInStatus: store.shift.getCurrentCheckInStatus
    } as any));

    const checkinIcon = require('../../../../Assets/clock.png');
    const checkoutIcon = require('../../../../Assets/checkout.png');
    const completeIcon = require('../../../../Assets/complete.png');
    const loadingIcon = require('../../../../Assets/loading.gif');

    const handleCheckInCheckOut = async () => {
        if (checkInStatus === 'CHECKIN') {
            checkIn({ userId, token, branchId });
        }
        if (checkInStatus === 'CHECKOUT') {
            checkOut({ userId, token, branchId });
        }
    }

    useEffect(() => {
        getCurrentCheckInStatus(userId);
    }, [checkInStatus])

    return (
        <TouchableOpacity
            style={[
                Styles.wrapper,
                checkInStatus === 'CHECKIN' && Styles.checkin,
                checkInStatus === 'CHECKOUT' && Styles.checkout,
            ]}
            onPress={() => handleCheckInCheckOut()}
            >
            { isLoading && <Image style={Styles.loading} source={loadingIcon}/> }
            { !isLoading && checkInStatus === 'CHECKIN' && <CheckInSvg fill="#fff"/> }
            { !isLoading &&  checkInStatus === 'CHECKOUT' && <CheckOutSvg fill="#fff" /> }
            <Text style={Styles.mgl5} color="#FFF" type="bold" value={checkInStatus || 'CHECKIN'}/>
        </TouchableOpacity>
    )
}

export default CheckInCheckOut;
