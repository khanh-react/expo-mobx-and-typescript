import { StyleSheet } from 'react-native';
import Helpers from '../../../../Theme/Helpers';
import Colors from '../../../../Theme/Colors';

export const Styles = StyleSheet.create({
    wrapper: {
        ...Helpers.rowCenter,
        padding: 5,
        borderRadius: 10,
        marginLeft: 5,
        marginTop: 3,
    },
    checkin: {
        backgroundColor: Colors.main,
    },
    checkout: {
        backgroundColor: '#1fb141',
    },
    complete: {
        backgroundColor: '#1fb141',
    },
    loading: {
        width: 20,
        height: 20,
    },
    mgl5: {
        marginLeft: 3,
    }
})

export default Styles;
