import React from 'react';
import { View, Image } from 'react-native';
import { Styles } from './styles';
import { ShiftItemProps as Props } from './types';
import { IText as Text } from '../../../../Components/Text';
import { UserRole } from '../../../../Components/UserRole';
import { UserThumb } from '../../../../Components/UserThumb';
import { friendlyDayName, isFriendlyDay, dateMonthFriendly, shortDayName } from '../../../../Helpers/Converter';
import ClockSvg from '../../../../Assets/icons/clock';
import RainSvg from '../../../../Assets/icons/rain';
import LocationSvg from '../../../../Assets/icons/location';

export const ShiftItem = ({ shift, myID, address }: Props) => {
    return (
        <>
        {
            shift.data_item.map((item, index) => {
                if (item && item.data_user && item.data_user.length) {
                    const listUsers = [] as any;
                    const myIndex = item.data_user.findIndex((user: any) => user.user_id === myID);
                    item.data_user.map((user: any, uindex: number) => {
                        if (uindex < 4) {
                            listUsers.push(
                                <UserThumb key={`user_${uindex}`} name={user['full_name']} avatar={user['avatar']}/>
                            )
                        }
                        if (uindex === 4) {
                            listUsers.push(<Text key={`user_${uindex}`} value="..." />)
                        }
                    })
    
                    if (friendlyDayName(shift.name_day) === 'Hôm qua') return null;
    
                    if (myIndex >= 0)
                        return <View style={Styles.wrapper} key={`shift_item_${index}`}>
                            <View style={Styles.date}>
                                { isFriendlyDay(shift.name_day)
                                &&  <>
                                        <Text color="#FFF" style={Styles.dateText} size="medium" value={friendlyDayName(shift.name_day)} />
                                        <View style={Styles.breakLine} />
                                    </>
                                }
    
                                <View style={[Styles.dateTextWrap, !isFriendlyDay(shift.name_day) && Styles.mrt30]}>
                                    <Text color="#FFF" style={Styles.dateText} size="xxlarge" value={shortDayName(shift.name_day)} />
                                    <Text color="#FFF" style={Styles.dateText} size="xlarge" value={dateMonthFriendly(shift.day)} />
                                </View>
                            </View>
                            <View style={Styles.content}>
                                <View style={[Styles.contentItem, Styles.mrb6]}>
                                    <ClockSvg />
                                    <Text style={{ marginRight: 5, marginLeft: 5 }} size="xsmall" value={`${item['start_time']} - ${item['end_time']}`} />
                                    <RainSvg />
                                </View>
                                <View style={[Styles.contentItem, Styles.mrb3]}>
                                    <LocationSvg />
                                    <Text style={{ marginRight: 5, marginLeft: 5 }} size="smaller" value={address} />
                                </View>
                                <UserRole style={Styles.mrb6} roleName={item.data_user[myIndex]['name_position']}/>
                                <View style={Styles.contentItem}>{listUsers}</View>
                            </View>
                        </View>
                }
                return null;
            })
        }
        </>
    );
};

export default ShiftItem;
