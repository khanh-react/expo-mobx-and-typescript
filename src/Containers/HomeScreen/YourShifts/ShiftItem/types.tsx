export type ShiftItemProps = {
    shift: {
        day: string;
        name_day: string;
        data_item: any[];
    };
    myID: string | number;
    address: string;
}