import { StyleSheet } from 'react-native';
import ThemeHelpers from '../../../../Theme/Helpers';
import ThemeColors from '../../../../Theme/Colors';
import { screenWidth, ratio, thumbRatio } from '../../Dimentions';

export const Styles = StyleSheet.create({
    wrapper: {
        ...ThemeHelpers.row,
        ...ThemeHelpers.shadow,
        width: screenWidth * ratio,
        height: 145,
        marginRight: 25,
        borderRadius: 10,
        backgroundColor: '#eeeeee',
    },
    date: {
        ...ThemeHelpers.colCross,
        width: screenWidth * thumbRatio,
        height: '100%',
        backgroundColor: ThemeColors.main,
        borderRadius: 10,
    },
    dateTextWrap: {
        ...ThemeHelpers.colCenter,
    },
    dateText: {
        color: '#ffffff',
    },
    breakLine: {
        width: 60,
        height: 1,
        borderWidth: 0.1,
        borderColor: '#ffffff',
        backgroundColor: '#ffffff',
        color: '#ffffff',
    },
    content: {
        width: '60%',
        height: '100%',
        padding: 10,
    },
    contentItem: {
        ...ThemeHelpers.rowCross,
    },
    roleIcon: {
        width: 8,
        height: 8,
        marginRight: 5,
        backgroundColor: 'purple',
        borderRadius: 7,
    },
    mrb6: {
        marginBottom: 6,
    },
    mrb3: {
        marginBottom: 3,
    },
    mrt30: {
        marginTop: 25,
    }
})