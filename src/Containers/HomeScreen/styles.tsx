import { StyleSheet } from 'react-native';
import ThemeColors from '../../Theme/Colors';

export const Styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    paddingLeft: 20,
    backgroundColor: ThemeColors.white,
    position: 'relative',
  },
  body: {
    marginBottom: 65,
  },
  foot: {
    marginLeft: 10,
  },
  margin: {
    marginTop: 19,
  },
});

export default Styles;
