import React from 'react';
import { View } from 'react-native';
import { Styles } from './styles';
import { ChangeShiftProps as Props } from './types';
import Colors from '../../../Theme/Colors';
import Popup from '../../../Components/Popup';
import Text from '../../../Components/Text';
import Button from '../../../Components/Button';
import { Textarea } from '../../../Components/Textarea';

export const PopupChangeShift = ({ shift, onPopupClosed }: Props) => {
    if (shift)
        return (
            <Popup openAt={new Date().getTime()} onClosed={onPopupClosed}>
                <View style={Styles.wrapper}>
                    <Text style={Styles.title} size="xsmall" type="bold">
                        Xác nhận hoán đổi ca với
                        <Text color={Colors.main}> {shift?.name}</Text>
                    </Text>
                    <View style={Styles.row}>
                        <Text style={Styles.col1}>Từ ca:</Text>
                        <Text style={Styles.col2}>{shift?.from}</Text>
                    </View>
                    <View style={Styles.row}>
                        <Text style={Styles.col1}>Sang ca:</Text>
                        <Text style={Styles.col2}>{shift?.to}</Text>
                    </View>
                    <View style={Styles.row}>
                        <Text style={Styles.col1}>Tin nhắn:</Text>
                        <Text style={Styles.col2}>{shift?.message}</Text>
                    </View>
                    <View style={Styles.row}>
                        <Textarea style={Styles.mrt10} size="small" placeholder={`Để lại phản hồi của bạn cho ${shift?.name} ...`}/>
                    </View>
                    <View style={Styles.foot}>
                        <Button style={[Styles.footBtn, Styles.mgr10]} type="outline-primary" text="ĐỒNG Ý" />
                        <Button style={Styles.footBtn} type="outline-danger" text="TỪ CHỐI"/>
                    </View>
                </View>
            </Popup>
        )
    return null;
}