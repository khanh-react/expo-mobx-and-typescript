import { StyleSheet, Dimensions } from 'react-native';
import Helpers from '../../../Theme/Helpers';

const width = Dimensions.get('screen').width;

export const Styles = StyleSheet.create({
    wrapper: {
        width: '100%',
        position: 'relative'
    },
    title: {
        marginBottom: 10,
        textAlign: 'center'
    },
    row: {
        ...Helpers.row,
    },
    col1: {
        width: '25%',
    },
    col2: {
        width: '75%'
    },
    foot: {
        ...Helpers.rowCenter,
        position: 'absolute',
        width: '100%',
        height: 55,
        bottom: 10,
    },
    footBtn: {
        width: '40%',
    },
    mgr10: {
        marginRight: 10,
    },
    mrt10: {
        marginTop: 10,
    }
});

export default Styles;
