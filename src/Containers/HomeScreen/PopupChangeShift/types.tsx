import { WaitingAcceptModel } from '../WaitingAccept/types';

export type ChangeShiftProps = {
    shift?: WaitingAcceptModel;
    onPopupClosed: () => void;
};
