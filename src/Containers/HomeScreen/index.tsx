import React, { useEffect, useState } from 'react';
import { ScrollView, View } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import * as moment from 'moment';
import { Styles } from './styles';
import { Header } from './Header';
import { Posts } from './Posts';
import { YourShifts } from './YourShifts';
import { LackShifts } from './LackShifts';
import { WaitingAccept } from './WaitingAccept';
import { PopupChangeShift } from './PopupChangeShift';
import { WaitingAcceptModel } from './WaitingAccept/types';
import { useStores } from '../../Helpers/UseStores';

export const Home = () => {
  const { myId, token, branchId, address, shifts, posts, isPopupShown, currentNotifyOpenAt,
    currentPost, setCurrentPost, setCurrentShift, lackShifts, getLackShifts, getMyShifts,
    updatePopup, pushNoti, getShifts, getPosts, shiftErrorNotify, shiftSuccessNotify } =
  useStores(store => ({
    myId: store.auth.id,
    token: store.auth.token,
    branchId: store.auth.branchId,
    address: store.auth.branchAddress,
    shifts: store.shift.allShifts,
    lackShifts: store.shift.lackShifts,
    posts: store.post.posts,
    currentPost: store.post.currentPost,
    isPopupShown: store.toggle.isPopupShown,
    updatePopup: store.toggle.updatePopup,
    pushNoti: store.toggle.pushNotification,
    currentNotifyOpenAt: store.toggle.currentOpenAt,
    setCurrentPost: store.post.setCurrentPost,
    getPosts: store.post.getPosts,
    getShifts: store.shift.getShifts,
    getMyShifts: store.shift.getMyShifts,
    getLackShifts: store.shift.getLackShifts,
    setCurrentShift: store.shift.setCurrentShift,
    shiftErrorNotify: store.shift.errorNotify,
    shiftSuccessNotify: store.shift.successNotify,
  } as any));

  const [currentShift, setShift] = useState<WaitingAcceptModel>();
  const navigation = useNavigation();
  const timestamp = moment.now();
  const todayDay = new Date(timestamp).getDay();

  const handleClickOutsidePopup = () => {
    if (isPopupShown) updatePopup(false);
  }

  const handleGetShift = (shift: WaitingAcceptModel) => {
    setShift(shift);
    updatePopup();
  }

  const handleClosePopup = () => {
    setShift(null || undefined);
    updatePopup(false);
  }

  const handleGetCurrentPostIndex = (index: number) => {
    setCurrentPost(index);
    navigation.navigate('Post');
  }

  const handleGetCurrentShiftIndex = (index: number) => {
    setCurrentShift(1);
    // navigation.navigate('DetailShift');
  }

  useEffect(() => {
    getPosts();
    getShifts();
    getLackShifts();
  }, [])

  return (
    <View style={Styles.wrapper}>
      <Header/>
      <ScrollView style={Styles.body} onTouchEnd={handleClickOutsidePopup}>
        <Posts posts={posts} onGetCurrentPostIndex={handleGetCurrentPostIndex}/>
        <YourShifts
          id={myId}
          token={token}
          branchId={branchId}
          address={address}
          shifts={shifts}
          minDay={todayDay + 1}/>
        <LackShifts shifts={lackShifts} onGetCurrentShiftIndex={handleGetCurrentShiftIndex}/>
        {/* <WaitingAccept onGetShift={shift => handleGetShift(shift)}/> */}
      </ScrollView>
      <PopupChangeShift shift={currentShift} onPopupClosed={() => handleClosePopup()}/>
    </View>
  );
};

export default Home;
