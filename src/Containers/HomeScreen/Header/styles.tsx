import { StyleSheet } from 'react-native';
import Helpers from '../../../Theme/Helpers';
import Colors from '../../../Theme/Colors';

export const HeaderStyles = StyleSheet.create({
  wrapper: {
    height: 35,
    marginRight: 20,
    marginBottom: 10,
    marginTop: 10,
    justifyContent: 'center',
    position: 'relative',
  },
  thumb: {
    ...Helpers.rowCross,
  },
  optionsWrap: {
    ...Helpers.row,
    position: 'absolute',
    width: 20,
    top: 0,
    right: 0,
  },
  notificationCount: {
    color: '#ffffff',
    backgroundColor: Colors.red,
    position: 'absolute',
    borderRadius: 20,
    alignItems: 'center',
    padding: 2,
    paddingTop: -5,
    right: 10,
    top: 0,
  },
  minutes: {
    color: Colors.main,
  },
  avatar: {
    width: 24,
    height: 24,
  },
  mg5: {
    width: '85%',
    marginRight: 22,
    marginLeft: 5,
  },
  white: {
    color: Colors.white,
  },
  red: {
    color: Colors.red,
  },
});
