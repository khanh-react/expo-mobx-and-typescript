import React, { useEffect, useState } from 'react';
import { View, Image, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { HeaderStyles as Styles } from './styles';
import { useStores } from '../../../Helpers/UseStores';
import Icon from '../../../Components/Icon';
import { IText as Text } from '../../../Components/Text';
import { UserThumb } from '../../../Components/UserThumb';
import { getProfileImage } from '../../../Helpers/LocalStorage';

export const Header = (): React.ReactElement => {
  const [reminds, setReminds] = useState<any>([]);
  const { avatar, fullname, setAvatar, getRemind } = useStores(store => ({
    avatar: store.auth.avatar,
    setAvatar: store.auth.doUpdateAvatar,
    fullname: store.auth.fullname,
    getRemind: store.shift.getRemind,
  } as any));

  const navigation = useNavigation();
  const notificationIcon = require('../../../Assets/notification.png');

  useEffect(() => {
    getRemind().then((data: any) => setReminds(data));
  }, [])

  return (
    <View style={Styles.wrapper}>
      <View style={Styles.thumb}>
        <TouchableOpacity onPress={() => navigation.navigate('Profile')}>
          <UserThumb id={2} name={fullname} uri={avatar}/>
          {/* <Image style={[Styles.avatar, Styles.mg5]} source={{ uri: avatar }} /> */}
        </TouchableOpacity>
        <Text style={Styles.mg5} color="#000" value={reminds.length && reminds[0].content}/>
        {/* <Text style={Styles.mg5} value="Còn" />
        <Text style={Styles.mg5} color="#000000" value="42:15" type="bold" size="small" />
        <Text value="nữa đến ca làm tiếp theo" /> */}
      </View>
      {/* <View style={Styles.optionsWrap}> */}
        {/* <Icon source={notificationIcon} onPress={() => navigation.navigate('Notification')}/> */}
        {/* <Text color="red" style={Styles.notificationCount} value="20" /> */}
      {/* </View> */}
    </View>
  );
};

export default Header;
