import { Dimensions } from 'react-native';

export const screenWidth = Dimensions.get('screen').width;

export const ratio = 0.68;

export const thumbRatio = 0.24;
