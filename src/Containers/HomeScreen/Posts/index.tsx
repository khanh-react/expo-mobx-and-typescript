import React from 'react';
import { View, ScrollView } from 'react-native';
import Styles from './styles';
import { NewsFeedProps as Props } from './types';
import PostItem from './PostItem';
import Text from '../../../Components/Text';
import { screenWidth, ratio } from '../Dimentions';

export const Posts = ({ posts, onGetCurrentPostIndex }: Props) => {
  if (!posts.length) return null;
  const items = [] as any;
  posts.map((item, index) => {
    items.push(
      <PostItem
        key={`new_feed_${index}`}
        title={item.title}
        description={item.description}
        onGetCurrentPostIndex={() => onGetCurrentPostIndex(index)}
      />
    )
  })

  // items.push(
  //   <PostItem
  //     key={'post-top-ads'}
  //     description=""
  //     title=""
  //     isAds
  //     onGetCurrentPostIndex={() => onGetCurrentPostIndex(posts.length)}/>
  //   )

  return (
    <View style={Styles.wrapper}>
      <View style={Styles.head}>
        <Text size="large" type="bold" value="Bảng tin" />
        {/* <View style={Styles.seeAll}>
          <Text size="smaller" value="Xem tất cả" />
        </View> */}
      </View>
      <ScrollView style={Styles.body} horizontal={true} snapToInterval={screenWidth * ratio}>
        {items}
      </ScrollView>
    </View>
  );
};

export default Posts;
