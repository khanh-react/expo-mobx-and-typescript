export type Post = {
    title: string;
    description: string;
    thumb?: string;
}

export type NewsFeedProps = {
    posts: Post[];
    onGetCurrentPostIndex: (index: number) => void;
};
