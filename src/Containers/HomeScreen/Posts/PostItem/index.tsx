import React from 'react';
import { View, Image, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import Styles from './styles';
import { Post as Props } from './types';
import Text from '../../../../Components/Text';
import Colors from '../../../../Theme/Colors';

export const PostItem = ({ title, thumb, description, isAds, onGetCurrentPostIndex }: Props) => {
  const staticThumb = require('../../../../Assets/post_banner.png');
  const adsThumb = require('../../../../Assets/mock/coffehouse_ads.jpg');
  const adsTitle = "The Coffee House tuyển dụng cuối năm 2020!"
  const navigation = useNavigation();
  if (!isAds)
    return (
      <TouchableOpacity style={Styles.wrapper} onPress={onGetCurrentPostIndex}>
        { thumb && <Image style={Styles.thumb} source={thumb as any} /> }
        { !thumb && <View style={Styles.staticThumbWrapper}><Image source={staticThumb}/></View> }
        <Text style={Styles.title} value={title} numberOfLine={2}/>
        {/* <Text color={Colors.main} style={Styles.loadmore} value="xem thêm"/> */}
      </TouchableOpacity>
    );
  return (
    <TouchableOpacity style={Styles.wrapper} onPress={onGetCurrentPostIndex}>
      <Image style={Styles.thumb} source={adsThumb} />
      <Text style={Styles.title} value={adsTitle} numberOfLine={2}/>
      {/* <Text color={Colors.main} style={Styles.loadmore} value="xem thêm"/> */}
    </TouchableOpacity>
  );
};

export default PostItem;
