export type Post = {
    title: string;
    description: string;
    thumb?: string;
    isAds?: boolean;
    onGetCurrentPostIndex: () => void;
}

export type PostItemProps = {
    posts: Post[];
};

