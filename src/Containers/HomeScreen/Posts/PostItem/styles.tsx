import { StyleSheet, Dimensions } from 'react-native';
import ThemeHelpers from '../../../../Theme/Helpers';
import ThemeColors from '../../../../Theme/Colors';
import { screenWidth, ratio } from '../../Dimentions';

export default StyleSheet.create({
  wrapper: {
    ...ThemeHelpers.shadow,
    width: screenWidth * ratio,
    height: 145,
    marginRight: 25,
    borderRadius: 10,
    backgroundColor: '#FBFBFB',
  },
  thumb: {
    width: '100%',
    borderRadius: 10,
    maxHeight: 100,
  },
  title: {
    marginLeft: 5,
    marginRight: 5,
  },
  loadmore: {
    position: 'absolute',
    bottom: 10,
    right: 10,
    color: ThemeColors.main,
  },
  color: {
    color: ThemeColors.main,
  },
  staticThumbWrapper: {
    ...ThemeHelpers.colCenter,
  }
});
