import { StyleSheet } from 'react-native';
import ThemeHelpers from '../../../Theme/Helpers';
import { screenWidth } from '../Dimentions';

export default StyleSheet.create({
  wrapper: {
    height: 200,
    marginBottom: 5,
  },
  head: {
    width: '100%',
    position: 'relative',
    justifyContent: 'center',
    marginBottom: 10,
  },
  seeAll: {
    position: 'absolute',
    right: 5,
  },
  body: {
    ...ThemeHelpers.row,
    width: screenWidth,
    height: 300,
    marginTop: 5,
  },
});
