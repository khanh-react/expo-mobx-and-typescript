import { StyleSheet } from 'react-native';
import ThemeHelpers from '../../../Theme/Helpers';

export default StyleSheet.create({
  wrapper: {
    marginBottom: 60,
  },
  head: {
    ...ThemeHelpers.rowCross,
    width: '100%',
    position: 'relative',
    marginBottom: 10,
    marginTop: 19,
  },
  seeAll: {
    position: 'absolute',
    right: 5,
  },
  body: {
    borderColor: '#e0e0e0',
    backgroundColor: '#ffffff',
    borderRadius: 10,
    marginRight: 15,
    borderWidth: 1,
    marginTop: 5,
    marginBottom: 10,
    shadowColor: '#ffffff',
    shadowOpacity: 0.41,
    shadowOffset: { width: 10, height: 10 },
    elevation: 5,
  },
  labelCircle: {
    width: 30,
    height: 30,
    borderWidth: 1,
    borderRadius: 20,
    textAlignVertical: 'center',
    textAlign: 'center',
    marginLeft: 2,
    marginRight: 2,
  },
  warn: {
    color: '#FFB84E',
    borderColor: '#FFB84E',
  },
  error: {
    color: '#FF0000',
    borderColor: '#FF0000',
  },
  success: {
    color: '#24CE85',
    borderColor: '#24CE85',
  },
  item: {
    ...ThemeHelpers.row,
    margin: 5,
  },
  itemDatetime: {
    color: '#000000',
    position: 'absolute',
    right: 0,
  },
  itemTextWrapper: {
    ...ThemeHelpers.row,
    width: '100%',
    flexWrap: 'wrap',
  },
  dot: {
    width: 5,
    height: 5,
    backgroundColor: 'silver',
  }
});
