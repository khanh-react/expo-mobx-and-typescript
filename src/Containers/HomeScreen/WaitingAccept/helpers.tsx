export const shortDate = (date: string) => {
    const splitted = date.split('-');
    return `${splitted[2]}/${splitted[1]}`
}

export const shortRangeTime = (startTime: string, endTime: string) => {
    const startTimeSplitted = startTime.split(' ')[1].split(':');
    const shortStartTime = `${startTimeSplitted[0]}:${startTimeSplitted[1]}`;

    const endTimeSplitted = endTime.split(' ')[1].split(':');
    const shortEndTime = `${endTimeSplitted[0]}:${endTimeSplitted[1]}`;

    return `${shortStartTime} - ${shortEndTime}`;
}