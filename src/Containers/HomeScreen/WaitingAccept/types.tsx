export type WaitingAcceptModel = {
    avatar?: string,
    name: string,
    type: number,
    from: string,
    to: string,
    message: string,
    createdAt: string,
}

export type WaitingAcceptProps = {
    onGetShift: (shift: WaitingAcceptModel) => void;
}