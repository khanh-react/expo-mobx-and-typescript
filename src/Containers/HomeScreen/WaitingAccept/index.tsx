import React, { useState, useEffect } from 'react';
import { View } from 'react-native';
import Styles from './styles';
import { WaitingAcceptProps as Props } from './types';
import { shortDate, shortRangeTime } from './helpers';
import Text from '../../../Components/Text';
import HTML from '../../../Components/HTML';
import { UserThumb } from '../../../Components/UserThumb';
import { useStores } from '../../../Helpers/UseStores';

export const WaitingAccept = ({ onGetShift }: Props): React.ReactElement => {
  const [ type, setType ] = useState(3);
  const { switchShifts, getSwitchShifts } = useStores(store => ({
    switchShifts: store.shift.switchShifts,
    getSwitchShifts: store.shift.getSwitchShifts,
  } as any))

  const waitingList = [] as any;

  switchShifts.map((item, index) => {
    if (type === 3 || type === item.type) {
        waitingList.push(
            <View key={`waitingList_${index}`} style={Styles.item}>
                <View><UserThumb id={item.switch_user} name={item.full_name}/></View>
                <View>
                    <View style={Styles.itemTextWrapper}>
                        <HTML key="html1" content={`${item.full_name} muốn <b>hoán đổi ca</b> với bạn`}/>
                    </View>
                    <Text>
                        <Text size="smaller" color="silver" value={`${item.name_position} ngày ${shortDate(item.date)} - vào lúc ${shortRangeTime(item.start_time, item.end_time)}`}/>
                    </Text>
                </View>
                {/* <Text style={Styles.itemDatetime} size="smaller" value={item.createdAt}/> */}
            </View>
        )
    }
  })

  useEffect(() => {
    getSwitchShifts().then();
  }, [])


  return (
        <View style={Styles.wrapper}>
            <View style={Styles.head}>
                <Text style={{ marginRight: 10 }} size="large" type="bold" value="Chờ duyệt"/>
                {/* <TouchableOpacity onPress={() => setType(0)}>
                    <Text style={[Styles.labelCircle, Styles.warn]} size="medium" value="7"/>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => setType(1)}>
                    <Text style={[Styles.labelCircle, Styles.success]} size="medium" value="88"/>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => setType(2)}>
                    <Text style={[Styles.labelCircle, Styles.error]} size="medium" value="3"/>
                </TouchableOpacity>
                <TouchableOpacity style={Styles.seeAll}>
                    <Text size="smaller" value="Xem tất cả"/>
                </TouchableOpacity> */}
            </View>
            <View style={Styles.body}>{waitingList}</View>
        </View>
  );
};

export default WaitingAccept;
