import { Asset } from 'expo';

export type SpeechState = {
  haveRecordingPermissions: boolean,
  isLoading: boolean,
  isPlaybackAllowed: boolean,
  muted: boolean,
  soundPosition: unknown,
  soundDuration: unknown,
  recordingDuration: unknown,
  shouldPlay: boolean,
  isPlaying: boolean,
  isRecording: boolean,
  fontLoaded: boolean,
  shouldCorrectPitch: boolean,
  volume: number,
  rate: number,
};


export class Icon {
  module; width; height;
  constructor(module, width, height) {
    this.module = module;
    this.width = width;
    this.height = height;
    Asset.fromModule(this.module).downloadAsync();
  }
}