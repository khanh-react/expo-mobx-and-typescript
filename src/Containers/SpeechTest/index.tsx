import React from 'react';
import { View, Text } from 'react-native';
import * as Permissions from 'expo-permissions';
import { Audio } from 'expo-av';

const SpeechTest = (): React.ReactElement => {
const recorder = new Audio.Recording();
//   const asking = async () => {
//     const res = await Permissions.askAsync(Permissions.AUDIO_RECORDING);
//     alert(JSON.stringify(res));
//   };

  const recording = async () => {
    try {
        await recorder.prepareToRecordAsync(Audio.RECORDING_OPTIONS_PRESET_HIGH_QUALITY);
        await recorder.startAsync();
    } catch (error) {
        alert(JSON.stringify(error));
    }
  };
  Audio.requestPermissionsAsync().then(e => alert('OK Ok')).catch(e => alert(JSON.stringify(e)))
  recording();
  return (
      <View>
          <Text>OK OK</Text>
      </View>
  );
};

export default SpeechTest;
