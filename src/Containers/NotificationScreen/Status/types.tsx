export type StatusTypes = 'SUCCESS' | 'WAITING' | 'CONFIRMING' | 'ACCEPTED' | 'CANCELED' | 'EXPIRED'

export type StatusProps = {
    type: StatusTypes;
    style?: any;
};
