import React, { useState } from 'react';
import { View } from 'react-native';
import { Styles } from './styles';
import { StatusTypes, StatusProps as Props } from './types';
import { IText as Text } from '../../../Components/Text';

export const Status = ({ style, type }: Props) => {
    const styles = [].concat(style);

    const handleFormatText = (): string => {
        switch (type) {
            case 'ACCEPTED': return 'Thành công';
            case 'CONFIRMING': return 'Đang xử lý';
            case 'WAITING': return 'Đang chờ...';
            case 'CANCELED': return 'Đã từ chối';
            case 'EXPIRED': return 'Hết hạn';
            default: return '';
        };
    }

    return (
        <View style={[
                ...styles,
                Styles.wrapper,
                type === 'ACCEPTED' && Styles.acceptedBg,
                type === 'CONFIRMING' && Styles.confirmingBg,
                type === 'WAITING' && Styles.waitingBg,
                type === 'CANCELED' && Styles.cancelBg,
                type === 'EXPIRED' && Styles.waitingBg,
            ]}>
            <Text
                style={(type === 'WAITING' || type === 'EXPIRED') ? Styles.waitingText : Styles.text}
                size="smaller"
                value={handleFormatText()}/>
        </View>
    );
};

export default Status;
