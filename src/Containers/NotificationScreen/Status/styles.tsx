import { StyleSheet } from 'react-native';
import Helpers from '../../../Theme/Helpers';
import Colors from '../../../Theme/Colors';

export const Styles = StyleSheet.create({
    wrapper: {
        ...Helpers.center,
        width: 80,
        padding: 5,
        borderRadius: 15,
    },
    acceptedBg: {
        backgroundColor: Colors.success,
    },
    confirmingBg: {
        backgroundColor: Colors.warn,
    },
    cancelBg: {
        backgroundColor: Colors.error1,
    },
    waitingBg: {
        backgroundColor: '#E9E9E9',
    },
    text: {
        color: '#FFF'
    },
    waitingText: {
        color: '#676767'
    }
});

export default Styles;
