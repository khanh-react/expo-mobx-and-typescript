import { StyleSheet, Dimensions } from 'react-native';
import Helpers from '../../Theme/Helpers';
import Colors from '../../Theme/Colors';

const width = Dimensions.get('screen').width;

export const Styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        backgroundColor: '#FFF',
    },
    head: {
        width: '100%',
        height: 40,
        borderBottomColor: '#F0F0F0',
        borderBottomWidth: 1,
    },
    headRow: {
        ...Helpers.rowCenter,
        position: 'relative',
        height: 40,
    },
    body: {
        marginBottom: 20,
    },
    bodyRow: {
        ...Helpers.row,
        width: '100%',
        minHeight: 70,
        padding: 10,
        borderBottomColor: '#F0F0F0',
        borderBottomWidth: 1,
    },
    bodyRowThumb: {
        width: 40,
    },
    bodyRowContent: {
        width: width - 55,
    },
    tabWrapper: {
        ...Helpers.row,
        height: 55,
        borderBottomWidth: 1,
        borderBottomColor: '#F0F0F0'
    },
    tap: {
        ...Helpers.center,
        width: '25%',
        height: '100%',
    },
    back: {
        position: 'absolute',
        left: 10,
    },
    status: {
        position: 'absolute',
        right: 10,
        top: 0,
    }
});
