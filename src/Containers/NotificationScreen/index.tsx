import React, { useState, useEffect } from 'react';
import { View, ScrollView , TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { Styles } from './styles';
import { Tabs } from './types';
import { Status } from './Status';
import { Icon } from '../../Components/Icon';
import { IText as Text } from '../../Components/Text';
import { UserThumb } from '../../Components/UserThumb';
import { useStores } from '../../Helpers/UseStores';
import Colors from '../../Theme/Colors';

export const NotificationScreen = () => {
    const [currentTab, setTab] = useState<Tabs>('ALL');
    const { updateVisiting } = useStores(store => ({
        updateVisiting: store.toggle.updateVisiting
    } as any));
    const navigation = useNavigation();
    const Back = require('../../Assets/back_black.png');
    const data = [
        {
            name: 'Nguyễn Quang Khánh',
            type: 'Hoán đổi ca',
            position: 'Bonpas Coffe',
            startTime: 'T4 | 12.02 | 17:30 - 22:00',
            role: 'Phục vụ',
            created: '2 giờ trước',
            status: 'ACCEPTED'
        },
        {
            name: 'Lê Quốc Hùng',
            type: 'Nhường ca',
            position: 'Gió Coffe',
            startTime: 'T4 | 12.02 | 17:30 - 22:00',
            role: 'Giữ xe',
            created: '3 giờ trước',
            status: 'WAITING'
        },
        {
            name: 'Lê Thanh Nghị',
            type: 'Hoán đổi ca',
            position: 'Time Coffee',
            startTime: 'T4 | 12.02 | 17:30 - 22:00',
            role: 'Pha chế',
            created: '1 ngày trước',
            status: 'CONFIRMING'
        },
        {
            name: 'Nguyễn Văn Linh',
            type: 'Hoán đổi ca',
            position: 'Hoàng Diệu',
            startTime: 'T4 | 12.02 | 17:30 - 22:00',
            role: 'Đầu bếp',
            created: '1 tuần trước',
            status: 'CANCELED'
        },
        {
            name: 'Trần Huy Tưởng',
            type: 'Hoán đổi ca',
            position: 'Bonpas Coffe',
            startTime: 'T4 | 12.02 | 17:30 - 22:00',
            role: 'Phục vụ',
            created: '2 tuần trước',
            status: 'WAITING'
        },
        {
            name: 'Nguyễn Quang Khánh',
            type: 'Hoán đổi ca',
            position: 'Bonpas Coffe',
            startTime: 'T4 | 12.02 | 17:30 - 22:00',
            role: 'Phục vụ',
            created: '2 giờ trước',
            status: 'ACCEPTED'
        },
        {
            name: 'Nguyễn Quang Khánh',
            type: 'Hoán đổi ca',
            position: 'Bonpas Coffe',
            startTime: 'T4 | 12.02 | 17:30 - 22:00',
            role: 'Phục vụ',
            created: '2 giờ trước',
            status: 'ACCEPTED'
        },
        {
            name: 'Nguyễn Quang Khánh',
            type: 'Hoán đổi ca',
            position: 'Bonpas Coffe',
            startTime: 'T4 | 12.02 | 17:30 - 22:00',
            role: 'Phục vụ',
            created: '2 giờ trước',
            status: 'ACCEPTED'
        },
        {
            name: 'Nguyễn Quang Khánh',
            type: 'Hoán đổi ca',
            position: 'Bonpas Coffe',
            startTime: 'T4 | 12.02 | 17:30 - 22:00',
            role: 'Phục vụ',
            created: '2 giờ trước',
            status: 'ACCEPTED'
        },
        {
            name: 'Nguyễn Quang Khánh',
            type: 'Hoán đổi ca',
            position: 'Bonpas Coffe',
            startTime: 'T4 | 12.02 | 17:30 - 22:00',
            role: 'Phục vụ',
            created: '2 giờ trước',
            status: 'ACCEPTED'
        },
    ];

    useEffect(() => {
        updateVisiting();
        return () => updateVisiting(false);
    });

    return (
        <View style={Styles.wrapper}>
            <View style={Styles.head}>
                <View style={Styles.headRow}>
                    <Icon style={Styles.back} source={Back} onPress={() => navigation.navigate('Home')}/>
                    <Text type="bold" size="xsmall" value="Quản lý yêu cầu"/>
                </View>
            </View>
            <View style={Styles.body}>
                <View style={Styles.tabWrapper}>
                    <TouchableOpacity style={Styles.tap} onPress={() => setTab('ALL')}>
                        <Text color={currentTab === 'ALL' ? Colors.main : '#C6E4FF'} type="bold" size="xsmall" value="Tất cả"/>
                    </TouchableOpacity>
                    <TouchableOpacity style={Styles.tap} onPress={() => setTab('CONFIRMING')}>
                        <Text color={currentTab === 'CONFIRMING' ? Colors.main : '#C6E4FF'} type="bold" size="xsmall" value="Chờ duyệt"/>
                    </TouchableOpacity>
                    <TouchableOpacity style={Styles.tap} onPress={() => setTab('CANCELED')}>
                        <Text color={currentTab === 'CANCELED' ? Colors.main : '#C6E4FF'} type="bold" size="xsmall" value="Đã hủy"/>
                    </TouchableOpacity>
                    <TouchableOpacity style={Styles.tap} onPress={() => setTab('ACCEPTED')}>
                        <Text color={currentTab === 'ACCEPTED' ? Colors.main : '#C6E4FF'} type="bold" size="xsmall" value="Đã duyệt"/>
                    </TouchableOpacity>
                </View>
                <ScrollView style={Styles.body}>
                    {data.map((item, index) => {
                        if (currentTab === 'ALL' || currentTab === item.status) {
                            return (
                                <View key={index} style={Styles.bodyRow}>
                                    <View style={Styles.bodyRowThumb}>
                                        <UserThumb id={1} name={item.name}/>
                                    </View>
                                    <View style={Styles.bodyRowContent}>
                                        <Text size="xsmall" weight={500} value={item.name}/>
                                        <Text value={`${item.type} - ${item.position}`}/>
                                        <Text value={`${item.startTime} - ${item.role}`}/>
                                        <Text color={Colors.main} size="smaller" value={item.created}/>
                                        <Status style={Styles.status} type={item.status}/>
                                    </View>
                                </View>
                            )
                        }
                    })}
                </ScrollView>
            </View>
        </View>
    );
};

export default NotificationScreen;
