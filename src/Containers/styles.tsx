import { StyleSheet } from 'react-native';

export const Styles = StyleSheet.create({
  wrapper: {
    marginTop: 25,
  },
  container: {
    marginBottom: 35,
  },
  afterContainer: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
});

export default Styles;
