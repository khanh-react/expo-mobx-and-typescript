import React from 'react';
import { View, Image } from 'react-native';
import { ColumnStyles as Styles } from './styles';
import { ColumnProps as Props } from './types';
import ThemeHelpers from '../../../Theme/Helpers';
import Text from '../../../Components/Text';

const CalendarColumn = ({ shift, type, isActive }: Props) => {
  const people = require('../../../Assets/calendar_people.png');
  const people_dark = require('../../../Assets/calendar_people_black.png');

  if (!shift) return <View style={Styles.wrapper} />;
  return (
    <View style={[Styles.wrapper]}>
      <View style={[Styles.column, type === 'lacking' && Styles.lacking, isActive && Styles.active]}>
        <Text color={type === 'lacking' && '#ffffff'}>{shift.start}</Text>
        <Text color={type === 'lacking' && '#ffffff'}>{shift.end}</Text>
        <Text color={type === 'lacking' && '#ffffff'} style={Styles.role}>{shift.role}</Text>
        <View style={{ ...ThemeHelpers.rowCross, marginTop: 5 }}>
          {type === 'lacking' && <Image style={Styles.lackCount} source={people}/>}
          <Text
            color={type === 'lacking' && '#ffffff'}
            style={Styles.mr}
            size="smaller"
            value={type === 'lacking' && (shift.lacking as any).toString()}/>
        </View>
      </View>
    </View>
  );
};

export default CalendarColumn;
