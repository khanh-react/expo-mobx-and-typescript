import React from 'react';
import { View } from 'react-native';
import CalendarRow from './Row';
import { TableStyles as Styles } from './styles';
import { TableProps as Props } from './types';
import { Shift } from '../types';

const CalendarTable = ({ shifts, currentDay, type = 'default' }: Props) => {
  const Rows = [];
  const mondayShifts: Shift[] = shifts.filter(shift => shift.date === 1);
  const tuedayShifts: Shift[] = shifts.filter(shift => shift.date === 2);
  const wednesdayShifts: Shift[] = shifts.filter(shift => shift.date === 3);
  const thursdayShifts: Shift[] = shifts.filter(shift => shift.date === 4);
  const fridayShifts: Shift[] = shifts.filter(shift => shift.date === 5);
  const saturdayShifts: Shift[] = shifts.filter(shift => shift.date === 6);
  const sundayShifts: Shift[] = shifts.filter(shift => shift.date === 7);

  Rows.push(<CalendarRow key="monday_shifts" shifts={mondayShifts} currentDay={currentDay} type={type}/>);
  Rows.push(<CalendarRow key="tuesday_shifts" shifts={tuedayShifts} currentDay={currentDay} type={type}/>);
  Rows.push(<CalendarRow key="wednesday_shifts" shifts={wednesdayShifts} currentDay={currentDay} type={type}/>);
  Rows.push(<CalendarRow key="thursday_shifts" shifts={thursdayShifts} currentDay={currentDay} type={type}/>);
  Rows.push(<CalendarRow key="friday_shifts" shifts={fridayShifts} currentDay={currentDay} type={type}/>);
  Rows.push(<CalendarRow key="saturday_shifts" shifts={saturdayShifts} currentDay={currentDay} type={type}/>);
  Rows.push(<CalendarRow key="sunday_shifts" shifts={sundayShifts} currentDay={currentDay} type={type}/>);

  return <View style={Styles.wrapper}>{Rows}</View>;
};

export default CalendarTable;
