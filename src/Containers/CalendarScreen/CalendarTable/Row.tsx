import React from 'react';
import { View } from 'react-native';
import CalendarColumn from './Column';
import { RowStyles as Styles } from './styles';
import { RowProps as Props } from './types';

const CalendarRow = ({ shifts, currentDay, type }: Props) => {
  const Columns = [];
  shifts.map((shift, index) => {
    const isActive = currentDay === shift.date ? true : false;
    Columns.push(<CalendarColumn key={`column_${shift.shiftId}`} type={type} shift={shift} isActive={isActive}/>);
  });

  return <View style={Styles.wrapper}>{Columns}</View>;
};

export default CalendarRow;
