import { Shift } from '../types';

export type TableProps = {
  shifts: Shift[];
  currentDay: number;
  type?: 'default' | 'lacking';
};

export type RowProps = {
  shifts: Shift[];
  currentDay: number;
  type: 'default' | 'lacking';
};

export type ColumnProps = {
  shift: Shift;
  isActive?: boolean;
  type?: 'default' | 'lacking';
};
