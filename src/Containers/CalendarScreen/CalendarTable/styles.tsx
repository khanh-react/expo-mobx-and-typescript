import { StyleSheet, Dimensions } from 'react-native';
import ThemeHelpers from '../../../Theme/Helpers';
import ThemeColors from '../../../Theme/Colors';

export const TableStyles = StyleSheet.create({
  wrapper: {
    ...ThemeHelpers.row,
  },
});

export const RowStyles = StyleSheet.create({
  wrapper: {
    ...ThemeHelpers.column,
    width: `${100 / 7}%`,
    marginBottom: 10,
  },
});

export const ColumnStyles = StyleSheet.create({
  wrapper: {
    ...ThemeHelpers.center,
    width: '95%',
  },
  column: {
    ...ThemeHelpers.colMain,
    // ...ThemeHelpers.shadow,
    shadowColor: '#FFF',
    shadowOpacity: 0.9,
    shadowRadius: 15,
    shadowOffset: { width: 10, height: 10 },
    elevation: 3,
    width: '100%',
    minHeight: 75,
    paddingLeft: 5,
    borderRadius: 8,
    borderWidth: 1,
    backgroundColor: '#FBFBFB',
    borderColor: '#D8D8D8',
    marginBottom: 5,
  },
  active: {
    borderWidth: 1,
    borderColor: ThemeColors.main,
  },
  lacking: {
    backgroundColor: ThemeColors.red,
  },
  lackCount: {
    width: 14,
    height: 14,
  },
  role: {
    fontSize: 11,
  },
  mr: {
    marginTop: -2,
    marginLeft: 5,
  },
});
