import { StyleSheet } from 'react-native';
import ThemeHelpers from '../../../Theme/Helpers';

export const Styles = StyleSheet.create({
  wrapper: {
    paddingLeft: 15,
    paddingRight: 15,
    borderBottomColor: '#f5f5f5',
    borderBottomWidth: 1,
  },
  head: {
    marginTop: 10,
    marginBottom: 10,
  },
  thumb: {
    ...ThemeHelpers.rowCross,
  },
  thumbName: {
    marginLeft: 3,
  },
  hoursCount: {
    ...ThemeHelpers.rowCenter,
    position: 'absolute',
    top: 2,
    right: 0,
  },
  title: {
    marginBottom: 10,
  },
  listShifts: {
    width: '100%',
  },
  shift: {
    width: '100%',
    height: 50,
    marginBottom: 5,
  },
  haveNoCalendar: {
    position: 'absolute',
    right: 10,
    top: 15,
  }
});

export default Styles;
