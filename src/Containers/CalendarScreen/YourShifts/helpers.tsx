import { getDayNumberFromDayName, friendlyPositionName } from '../../../Helpers/Converter';

export const filterToYourShifts = (allShifts: any[], myID: number): any[] => {
    const myShifts = [] as any;
    allShifts.map((items: any) => {
        if (items && items.data_item && items.data_item.length) {
            items.data_item.map((item: any) => {
                if (item && item.data_user && item.data_user.length) {
                    const myIndex = item.data_user.findIndex((e: any) => e.user_id === myID);
                    if (myIndex >= 0) {
                        const shift = {
                            shiftId: item.data_user[myIndex].id, // RECHECK!!!
                            date: getDayNumberFromDayName(items.name_day),
                            start: item.start_time,
                            end: item.end_time,
                            role: friendlyPositionName(item.data_user[myIndex].name_position || item.data_user[myIndex].name),
                            lacking: 1,
                        };
                        myShifts.push(shift);
                    }
                }
            });
        }
    });
    return myShifts;
};
