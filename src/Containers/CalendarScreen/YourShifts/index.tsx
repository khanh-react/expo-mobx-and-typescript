import React from 'react';
import { View, Image } from 'react-native';
import { Styles } from './styles';
import { YourShipProps as Props } from './types';
import { filterToYourShifts } from './helpers';
import CalendarTable from '../CalendarTable';
import { IText as Text } from '../../../Components/Text';
import { UserThumb } from '../../../Components/UserThumb';

const YourShifts = ({
  id,
  fullname,
  avatar,
  uri,
  currentDay,
  items,
}: Props): React.ReactElement => {
  const _items = filterToYourShifts(items, id);
  const vector = require('../../../Assets/vector.png');
  return (
    <View style={Styles.wrapper}>
      <View style={Styles.head}>
        <View style={Styles.thumb}>
          <UserThumb uri={uri} name={fullname}/>
          <Text style={Styles.thumbName} value={fullname || 'Ca của tôi'}/>
        </View>
        {/* <View style={Styles.hoursCount}>
          <Image source={vector}/>
          <Text size="smaller" value=": 30h"/>
        </View> */}
      </View>
      {
        !_items.length ?
        <Text style={Styles.haveNoCalendar} size="smaller" value="Chưa có lịch"/> :
        <CalendarTable shifts={_items} currentDay={currentDay}/>
      }
    </View>
  );
};

export default YourShifts;
