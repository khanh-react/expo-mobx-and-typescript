export type YourShipProps = {
  id: number;
  fullname: string;
  avatar?: string;
  uri?: string;
  currentDay: number;
  items: [];
};
