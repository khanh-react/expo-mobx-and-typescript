import React, { useState, useEffect } from 'react';
import { View, ScrollView } from 'react-native';
import GestureRecognizer from 'react-native-swipe-gestures';
import Styles from './styles';
import { CalendarProps as Props } from './types';
import Header from './Header';
import LackShifts from './LackShifts';
import YourShifts from './YourShifts';
import OtherShifts from './OtherShifts';
import CreateUnavailable from './CreateUnavailable';
import { WEEK_TIMESTAMP } from './constants';
import { useStores } from '../../Helpers/UseStores';
import { getDateTimeFrom } from '../../Helpers/Converter';

const Calendar = (props: Props): React.ReactElement => {
  const [isRemind, setRemind] = useState(false);
  const [isCalendarShow, setCalendarShow] = useState(false);

  const {
    currentUserID,
    currentDay, currentMonday, currentMonth, currentYear, currentStartingDate, isCreating,
    updateCurrentDay, updateCurrentMonday, updateMonth, updateYear, updateStartingDate, updateCreatingStatus,
    allShifts,
    lackShifts,
    getLackShifts,
    allUsers,
    fullName,
    branchId,
    avatar,
    isNextWeek,
    getCalendarNextWeek,
    getMyCalendar,
    myCalendar,
    } = useStores(store => ({
    allUsers: store.shift.allUsers,
    currentUserID: store.auth.id,
    fullName: store.auth.fullname,
    branchId: store.auth.branchId,
    avatar: store.auth.avatar,
    currentDay: store.calendar.currentDay,
    currentMonday: store.calendar.currentMonday,
    currentMonth: store.calendar.currentMonth,
    currentYear: store.calendar.currentYear,
    currentStartingDate: store.calendar.currentStartingDate,
    isCreating: store.calendar.isCreating,
    allShifts: store.shift.allShifts,
    lackShifts: store.shift.lackShifts,
    updateCurrentDay: store.calendar.updateCurrentDay,
    updateCurrentMonday: store.calendar.updateCurrentMonday,
    updateMonth: store.calendar.updateCurrentMonth,
    updateYear: store.calendar.updateCurrentYear,
    updateStartingDate: store.calendar.updateCurrentStartingDate,
    updateCreatingStatus: store.calendar.updateCreatingStatus,
    getLackShifts: store.shift.getLackShifts,
    getCalendarNextWeek: store.calendar.getCalendarOnNextWeek,
    isNextWeek: store.calendar.isNextWeek,
    getMyCalendar: store.calendar.getMyCalendar,
    myCalendar: store.calendar.myCalendar,
  } as any));

  const handleSwipeChanged = (way: string) => {
    if (way === 'NEXT') {
      return updateStartingDate(currentStartingDate + WEEK_TIMESTAMP);
    }
    if (way === 'BACK') {
      if (isCreating) {
        if (currentStartingDate <= (new Date().getTime() + WEEK_TIMESTAMP)) {
          return;
        }
      }
      return updateStartingDate(currentStartingDate - WEEK_TIMESTAMP);
    }
  };

  const handleCloseCreateUnavailable = () => {
    updateCreatingStatus(false);
    updateStartingDate(new Date().getTime());
  };

  const handeCalendarChanged = (isAll: boolean) => {
    const datetime = getDateTimeFrom(0);
    const _date = datetime.getDate();
    const _date_end = datetime.getDate() + 6;
    const _month = datetime.getMonth() + 1;
    const _year = datetime.getFullYear();
    const params = {
      branch_id: branchId,
      start_time: `${_year}-${_month >= 10 ? _month : `0${_month}`}-${_date >= 10 ? _date : `0${_date}`}`,
      end_time: `${_year}-${_month >= 10 ? _month : `0${_month}`}-${_date_end >= 10 ? _date_end : `0${_date_end}`}`,
      all_user: isAll,
    }
    getCalendarNextWeek(params)
  }

  const handleGetCalendarByDate = (isAll: boolean) => {
    if (isNextWeek) {
      return getMyCalendar();
    }
    handeCalendarChanged(isAll);
  }

  useEffect(() => {
    getLackShifts();
    getMyCalendar();
    !currentDay && updateCurrentDay(7);
  }, []);

  return (
    <View style={Styles.wrapper}>
      <Header
        currentDay={currentDay}
        currentMonday={currentMonday}
        currentMonth={currentMonth}
        currentYear={currentYear}
        currentStartingDate={currentStartingDate}
        isCalendarShow={isCalendarShow}
        isNextWeek={isNextWeek}
        creatingState={isCreating}
        onUpdateCurrentMonday={updateCurrentMonday}
        onUpdateMonth={updateMonth}
        onUpdateYear={updateYear}
        onUpdateStartingDate={updateStartingDate}
        onGetCurrentDay={val => updateCurrentDay(val)}
        onChangeCalendar={setCalendarShow}
        onCreateUnavailable={() => {
          setRemind(true);
          updateCreatingStatus(true);
          updateStartingDate(new Date().getTime() + WEEK_TIMESTAMP);
        }}
        onChangeSwipe={val => handleSwipeChanged(val)}
        onGetNextWeekCalendar={() => handleGetCalendarByDate(false)}
        onBack={handleCloseCreateUnavailable}
        />
      <GestureRecognizer
        onSwipeLeft={() => handleSwipeChanged('NEXT')}
        onSwipeRight={() => handleSwipeChanged('BACK')}>
        {!isCreating ?
          <ScrollView
            style={Styles.body}
            onTouchStart={() => isCalendarShow && setCalendarShow(false)}
            >
            <LackShifts currentDay={currentDay} items={lackShifts} isNextWeek={isNextWeek} />
            <YourShifts
              id={currentUserID}
              currentDay={currentDay}
              items={myCalendar}
              fullname={fullName}
              uri={avatar}
              />
            <OtherShifts
              id={currentUserID}
              items={allShifts}
              itemsNext={myCalendar}
              users={allUsers}
              currentDay={currentDay}
              isNextWeek={isNextWeek}
              />

          </ScrollView> :
          <ScrollView>
            <CreateUnavailable
              currentDay={currentDay}
              currentMonday={currentMonday}
              currentMonth={currentMonth}
              currentYear={currentYear}
              onCurrentDayChanged={updateCurrentDay}
              onClose={handleCloseCreateUnavailable}/>
          </ScrollView>
        }
      </GestureRecognizer>
    </View>
  );
};

export default Calendar;
