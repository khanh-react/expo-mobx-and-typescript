import React, { useState } from 'react';
import { View, TouchableOpacity } from 'react-native';
import Brand from './Brand';
import Styles from './styles';
import Timeline from './Timeline';
import { CalendarProps as Props } from '../types';
import Text from '../../../Components/Text';
import Icon from '../../../Components/Icon';
import { getDateTimeFrom, getCurrentMondayFromToday } from '../../../Helpers/Converter';

const Header = ({
  currentDay,
  currentStartingDate,
  currentMonday,
  currentMonth,
  currentYear,
  creatingState,
  isCalendarShow,
  isNextWeek,
  onUpdateCurrentMonday,
  onUpdateMonth,
  onUpdateYear,
  onGetCurrentDay,
  onUpdateStartingDate,
  onChangeCalendar,
  onCreateUnavailable,
  onChangeSwipe,
  onGetNextWeekCalendar,
  onBack,
}: Props): React.ReactElement => {
  const plusShiftIcon = require('../../../Assets/calendar_plus_shift.png');
  return (
    <View style={Styles.wrapper}>
      {!creatingState &&
        <TouchableOpacity style={Styles.addShift} onPress={() => onCreateUnavailable()}>
          <Icon source={plusShiftIcon} onPress={() => onCreateUnavailable()}/>
          <Text style={Styles.addShiftText} size="smaller" value="Thêm lịch bận" color="#B0B0B0"/>
        </TouchableOpacity>
      }
      <Brand isCreating={creatingState || ''} />
      {
        !creatingState &&
        <TouchableOpacity style={Styles.nextWeekCalendar} onPress={() => onGetNextWeekCalendar(isNextWeek)}>
          <Text style={Styles.nextWeekText} value="Xem lịch" size="smaller"/>
          <Text style={[Styles.nextWeekText, Styles.mrt_5]} value={!isNextWeek ? 'tuần sau' : 'tuần này'} size="smaller"/>
        </TouchableOpacity>
      }
      <Timeline
        isHidden={isCalendarShow}
        isNextWeek={isNextWeek}
        isCreatingStatus={creatingState as any}
        currentDay={currentDay}
        currentMonday={currentMonday}
        currentStartingDate={currentStartingDate}
        onNavigateLeft={() => onUpdateMonth(currentMonday - 7)}
        onNavigateRight={() => onUpdateMonth(currentMonday + 7)}
        onUpdateStartingDate={onUpdateStartingDate}
        onGetCurrentDay={day => onGetCurrentDay(day)}
        onGetCurrentMonth={month => onUpdateMonth(month)}
        onGetCurrentYear={year => onUpdateYear(year)}
        onSwipeChanged={val => onChangeSwipe(val)}
      />
    </View>
  );
};

export default Header;
