export type TimeLineProps = {
  swipe?: 'LEFT' | 'RIGHT' | '',
  currentDay: number;
  currentMonday: number;
  currentStartingDate: number;
  isHidden?: boolean;
  isNextWeek: boolean;
  isCreatingStatus?: boolean;
  onUpdateStartingDate?: (date: number) => void;
  onGetCurrentDay: (day: number) => void;
  onGetCurrentMonth: (month: number) => void;
  onGetCurrentYear: (year: number) => void;
  onNavigateLeft: () => void;
  onNavigateRight: () => void;
  onSwipeChanged: (way: string) => void;
};

export type DayItemProps = {
  style?: any;
  dayOfWeek: number;
  dayOfMonth: number;
  isActive?: boolean;
  isWork?: boolean;
  onGetCurrentDay: (day: number) => void;
};
