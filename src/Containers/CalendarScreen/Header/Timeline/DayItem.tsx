import React from 'react';
import { Image, TouchableOpacity } from 'react-native';
import { DayItemProps as Props } from './types';
import Styles from './styles';
import Text from '../../../../Components/Text';

const dayItem = ({
  style,
  dayOfWeek,
  dayOfMonth,
  isActive,
  isWork,
  onGetCurrentDay,
}: Props): React.ReactElement => {
  const oclock = require('../../../../Assets/calendar_oclock.png');
  const _styles = [].concat(style as any);
  const activeColor = '#28A0F9';

  const getDay = (day: number): string => {
    if (day === 6) return 'CN';
    return `T${day + 2}`;
  };

  return (
    <TouchableOpacity style={[_styles, isActive && Styles.dayActive]} onPress={() => onGetCurrentDay(dayOfWeek)}>
      <Text size="small" type={isActive ? 'bold' : ''} color={isActive && activeColor} value={getDay(dayOfWeek)} />
      <Text
        style={Styles.mrt3}
        size="medium"
        type="bold"
        color={isActive && activeColor}
        value={dayOfMonth.toString()} />
      { isWork && <Image style={[Styles.dayWork, isActive && Styles.dayWorkActive]} source={oclock} /> }
    </TouchableOpacity>
  );
};

export default dayItem;
