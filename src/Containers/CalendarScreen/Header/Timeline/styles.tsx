import { StyleSheet, Dimensions } from 'react-native';
import ThemeHelpers from '../../../../Theme/Helpers';
import Colors from '../../../../Theme/Colors';

const width = Dimensions.get('screen').width - 30;

export default StyleSheet.create({
  wrapper: {
    ...ThemeHelpers.center,
    height: 55,
    width: '100%',
    textAlign: 'center',
    position: 'relative',
  },
  days: {
    ...ThemeHelpers.row,
    width,
    height: '100%',
  },
  dayWrap: {
    ...ThemeHelpers.colCross,
    width: `${(100 / 7)}%`,
    height: '100%',
    borderRadius: 10,
    paddingBottom: 3,
    position: 'relative',
    backgroundColor: '#fff'
  },
  day: {
    ...ThemeHelpers.colCenter,
    width: '65%',
  },
  dayActive: {
    borderColor: '#79C6FF',
    borderWidth: 2,
    borderRadius: 8,
  },
  dayText: {
    fontWeight: "600",
    fontSize: 13,
  },
  dayTextActive: {
    color: Colors.primary,
    fontWeight: "bold",
  },
  dateText: {
    fontWeight: "600",
    fontSize: 18,
  },
  dayToday: {
    color: Colors.primary,
  },
  dateToday: {
    color: Colors.primary,
  },
  dateTextActive: {
    color: Colors.primary,
    fontWeight: "bold",
  },
  dayWork: {
    position: 'absolute',
    bottom: 3,
  },
  dayWorkActive: {
    bottom: 3,
  },
  arrow: {
    ...ThemeHelpers.colCenter,
    position: 'absolute',
    width: 15,
    height: '100%',
  },
  left: {
    left: 5,
  },
  right: {
    right: 5,
  },
  mrt3: {
    marginTop: -5,
  },
});
