import React, { useEffect } from 'react';
import { View, TouchableOpacity } from 'react-native';
import Styles from './styles';
import { IText as Text } from '../../../../Components/Text';
import { TimeLineProps as Props } from './types';
import { getUTCDateFrom } from '../../../../Helpers/Converter';
import { WEEK_TIMESTAMP } from '../../../../Helpers/Constants';
// import CalendarWeekly from '../../../../Components/CalendarWeekly';

const Timeline = ({
  currentDay,
  // currentMonday,
  // currentStartingDate,
  // swipe,
  isNextWeek,
  isHidden,
  isCreatingStatus,
  onGetCurrentDay,
  onGetCurrentMonth,
  onGetCurrentYear,
  // onSwipeChanged,
}: Props) => {
  // const startingDate = getCurrentMondayFromToday() + 7;
  // const handleDateChanged = (date: number | string) => {
  //   onGetCurrentDay(getCurrentDay(date as any));
  // };

  // const handleWeekChanged = (date: number) => {
  //   onGetCurrentMonth(new Date(date).getUTCMonth() + 1);
  //   onGetCurrentYear(new Date(date).getFullYear());
  // };

  const today = new Date().getUTCDate();
  const weekDays = [];

  useEffect(() => {
  }, []);

  for(let i = 1; i <= 7; i++) {
    weekDays.push(
      <TouchableOpacity key={i} style={Styles.dayWrap} onPress={() => onGetCurrentDay(i)}>
        <View style={[Styles.day, i === currentDay && Styles.dayActive]}>
          <Text style={[
              Styles.dayText, i === currentDay &&
              Styles.dayTextActive,
              getUTCDateFrom((isCreatingStatus ? 7 : 0) + i, isNextWeek) === today && Styles.dayToday,
            ]}
            value={i < 7 ? `T${i+1}` : 'CN'}
            />
          <Text
            style={[
              Styles.dateText, i === currentDay &&
              Styles.dateTextActive,
              getUTCDateFrom((isCreatingStatus ? 7 : 0) + i, isNextWeek) === today && Styles.dateToday,
            ]}
            value={getUTCDateFrom((isCreatingStatus ? 7 : 0) + i, isNextWeek).toString()
          }/>
        </View>
      </TouchableOpacity>
    )
  }
  if (isHidden) return null;
  return (
    <View style={Styles.wrapper}>
      {/* <CalendarWeekly
        statingDate={currentStartingDate}
        onDateSelected={val => handleDateChanged(val)}
        onWeekChanged={val => handleWeekChanged(val)}
        onSwipeLeft={() => onSwipeChanged('LEFT')}
        onSwipeRight={() => onSwipeChanged('RIGHT')}
      /> */}
      <View style={Styles.days}>{weekDays}</View>
    </View>
  );
};

export default Timeline;
