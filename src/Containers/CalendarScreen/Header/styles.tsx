import { StyleSheet } from 'react-native';
import ThemeHelpers from '../../../Theme/Helpers';
import Colors from '../../../Theme/Colors';

export default StyleSheet.create({
  wrapper: {
    ...ThemeHelpers.colCross,
    borderBottomWidth: 1,
    borderBottomColor: '#f5f5f5',
    position: 'relative',
    marginTop: 10,
  },
  addShift: {
    ...ThemeHelpers.colCenter,
    position: 'absolute',
    left: 10,
    top: 10,
  },
  addShiftText: {
    marginTop: 5,
  },
  nextWeekCalendar: {
    ...ThemeHelpers.colCenter,
    height: 40,
    position: 'absolute',
    right: 10,
    borderWidth: 1,
    borderColor: Colors.main,
    borderRadius: 8,
  },
  nextWeekText: {
    padding: 7,
    paddingTop: 2,
    paddingBottom: 3,
    fontSize: 11,
  },
  options: {
    ...ThemeHelpers.rowCross,
    position: 'absolute',
    right: 10,
    top: 10,
  },
  calendar: {
  },
  left30: {
    left: 10,
  },
  mg5: {
    marginRight: 5,
  },
  mrt_5: {
    marginTop: -5,
  }
});
