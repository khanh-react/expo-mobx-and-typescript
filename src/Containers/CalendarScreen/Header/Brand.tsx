import React from 'react';
import { StyleSheet, View } from 'react-native';
import { BrandProps as Props } from './types';
import ThemeHelpers from '../../../Theme/Helpers';
import { WEEK_TIMESTAMP } from '../../../Helpers/Constants';
import Text from '../../../Components/Text';

const Styles = StyleSheet.create({
  wrapper: {
    ...ThemeHelpers.colCenter,
    position: 'relative',
    textAlign: 'center',
    marginBottom: 10,
    height: 50,
  },
  title: {
    ...ThemeHelpers.rowCross,
  },
  arrow: {
    position: 'absolute',
    right: -15,
    top: 15,
    zIndex: 10,
    width: 15,
    height: 15,
  },
});

const HeaderBrand = ({ isCreating }: Props) => {
  const datetime = !isCreating ? new Date() : new Date(new Date().getTime() + WEEK_TIMESTAMP);
  const month = datetime.getUTCMonth() + 1;
  const year = datetime.getFullYear();
  return (
    // <View style={Styles.wrapper}>
    //   <TouchableOpacity style={Styles.title} onPress={() => !isCreating && onChangeCalendar()}>
    //     <Text type="bold" size="large" value={`Tháng ${currentMonth}, ${currentYear}`}/>
    //     <Image style={Styles.arrow} source={arrowDownIcon} />
    //   </TouchableOpacity>
    //   <Text value={store} color="#B0B0B0"/>
    // </View>
    <View style={Styles.wrapper}>
      <Text type="bold" size="large" value={`Tháng ${month > 9 ? month : `0${month}`}, ${year}`}/>
    </View>
  );
};

export default HeaderBrand;
