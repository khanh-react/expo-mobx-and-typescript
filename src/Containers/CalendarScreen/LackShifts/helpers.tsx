import { getDayNumberFromDayName, friendlyPositionName } from '../../../Helpers/Converter';

export const formatLackData = (data: any[]): any[] => {
    const lacks = [] as any;
    data.map((items: any) => {
        items.data_item.map((item: any) => {
            const lack = {
                shiftId: item.data_position[0].shift_detail_id, // RECHECK!!!
                date: getDayNumberFromDayName(items.name_day),
                start: item.start_time,
                end: item.end_time,
                role: friendlyPositionName(item.data_position[0].name_position),
                lacking: 1,
            };
            lacks.push(lack);
        });
    });
    return lacks;
};
