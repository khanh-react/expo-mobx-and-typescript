import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  wrapper: {
    paddingLeft: 15,
    paddingRight: 15,
    borderBottomColor: '#f5f5f5',
    borderBottomWidth: 1,
  },
  title: {
    marginBottom: 10,
  },
  listShifts: {
    width: '100%',
  },
  shift: {
    width: '100%',
    height: 50,
    marginBottom: 5,
  },
});
