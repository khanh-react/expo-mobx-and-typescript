import React from 'react';
import { View } from 'react-native';
import Styles from './styles';
import { LackShiftProps as Props } from './types';
import { formatLackData } from './helpers';
import CalendarTable from '../CalendarTable';
import { IText as Text } from '../../../Components/Text';

const LackShifts = ({ currentDay, items, isNextWeek }: Props) => {
  const lackShifts = formatLackData(items);

  let _items = [] as any;
  items.map((item: any) => item.data_item.length && _items.push(1));

  if (!_items.length || isNextWeek) return null;

  return (
    <View style={Styles.wrapper}>
      <Text style={Styles.title} value="Bạn có thể nhận làm ở các ca thiếu người dưới đây" />
      <CalendarTable type="lacking" shifts={lackShifts} currentDay={currentDay} />
    </View>
  );
};

export default LackShifts;
