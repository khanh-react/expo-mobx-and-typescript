export type LackShiftProps = {
  currentDay: number;
  items: any[];
  isNextWeek: boolean;
};
