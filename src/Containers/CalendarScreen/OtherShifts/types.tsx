export type OtherShiftsProps = {
  id: number;
  currentDay: number;
  items: any[];
  itemsNext: any[];
  users: any[];
  isNextWeek: boolean;
}