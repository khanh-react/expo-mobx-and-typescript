import React, { useState } from 'react';
import { View, ScrollView, TouchableOpacity } from 'react-native';
import Styles from './styles';
import { OtherShiftsProps as Props } from './types';
import { IText as Text } from '../../../Components/Text';
import YourShifts from '../YourShifts';
import DownSvg from '../../../Assets/icons/arrow-down';

export const OtherShifts = ({ id, items, itemsNext, users, currentDay, isNextWeek }: Props) => {
    const [seeAll, setSeeAll] = useState(false);
    if (!seeAll) {
        return (
            <TouchableOpacity style={Styles.seeAllWrapper} onPress={() => setSeeAll(true)}>
                <View style={Styles.seeAll}>
                    <Text size="smaller" value="Xem tất cả"/>
                    <DownSvg fill="#000"/>
                </View>
            </TouchableOpacity>
        )
    }
    return (
        <ScrollView>
            {
                users.map((user => {
                    if (user.id !== id) {
                        return <YourShifts
                                key={user.id}
                                currentDay={currentDay}
                                items={isNextWeek ? itemsNext : (items as any)}
                                id={user.id}
                                fullname={user.name}
                                />
                    }
                    return null
                }))
            }
        </ScrollView>
    )
}

export default OtherShifts;
