import { StyleSheet } from 'react-native';
import Helpers from '../../../Theme/Helpers';

export const Styles = StyleSheet.create({
    seeAllWrapper: {
        ...Helpers.center,
        padding: 10,
    },
    seeAll: {
        ...Helpers.colCenter,
    },
});

export default Styles;
