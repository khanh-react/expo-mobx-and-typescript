import React, { useState, useEffect } from 'react';
import { View } from 'react-native';
import Styles from './styles';
import { CreateUnavailableProps as Props } from './types';
import { Column } from './Column';
import Button from '../../../Components/Button';
import Text from '../../../Components/Text';
import { getDayNameFromDayNumber } from '../../../Helpers/Converter';
import { getReminder, setReminder } from '../../../Helpers/LocalStorage';
import { useStores } from '../../../Helpers/UseStores';

export const CreateUnavailable = ({
  onCurrentDayChanged,
  onClose,
}: Props) => {
  const [reminder, updateReminder] = useState(null);
  const {
    currentDay,
    workHours,
    addBusyTimes,
    getBranchCalendar,
    updateBusyTimes,
  } = useStores(stores => ({
    currentDay: stores.calendar.currentDay,
    shifts: stores.shift.shiftHours,
    workHours: stores.calendar.workHours,
    currentBusyTimes: stores.shift.currentBusyTimes,
    addBusyTimes: stores.calendar.onAddBusyTime,
    getBranchCalendar: stores.calendar.getBranchCalendar,
    updateBusyTimes: stores.calendar.updateBusyTime,
  } as any));

  useEffect(() => {
    getBranchCalendar();
    onCurrentDayChanged(999);
    getReminder()
    .then((e: any) => updateReminder(e));
    return () => onCurrentDayChanged(999);
  }, [])

  if (!workHours['T2']) return <Text value="Chưa có lịch làm việc" />

  return (
    <>
      <View style={Styles.wrapper}>
        {
          Object.keys(workHours).map((key: string, index: number) => {
            return <View key={key} style={Styles.rowWrap}>
              {
                workHours[key].map((item: any, index: number) => {
                  return (
                    <Column
                      key={`${key} ${index}`}
                      dayName={key}
                      isActive={getDayNameFromDayNumber(currentDay) === key}
                      startTime={item.start_time.slice(0,5)}
                      endTime={item.end_time.slice(0,5)}
                      isChecked={item.isBusy}
                      onChange={updateBusyTimes}
                    />
                  )
                })
              }
            </View>
          })
        }

        <View style={Styles.foot}>
          <Button style={[Styles.footBtn, Styles.mgr10]} text="Đóng" type="default" onPress={onClose}/>
          <Button style={Styles.footBtn} text="Gửi" type="default" onPress={() => addBusyTimes()}/>
        </View>
      </View>
    </>
  );
};

export default CreateUnavailable;
