import { BusyTime } from './types';
import { WEEK_TIMESTAMP } from '../../../Helpers/Constants';
import { getCurrentMondayFromToday, getDayNumberFromDayName } from '../../../Helpers/Converter';

export const SHIFTS = [
  {
    key: 'morning',
    name: 'Ca Sáng',
    hour: '07:00 - 12:00',
  },
  {
    key: 'afternoon',
    name: 'Ca Chiều',
    hour: '13:00 - 17:00',
  },
  {
    key: 'night',
    name: 'Ca Tối',
    hour: '17:30 - 22:00',
  },
];

export const getDateFromDay = () => {}

type Item = {
  rowKey: string;
  day: number;
  isChecked: boolean;
}

type CurrentDatetimes = {
  currentMonth: number;
  currentYear: number;
}

const todayDate = getCurrentMondayFromToday(new Date().getTime() + WEEK_TIMESTAMP);
const correctDate = (date: number) => {
  if (date >= 10) return date;
  return `0${date}`;
}

export const updateBusyTimesHelper = (addedList: BusyTime[], item: Item, currentDatetimes: CurrentDatetimes) => {
  const { currentMonth, currentYear } = currentDatetimes;
  const _date = `${currentYear}-${correctDate(currentMonth)}-${correctDate(todayDate + item.day)}`;
  const shirtHour = SHIFTS.find(shift => shift.key === item.rowKey)?.hour;
  const startTime = shirtHour?.split(' - ')[0] || '';
  const endTime = shirtHour?.split(' - ')[1] || '';

  if (item.isChecked) {
    addedList.push({
      rowKey: item.rowKey, // morning or afternoon or night
      index: item.day, // 0 = T2 ; 6 = CN
      start_time: `${_date} ${startTime}`,
      end_time: `${_date} ${endTime}`,
      note: ''
    })
    return addedList;
  }

  const _findIndex = addedList.findIndex(e => e.rowKey === item.rowKey && e.index === item.day);
  addedList.splice(_findIndex, 1);
  return addedList;
}


export const getListCurrentBusyTimes = (items) => {
  let busyTimes = {
    mornings: [],
    afternoons: [],
    nights: [],
  };
  items.map(item => {
    item.data_item.map(dataItem => {
      if (dataItem && dataItem.start === '07:00') {
        busyTimes.mornings.push(getDayNumberFromDayName(item.name_day))
      }
      if (dataItem && dataItem.start === '13:00') {
        busyTimes.afternoons.push(getDayNumberFromDayName(item.name_day))
      }
      if (dataItem && dataItem.start === '17:30') {
        busyTimes.nights.push(getDayNumberFromDayName(item.name_day))
      }
    })
  })
  return busyTimes;
}

export const getBusyTimesFromOldData = (items: any[]) => {
  let list = [];
  items.map(item => {
    item.data_item.map(dataItem => {
      let day = {
        rowKey: '',
        index: getDayNumberFromDayName(item.name_day) - 1, // 0 = T2 ; 6 = CN
        start_time: `${item.day} ${dataItem.start}`,
        end_time: `${item.day} ${dataItem.end}`,
        note: dataItem.note
      };
      if (dataItem && dataItem.start === '07:00') {
        day.rowKey = 'morning';
      }
      if (dataItem && dataItem.start === '13:00') {
        day.rowKey = 'afternoon';
      }
      if (dataItem && dataItem.start === '17:30') {
        day.rowKey = 'night';
      }
      list.push(day);
    })
  });
  return list;
}
