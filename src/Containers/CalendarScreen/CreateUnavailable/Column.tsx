import React, { useState, useEffect } from 'react';
import { View, Image, TouchableOpacity } from 'react-native';
import Styles from './styles';
import { ColumnXProps as Props } from './types';
import { IText as Text } from '../../../Components/Text';
import { Icon } from '../../../Components/Icon';
import { getCurrentDay, _getDayNumberFromDayName, getCurrentMondayFromToday, getDateTimeFrom } from '../../../Helpers/Converter';
import { WEEK_TIMESTAMP } from '../../../Helpers/Constants';

export const Column = ({
  dayName,
  isActive,
  startTime,
  endTime,
  isChecked,
  onChange,
}: Props) => {
  const uncheckIcon = require('../../../Assets/calendar_elipse_uncheck.png');
  const checkedIcon = require('../../../Assets/calendar_elipse_checked.png');
  const [checked, setChecked] = useState(false);
  const todayTimestamp = new Date().getTime() + WEEK_TIMESTAMP;
  const currentMondayDate = getCurrentMondayFromToday(todayTimestamp);

  const friendlyDate = (date: number) => {
    if (date < 10) {
      return `0${date}`;
    }
    return date.toString();
  }

  const handleChecked = () => {
    const dayNumber = _getDayNumberFromDayName(dayName) || 0;
    const datetime = getDateTimeFrom(dayNumber - 1);
    const year = datetime.getFullYear();
    const month = datetime.getUTCMonth() + 1;
    const day = datetime.getUTCDate();
    const date = `${year}-${friendlyDate(month)}-${friendlyDate(day)}`;
    onChange({ dayName, date, startTime, endTime, isChecked: checked })
  };

  return (
    <View style={[Styles.col, isChecked && Styles.colChecked, isActive && Styles.colActived]}>
      <TouchableOpacity onPress={handleChecked}>
        {!isChecked && <Image style={Styles.radioCheckbox} width={5} source={uncheckIcon}/>}
        {isChecked && <Image style={Styles.radioCheckbox} source={checkedIcon}/>}
        <Text size="smaller" value={startTime} />
        <Text size="smaller" value={endTime} />
      </TouchableOpacity>
    </View>
  );
};

export default Column;
