export type CreateUnavailableProps = {
  currentDay: number;
  currentMonday: number;
  currentMonth: number;
  currentYear: number;
  onCurrentDayChanged: (currentDay: number) => void;
  onClose: () => void;
};

export type RowProps = {
  rowKey: string;
  title: string;
  activeIndex: number;
  busyTimes: number[],
  onRowChanged: (payload: {
    day: number,
    isChecked: boolean,
    rowKey: string,
  }) => void;
};

export type ColumnProps = {
  index: number;
  isActived: boolean;
  busyTimes: number[];
  onSetCurrentIndex: (payload: { index: number, isChecked: boolean }) => void;
};

export type ColumnXProps = {
  dayName: string;
  isActive: boolean;
  startTime: string;
  endTime: string;
  isChecked: boolean;
  onChange: (payload: {
    dayName: string;
    date: string;
    startTime: string;
    endTime: string;
    isChecked: boolean;
  }) => void;
};

export type BusyTime = {
  rowKey: string;
  index: number;
  start_time?: string;
  end_time?: string;
  note?: string;
  isChecked?: boolean;
}