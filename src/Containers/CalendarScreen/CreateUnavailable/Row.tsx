import React from 'react';
import { View, Image } from 'react-native';
import Styles from './styles';
import { RowProps as Props } from './types';
import Column from './Column';
import Text from '../../../Components/Text';

export const Row = ({ rowKey, title, activeIndex, busyTimes, onRowChanged }: Props): React.ReactElement => {
  const cols = [];
  const handleCurrentIndexChanged = (e: any) => {
    onRowChanged({
      day: e.index,
      isChecked: e.isChecked,
      rowKey,
    })
  }

  for (let i = 0; i < 7; i++) {
    cols.push(
      <Column
        key={`unavailable_col_${i}`}
        index={i}
        isActived={activeIndex === i}
        busyTimes={busyTimes}
        onSetCurrentIndex={handleCurrentIndexChanged}/>
    );
  }

  return (
    <View style={Styles.row}>
      <Text style={Styles.title} value={title}/>
      <View style={Styles.colWrap}>{cols}</View>
    </View>
  );
};

export default Row;
