import { StyleSheet, Dimensions } from 'react-native';
import ThemeHelpers from '../../../Theme/Helpers';
import ThemeColors from '../../../Theme/Colors';

const { width, height } = Dimensions.get('screen');

export default StyleSheet.create({
  wrapper: {
    ...ThemeHelpers.row,
    width: width,
    height: height - 150,
    paddingLeft: 10,
    paddingRight: 10,
  },
  rowWrap: {
    ...ThemeHelpers.colCross,
    height: '100%',
    width: `${100 / 7}%`,
  },
  row: {
    ...ThemeHelpers.colCross,
    width: '100%',
    height: 110,
  },
  colWrap: {
    ...ThemeHelpers.row,
    width: '100%',
    height: '100%',
  },
  // cols: {
  //   ...ThemeHelpers.center,
  //   width: `${100 / 7}%`,
  //   height: 75,
  // },
  col: {
    ...ThemeHelpers.colCenter,
    width: '90%',
    height: 65,
    borderWidth: 1,
    borderColor: 'silver',
    shadowColor: '#FFF',
    backgroundColor: '#FFF',
    shadowOpacity: 0.21,
    shadowRadius: 5,
    shadowOffset: {
      width: 0,
      height: 27,
    },
    elevation: 8,
    borderRadius: 10,
    marginBottom: 5,
    position: 'relative',
  },
  colChecked: {
    backgroundColor: ThemeColors.red,
  },
  colActived: {
    borderColor: ThemeColors.main,
  },
  radioCheckbox: {
    position: 'absolute',
    top: -12,
    width: 10,
    height: 10,
  },
  title: {
    marginLeft: 5,
    marginBottom: 5,
  },
  foot: {
    ...ThemeHelpers.rowCenter,
    width: '100%',
    marginLeft: 10,
    position: 'absolute',
    bottom: 20,
  },
  footBtn: {
    width: 150,
  },
  mgr10: {
    marginRight: 10,
  },
  busy: {
  }
});
