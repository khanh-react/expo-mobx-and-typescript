import React, { useState } from 'react';
import { View } from 'react-native';
import Styles from './styles';
import { ReminderProps as Props } from './types';
import Text from '../../../../Components/Text';
import Button from '../../../../Components/Button';
import Checkbox from '../../../../Components/Checkbox';
import { Popup } from '../../../../Components/Popup';

export const Reminder = ({ onNext }: Props): React.ReactElement => {
  const [isChecked, setChecked] = useState(false);

  return (
    <Popup openAt={new Date().getTime()}>
      <View style={Styles.wrapper}>
        <View style={Styles.head}>
          <Text size="medium" type="bold" value="Đăng ký lịch bận"/>
        </View>
        <View style={Styles.messages}>
          <Text size="medium" value="Chọn các khoảng thời gian trong tuần mà bạn không thể đi làm"/>
          <Text size="medium" value="Quy tắc đăng ký lịch làm:"/>
        </View>
        <View style={Styles.foot}>
          <View style={Styles.remind}>
            <Checkbox isChecked={isChecked} onPress={val => setChecked(val)}/>
            <Text value="Không hiện lần sau"/>
          </View>
          <Button
            style={Styles.button}
            text="Đã hiểu"
            type="default"
            onPress={() => onNext() }/>
        </View>
      </View>
    </Popup>
  );
};

export default Reminder;
