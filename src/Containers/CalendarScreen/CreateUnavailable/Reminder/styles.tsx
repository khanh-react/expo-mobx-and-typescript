import { StyleSheet } from 'react-native';
import ThemeHelpers from '../../../../Theme/Helpers';

export default StyleSheet.create({
  wrapper: {
    ...ThemeHelpers.colCross,
  },
  head: {},
  foot: {
    ...ThemeHelpers.center,
    height: 100,
    width: '100%',
    position: 'absolute',
    bottom: 0,
  },
  title: {},
  messages: {
    marginTop: 20,
  },
  remind: {
    ...ThemeHelpers.rowCenter,
    marginBottom: 10,
  },
  button: {
    width: 120,
  },
});
