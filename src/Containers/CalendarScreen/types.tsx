export type CalendarProps = {
  currentDay: number;
  currentMonday: number;
  currentMonth: number;
  currentYear: number;
  currentStartingDate: number;
  swipe?: 'LEFT' | 'RIGHT' | '';
  creatingState?: string;
  isCalendarShow?: boolean;
  isNextWeek: boolean;
  onUpdateCurrentMonday: (day?: number) => void;
  onUpdateMonth: (month: number) => void;
  onUpdateYear: (year: number) => void;
  onChangeCalendar: (val: any) => void;
  onGetCurrentDay: (day: number) => void;
  onUpdateStartingDate: (date: number) => void;
  onCreateUnavailable: () => void;
  onChangeSwipe: (way: string) => void;
  onGetNextWeekCalendar: (bool: boolean) => void; 
  onBack: () => void;
};

export type Shift = {
  shiftId: string,
  date: number,
  start: string,
  end: string,
  role: string,
  lacking?: number,
};
