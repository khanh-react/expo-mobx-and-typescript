import { StyleSheet, Dimensions } from 'react-native';

const height = Dimensions.get('screen').height;

export default StyleSheet.create({
  wrapper: {
    flex: 1,
    position: 'relative',
    backgroundColor: '#ffffff',
  },
  body: {
    marginBottom: 50,
    maxHeight: height - 200,
  }
});
