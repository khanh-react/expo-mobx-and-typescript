import React, { useState } from 'react';
import { View } from 'react-native';
import Styles from './styles';
import { Footer } from '../Components/Footer';
import { ContainerNavigator } from '../Navigators/ContainerNavigator';
import { Notification } from '../Components/Notification';
import { useStores } from '../Helpers/UseStores';

export const ContainerAfterSignIn = () => {
  const [currentRoute, setRoute] = useState('Home');
  const { currentShift, isCreating, isPopupShown, isVisiting, pushNoti } = useStores(store => ({
    isCreating: store.calendar.isCreating,
    isPopupShown: store.toggle.isPopupShown,
    isVisiting: store.toggle.isVisiting,
    currentShift: store.shift.currentShift,
  } as any));

  return (
    <View style={Styles.afterContainer}>
      <ContainerNavigator/>
      <Footer
        isHidden={isPopupShown || isCreating || currentShift || isVisiting}
        currentRoute={currentRoute}
        onChangeRoute={setRoute}
      />
      <Notification/>
    </View>
  );
};

export default ContainerAfterSignIn;
