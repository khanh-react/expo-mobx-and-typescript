import React from 'react';
import { SafeAreaView, Text, View } from 'react-native';
import { StatusBar } from 'expo-status-bar';
import { useFonts } from '@expo-google-fonts/barlow';
import ContainerStyles from './styles';
import { NavigationContainer } from '@react-navigation/native';
import AppNavigator from '../Navigators/AppNavigator';
import ThemeHelpers from '../Theme/Helpers';
import { appFonts } from '../Theme/Fonts';

export const Container = (): React.ReactElement => {
  const [injectFonts] = useFonts(appFonts);
  if (!injectFonts) return <View />;
  return (
    <SafeAreaView style={[ContainerStyles.wrapper, ThemeHelpers.fill]}>
      <NavigationContainer fallback={<Text>Loading ...</Text>} >
        <AppNavigator></AppNavigator>
      </NavigationContainer>
      <StatusBar style="auto" />
    </SafeAreaView>
  );
};

export default Container;
