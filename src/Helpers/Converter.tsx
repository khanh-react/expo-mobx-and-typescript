import { validateEmail, validatePhone } from './Validate';
import { WEEK_TIMESTAMP, ONE_DAY_TIMESTAMP } from './Constants';

export const convertFileToBase64 = (file: any) => {
  try {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = error => reject(error);
    });
  } catch (error) {
    console.log(JSON.stringify(error));
  }
};

export const getUTCDateFrom = (daysNumber: number = 0, isNextWeek?: boolean): number => {
  let today = new Date().getDay();
  let todayDate = new Date().getTime();
  switch (today) {
    case 0: // CN
      return new Date((todayDate - 7*ONE_DAY_TIMESTAMP) + daysNumber*ONE_DAY_TIMESTAMP + (isNextWeek ? WEEK_TIMESTAMP : 0)).getUTCDate();
    case 6:
      return new Date((todayDate - 6*ONE_DAY_TIMESTAMP) + daysNumber*ONE_DAY_TIMESTAMP + (isNextWeek ? WEEK_TIMESTAMP : 0)).getUTCDate();
    case 5:
      return new Date((todayDate - 5*ONE_DAY_TIMESTAMP) + daysNumber*ONE_DAY_TIMESTAMP + (isNextWeek ? WEEK_TIMESTAMP : 0)).getUTCDate();
    case 4:
      return new Date((todayDate - 4*ONE_DAY_TIMESTAMP) + daysNumber*ONE_DAY_TIMESTAMP + (isNextWeek ? WEEK_TIMESTAMP : 0)).getUTCDate();
    case 3:
      return new Date((todayDate - 3*ONE_DAY_TIMESTAMP) + daysNumber*ONE_DAY_TIMESTAMP + (isNextWeek ? WEEK_TIMESTAMP : 0)).getUTCDate();
    case 2:
      return new Date((todayDate - 2*ONE_DAY_TIMESTAMP) + daysNumber*ONE_DAY_TIMESTAMP + (isNextWeek ? WEEK_TIMESTAMP : 0)).getUTCDate();
    case 1: // T2
      return new Date((todayDate - 1*ONE_DAY_TIMESTAMP) + daysNumber*ONE_DAY_TIMESTAMP + (isNextWeek ? WEEK_TIMESTAMP : 0)).getUTCDate();
    default:
      return 999;
  }
};

export const getDateTimeFrom = (daysNumber: number = 0): Date => {
  let today = new Date().getDay();
  let todayDate = new Date().getTime();
  switch (today) {
    case 0: // CN
      return new Date((todayDate - 6*ONE_DAY_TIMESTAMP) + 1*WEEK_TIMESTAMP + daysNumber*ONE_DAY_TIMESTAMP);
    case 6:
      return new Date((todayDate - 5*ONE_DAY_TIMESTAMP) + 1*WEEK_TIMESTAMP + daysNumber*ONE_DAY_TIMESTAMP);
    case 5:
      return new Date((todayDate - 4*ONE_DAY_TIMESTAMP) + 1*WEEK_TIMESTAMP + daysNumber*ONE_DAY_TIMESTAMP);
    case 4:
      return new Date((todayDate - 3*ONE_DAY_TIMESTAMP) + 1*WEEK_TIMESTAMP + daysNumber*ONE_DAY_TIMESTAMP);
    case 3:
      return new Date((todayDate - 2*ONE_DAY_TIMESTAMP) + 1*WEEK_TIMESTAMP + daysNumber*ONE_DAY_TIMESTAMP);
    case 2:
      return new Date((todayDate - 1*ONE_DAY_TIMESTAMP) + 1*WEEK_TIMESTAMP + daysNumber*ONE_DAY_TIMESTAMP);
    case 1: // T2
      return new Date((todayDate - 0*ONE_DAY_TIMESTAMP) + 1*WEEK_TIMESTAMP + daysNumber*ONE_DAY_TIMESTAMP);
    default:
      return new Date();
  }
};

export const getCurrentMondayFromToday = (timestamp?: number): number => {
  let today = new Date().getDay();
  let todayDate = new Date().getUTCDate();

  if (timestamp) {
    today = new Date(timestamp).getDay();
    todayDate = new Date(timestamp).getUTCDate();
  }

  switch (today) {
    case 0: // CN
      return todayDate - 6;
    case 6:
      return todayDate - 5;
    case 5:
      return todayDate - 4;
    case 4:
      return todayDate - 3;
    case 3:
      return todayDate - 2;
    case 2:
      return todayDate - 1;
    case 1: // T2
      return todayDate - 0;
    default:
      return 999;
  }
};

export const getDayNumberFromDayName = (dayName: string) => {
  switch (dayName) {
    case 'Thứ 2': return 1;
    case 'Thứ 3': return 2;
    case 'Thứ 4': return 3;
    case 'Thứ 5': return 4;
    case 'Thứ 6': return 5;
    case 'Thứ 7': return 6;
    case 'Chủ nhật': return 7;
  }
}

export const _getDayNumberFromDayName = (dayName: string) => {
  switch (dayName) {
    case 'T2': return 1;
    case 'T3': return 2;
    case 'T4': return 3;
    case 'T5': return 4;
    case 'T6': return 5;
    case 'T7': return 6;
    case 'CN': return 7;
  }
}

export const getDayNameFromDayNumber = (dayNumer: number) => {
  switch (dayNumer) {
    case 1: return 'Thứ 2';
    case 2: return 'Thứ 3';
    case 3: return 'Thứ 4';
    case 4: return 'Thứ 5';
    case 5: return 'Thứ 6';
    case 6: return 'Thứ 7';
    case 7: return 'Chủ nhật';
  }
}

export const getCurrentDay = (currentDay = '') => {
  const day = new Date(currentDay).getDay();
  if (!day) return 7;
  return day;
}

export const convertDayName = (dayName: string) => {
  const splitted = dayName.split(' ');
  const key = splitted[0];
  const value = parseInt(splitted[1]);
  return { key, value };
}

export const friendlyPositionName = (name: string) => {
  switch (name) {
    case 'Phục vụ': return 'P.vụ';
    case 'Bảo vệ': return 'B.vệ';
    case 'Thu ngân': return 'T.ngân';
    case 'Giữ xe': return 'G.xe';
    default:
      break;
  }
}

export const friendlyDayName = (dayName: string) => {
  const today = new Date().getDay() + 1;
  if (convertDayName(dayName).value === today) {
    return 'Hôm nay';
  }
  if (convertDayName(dayName).value === (today + 1)) {
    return 'Ngày mai';
  }
  if (convertDayName(dayName).value < today) {
    return 'Hôm qua';
  }
  return dayName;
}

export const isFriendlyDay = (dayName: string) => {
  if (friendlyDayName(dayName) === 'Hôm nay' || friendlyDayName(dayName) === 'Ngày mai') {
    return true;
  }
  return false;
}

export const shortDayName = (day: string) => {
  switch (day) {
    case 'Thứ 2': return 'T.2';
    case 'Thứ 3': return 'T.3';
    case 'Thứ 4': return 'T.4';
    case 'Thứ 5': return 'T.5';
    case 'Thứ 6': return 'T.6';
    case 'Thứ 7': return 'T.7';
    case 'Chủ nhật': return 'CN';
    default: return '';
  }
};

export const _shortDayName = (day: string) => {
  switch (day) {
    case 'Thứ 2': return 'T2';
    case 'Thứ 3': return 'T3';
    case 'Thứ 4': return 'T4';
    case 'Thứ 5': return 'T5';
    case 'Thứ 6': return 'T6';
    case 'Thứ 7': return 'T7';
    case 'Chủ nhật': return 'CN';
    default: return '';
  }
};

export const getFullDayNameFromShortDayName = (dayName: string) => {
  switch (dayName) {
    case 'T2' || 'T.2': return 'Thứ 2';
    case 'T3' || 'T.3': return 'Thứ 3';
    case 'T4' || 'T.4': return 'Thứ 4';
    case 'T5' || 'T.5': return 'Thứ 5';
    case 'T6' || 'T.6': return 'Thứ 6';
    case 'T7' || 'T.7': return 'Thứ 7';
    case 'CN': return 'Chủ nhật';
    default: return '';
  }
};

export const dateMonthFriendly = (date: string) => {
  const splitted = date.split('-');
  return `${splitted[2]}-${splitted[1]}`;
}

export const convertInitInput = (input: string) => {
  if (validateEmail(input)) {
    const isAdmin = input === 'admin@bestaff.io';
    return { by: 'email', value: input, admin: isAdmin };
  }
  if (validatePhone(input)) {
    return { by: 'phone', value: input };
  }
  return null;
};
