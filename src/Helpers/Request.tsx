import axios from 'axios';
import { getToken } from '../Helpers/LocalStorage';
// const ROOT_URL = 'https://app.bestaff.io/api';
const ROOT_URL = 'http://35.240.142.38/api';

const http = axios.create({
  baseURL: ROOT_URL,
  timeout: 0,
  headers: {
    'Content-Type': 'application/json',
    'Accept': 'application/json',
  }
});

// request configs
http.interceptors.request.use(
  async config => {
    const token = await getToken();
    if (token) {
      config.headers.Authorization = `Bearer ${token}`;
    }
    return config
  },
  error => {
    return Promise.reject(error)
  }
);

// response configs
http.interceptors.response.use(
  (response: any) => response,
  (error: any) => error,
);

export { http };
export default http;
