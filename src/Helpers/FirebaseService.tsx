export const firebaseConfig = {
    apiKey: "AIzaSyC40pmm1XaTxxrtAYIM-JypvUmYc85FEAY",
    authDomain: "bestaff.firebaseapp.com",
    databaseURL: "https://bestaff.firebaseio.com",
    projectId: "bestaff",
    storageBucket: "bestaff.appspot.com",
    messagingSenderId: "825394733272",
    appId: "1:825394733272:web:a96e050fcf0b25ad67b7ad",
    measurementId: "G-927GCJ85YL"
};
export const getUserAvatar = (userId: string) => {
    return `https://firebasestorage.googleapis.com/v0/b/bestaff.appspot.com/o/avatar_${userId}.png?alt=media`
}