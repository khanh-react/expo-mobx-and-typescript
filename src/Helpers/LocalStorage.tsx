import AsyncStorage from '@react-native-community/async-storage';

export const setItem = async (itemName: string, value: string) => {
    try {
        await AsyncStorage.setItem(itemName, value);
    } catch (error) {
        throw error;
    }
}

export const getItem = async (itemName: string) => {
    try {
        const item = await AsyncStorage.getItem(itemName);
        if (item) return item;
        return null;
    } catch (error) {
        throw error;
    }
}

export const isInitialized = async () => {
    try {
        const item = await AsyncStorage.getItem('INITIALZIED_SCREEN');
        if (item) return true;
        return false
    } catch (error) {
        throw error;
    }
}

export const setInitialized = async () => {
    try {
        await AsyncStorage.setItem('INITIALZIED_SCREEN', new Date().toString());
    } catch (error) {
        throw error;
    }
}

export const isDescribed = async () => {
    try {
        return await AsyncStorage.getItem('DESCRIPTION_SCREEN');
    } catch (error) {
        return error;
    }
}

export const setDescribed = async () => {
    try {
        return await AsyncStorage.setItem('DESCRIPTION_SCREEN', new Date().toString());
    } catch (error) {
        return error;
    }
}

export const isGuided = async () => {
    try {
        return await AsyncStorage.getItem('GUIDELINE_SCREEN');
    } catch (error) {
        throw error;
    }
}

export const setGuided = async () => {
    try {
        return await AsyncStorage.setItem('GUIDELINE_SCREEN', new Date().toString());
    } catch (error) {
        throw error;
    }
}

export const getProfileImage = async () => {
    try {
        return await AsyncStorage.getItem('CURRENT_PROFILE_IMAGE')
    } catch (error) {
        throw error;
    }
}

export const setProfileImage = async (uri: string) => {
    try {
        return await AsyncStorage.setItem('CURRENT_PROFILE_IMAGE', uri)
    } catch (error) {
        throw error;
    }
}

export const getToken = async () => {
    try {
        const token = await AsyncStorage.getItem('ACCESS_TOKEN');
        if (token) {
            return JSON.parse(token);
        }
        return null;
    } catch (error) {
        throw error;
    }
}


export const setToken = async (token: string) => {
    try {
        return await AsyncStorage.setItem('ACCESS_TOKEN', JSON.stringify(token));
    } catch (error) {
        throw error;
    }
}

export const getCurrentUserID = async () => {
    try {
        const ID = await AsyncStorage.getItem('CURRENT_USER');
        if (ID) {
            return JSON.parse(ID);
        }
        return '';
    } catch (error) {
        throw error;
    }
}


export const setCurrentUserID = async (id: string) => {
    try {
        return await AsyncStorage.setItem('CURRENT_USER', JSON.stringify(id));
    } catch (error) {
        throw error;
    }
}

export const getReminder = async () => {
    try {
        return await AsyncStorage.getItem('CURRENT_REMINDER')
    } catch (error) {
        throw error;
    }
}


export const setReminder = async (datetime: string | number) => {
    try {
        return await AsyncStorage.setItem('CURRENT_REMINDER', JSON.stringify(datetime));
    } catch (error) {
        throw error;
    }
}

export const getCurrentCheckInTime = async () => {
    try {
        return await AsyncStorage.getItem('CURRENT_CHECKIN_TIME')
    } catch (error) {
        throw error;
    }
}


export const setCurrentCheckInTime = async (timestamp: number) => {
    try {
        return await AsyncStorage.setItem('CURRENT_CHECKIN_TIME', JSON.stringify(timestamp));
    } catch (error) {
        throw error;
    }
}

export const getCurrentPassword = async () => {
    try {
        return await AsyncStorage.getItem('CURRENT_PASSWORD')
    } catch (error) {
        throw error;
    }
}


export const setCurrentPassword = async (password: string) => {
    try {
        return await AsyncStorage.setItem('CURRENT_PASSWORD', JSON.stringify(password));
    } catch (error) {
        throw error;
    }
}

export const setCurrentUserInfo = async (id: string, items: {key: string, value: string}[]) => {
    try {
        let user: any = await AsyncStorage.getItem(`USER_${id}`);
        user = { ...(JSON.parse(user) as any) };
        items.map((item) => {
            user[item.key] = item.value;
        });
        return await AsyncStorage.setItem(`USER_${id}`, JSON.stringify(user));
    } catch (error) {
        throw error;
    }
}

export const getCurrentUserInfo = async (id: string) => {
    try {
        const user = await AsyncStorage.getItem(`USER_${id}`);
        if (user) {
            return JSON.parse(user);
        }
        return null;
    } catch (error) {
        throw error;
    }
}

export const logout = async () => {
    try {
        return await AsyncStorage.removeItem('CURRENT_USER');
    } catch (error) {
        return false;
    }
}