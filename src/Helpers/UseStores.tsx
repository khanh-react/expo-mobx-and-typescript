import * as React from 'react';
import { useObserver, Observer } from 'mobx-react-lite';
import { RootStoreType } from '../Stores/Config';
import { storeContext } from '../Stores/Config/createStores';

const useStoreData = <Selection, ContextData, Store>(
  context: React.Context<ContextData>,
  storeSelector: (contextData: ContextData) => Store,
  dataSelector: (store: Store) => Selection,
) => {
  const value = React.useContext(context);
  if (!value) { throw new Error(); } // React context does not exits!
  const store = storeSelector(value);
  return useObserver(() => dataSelector(store));
};

export const useStores = (dataSelector: (store: RootStoreType) => Selection): any =>
  useStoreData(storeContext, contextData => contextData!, dataSelector);

export default useStores;
