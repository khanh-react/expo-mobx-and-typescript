import * as React from 'react';
import { SvgXml } from 'react-native-svg';

type Props = {
  fill?: string;
  stroke?: string;
};

export default ({ fill = '#909090', stroke = 'white' }: Props) => {
  const xml = `
    <svg width="8" height="8" viewBox="0 0 8 8" fill="none" xmlns="http://www.w3.org/2000/svg">
        <circle cx="4" cy="4" r="4" fill="${fill}"/>
        <path d="M4 0.572266V4.00084H7.42857" stroke="${stroke}"/>
    </svg>
  `;
  return <SvgXml xml={xml} />;
};
