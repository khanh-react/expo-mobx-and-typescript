import * as React from 'react';
import { SvgXml } from 'react-native-svg';

type Props = {
  fill?: string;
  stroke?: string;
};

export default ({ fill = 'none', stroke = 'none' }: Props) => {
  const xml = `
  <svg width="15" height="15" viewBox="0 0 17 19" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M8 19C3.58172 19 0 15.4183 0 11C0 6.58172 3.58172 3 8 3C12.4183 3 16 6.58172 16 11C15.995 15.4162 12.4162 18.995 8 19ZM8 5C4.68629 5 2 7.68629 2 11C2 14.3137 4.68629 17 8 17C11.3137 17 14 14.3137 14 11C13.9961 7.68789 11.3121 5.00386 8 5ZM9 12H7V7H9V12ZM15.293 5.707L13.293 3.707L14.707 2.293L16.707 4.293L15.294 5.706L15.293 5.707ZM11 2H5V0H11V2Z" fill="${fill}"/>
  </svg>
  `;
  return <SvgXml xml={xml} />;
};
