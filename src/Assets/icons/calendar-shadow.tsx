import * as React from 'react';
import { SvgXml } from 'react-native-svg';

type Props = {
  fill?: string;
  stroke?: string;
};

export default ({ fill = 'none', stroke = 'none' }: Props) => {
  const xml = `
  <svg width="18" height="17" viewBox="0 0 18 17" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M15.5 0H2C0.895431 0 0 0.89543 0 2V15C0 16.1046 0.89543 17 2 17H15.5C16.6046 17 17.5 16.1046 17.5 15V2C17.5 0.895431 16.6046 0 15.5 0Z" fill="#CCE9FF"/>
  </svg>
  `;
  return <SvgXml xml={xml} />;
};
