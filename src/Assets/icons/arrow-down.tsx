import * as React from 'react';
import { SvgXml } from 'react-native-svg';

type Props = {
  fill?: string;
  stroke?: string;
};

export default ({ fill = '#c4cad3' }: Props) => {
  const xml = `
    <svg xmlns="http://www.w3.org/2000/svg" width="8" height="4" viewBox="0 0 8 4">
        <path id="chevron" d="M77.285,385.549l4.033,4,3.967-4Z" transform="translate(-77.285 -385.549)" fill="${fill}"/>
    </svg>
  `;
  return <SvgXml xml={xml} />;
};
