import * as React from 'react';
import { SvgXml } from 'react-native-svg';

type Props = {
  fill?: string;
  stroke?: string;
};

export default ({ fill = '#909090', stroke = 'white' }: Props) => {
  const xml = `
  <svg width="17" height="17" viewBox="0 0 17 17" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M8.5 17C3.80558 17 0 13.1944 0 8.5C0 3.80558 3.80558 0 8.5 0C13.1944 0 17 3.80558 17 8.5C16.9948 13.1923 13.1923 16.9948 8.5 17ZM7.65 12.75V14.45H9.35V12.75H7.65ZM8.5 4.25C9.43888 4.25 10.2 5.01112 10.2 5.95C10.203 6.40112 10.0219 6.83395 9.6985 7.1485L8.6445 8.2195C8.00822 8.85846 7.65069 9.72327 7.65 10.625V11.05H9.35C9.28394 10.1363 9.65248 9.24483 10.3445 8.6445L11.1095 7.8625C11.6179 7.35617 11.9025 6.66749 11.9 5.95C11.9 4.07223 10.3778 2.55 8.5 2.55C6.62223 2.55 5.1 4.07223 5.1 5.95H6.8C6.8 5.01112 7.56112 4.25 8.5 4.25Z" fill="black" fill-opacity="0.5"/>
  </svg>
  `;
  return <SvgXml xml={xml} />;
};
