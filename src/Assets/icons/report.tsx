import * as React from 'react';
import { SvgXml } from 'react-native-svg';

type Props = {
  fill?: string;
  stroke?: string;
};

export default ({ fill = 'none', stroke = 'none' }: Props) => {
  const xml = `
  <svg width="18" height="17" viewBox="0 0 18 17" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M16.2 16.2H1.8C0.805887 16.2 0 15.3941 0 14.4V1.8C0 0.805887 0.805887 0 1.8 0H16.2C17.1941 0 18 0.805887 18 1.8V14.4C18 15.3941 17.1941 16.2 16.2 16.2ZM1.8 3.6V14.4H16.2V3.6H1.8ZM6.9354 12.3372L5.6637 11.0637L7.7274 9L5.6637 6.9363L6.9363 5.6637L9 7.7274L11.0637 5.6637L12.3363 6.9363L10.2726 9L12.3363 11.0637L11.0646 12.3354L9 10.2726L6.9363 12.3363L6.9354 12.3372Z" fill="black" fill-opacity="0.5"/>
  </svg>
  `;
  return <SvgXml xml={xml} />;
};
