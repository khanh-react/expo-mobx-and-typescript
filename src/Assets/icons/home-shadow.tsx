import * as React from 'react';
import { SvgXml } from 'react-native-svg';

type Props = {
  fill?: string;
  stroke?: string;
};

export default ({ fill = '#909090', stroke = 'white' }: Props) => {
  const xml = `
    <svg width="15" height="18" viewBox="0 0 15 18" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M0.292893 7.20711L6.79289 0.707107C7.18342 0.316583 7.81658 0.316583 8.20711 0.707107L14.7071 7.20711C14.8946 7.39464 15 7.649 15 7.91421V17C15 17.5523 14.5523 18 14 18H1C0.447715 18 0 17.5523 0 17V7.91421C0 7.649 0.105357 7.39464 0.292893 7.20711Z" fill="#CCE9FF"/>
    </svg>
  `;
  return <SvgXml xml={xml} />;
};
