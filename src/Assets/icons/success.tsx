import * as React from 'react';
import { SvgXml } from 'react-native-svg';

type Props = {
  fill?: string;
  stroke?: string;
};

export default ({ fill = 'none', stroke = 'none' }: Props) => {
  const xml = `
    <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M9 18C4.02944 18 0 13.9706 0 9C0 4.02944 4.02944 0 9 0C13.9706 0 18 4.02944 18 9C17.9945 13.9683 13.9683 17.9945 9 18ZM8.9856 16.2H9C12.975 16.196 16.1948 12.9714 16.1928 8.9964C16.1908 5.02136 12.9678 1.8 8.9928 1.8C5.01776 1.8 1.79479 5.02136 1.7928 8.9964C1.79081 12.9714 5.01056 16.196 8.9856 16.2ZM7.2 13.5L3.6 9.9L4.869 8.631L7.2 10.953L13.131 5.022L14.4 6.3L7.2 13.5Z" fill="white"/>
    </svg>
  `;
  return <SvgXml xml={xml} />;
};
