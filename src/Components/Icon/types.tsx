export type IconProps = {
  source?: any;
  size?: 'small' | 'large' | 'default';
  uri?: string;
  style?: any;
  isActived?: boolean;
  isHidden?: boolean;
  onPress?: (e?: any) => void;
};
