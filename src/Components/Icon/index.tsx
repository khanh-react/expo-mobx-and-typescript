import React from 'react';
import { TouchableOpacity, Image, View } from 'react-native';
import Styles from './styles';
import { IconProps as Props } from './types';

export const Icon = ({ source, uri, size = 'default', style, isHidden, onPress }: Props) => {
  if (isHidden) return <View />
  const styles = [].concat(style);
  return (
    <TouchableOpacity style={[Styles.wrapper, ...styles]} onPress={e => onPress && onPress(e)}>
      { source && <Image style={Styles[size]} source={source} /> }
      { uri && <Image source={{ uri }} resizeMode={'stretch'} width={24} height={24} /> }
    </TouchableOpacity>
  );
};

export default Icon;
