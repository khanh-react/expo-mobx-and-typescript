import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  wrapper: {},
  notification: {},
  settings: {},
  small: {
    width: 20,
    height: 20,
  },
  medium: {
    width: 24,
    height: 24
  },
  large: {
    width: 28,
    height: 28,
  },
  default: {
    width: 24,
    height: 24
  }
});
