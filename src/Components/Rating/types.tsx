export type RatingProps = {
    onChange: (val: number) => void;
}