import React from 'react';
import { View } from 'react-native';
import { Rating, AirbnbRating } from 'react-native-ratings';
import { RatingProps as Props } from './types';

export const IRating = ({ onChange }: Props) => {
    return <AirbnbRating
        defaultRating={2}
        reviewColor={'#00B1FF'}
        selectedColor={'#00B1FF'}
        onFinishRating={onChange}
    />;
}

export default IRating;
