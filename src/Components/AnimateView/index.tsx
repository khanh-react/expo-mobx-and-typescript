import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import { Animated } from 'react-native';

export const FadeInView = ({ style, children }): React.ReactElement => {
  const fadeAnim = useRef(new Animated.Value(0)).current; // Initial value for opacity: 0

  const updateAnimate = () => {
    Animated.timing(fadeAnim, {
      toValue: 1,
      duration: 1000,
      useNativeDriver: true,
    }).start();
  };

  return (
    <Animated.View // Special animatable View
      style={{
        ...style,
        opacity: fadeAnim, // Bind opacity to animated value
      }}
    >
      {updateAnimate()}
      {children}
    </Animated.View>
  );
};

FadeInView.propTypes = {
  style: PropTypes.any,
  children: PropTypes.any,
};

export default FadeInView;
