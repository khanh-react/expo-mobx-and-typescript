import React, { useState } from 'react';
import { View } from 'react-native';
import Checkbox from 'react-native-check-box';
import { CheckboxProps as Props } from './types';
import ThemeColors from '../../Theme/Colors';

export const ICheckbox = ({ style, isChecked, onPress }: Props): React.ReactElement => {
  const styles = [].concat(style);
  const [checked, setChecked] = useState(isChecked);
  const handleChecked = () => {
    if (checked) {
      onPress(false);
      setChecked(false);
      return;
    }
    onPress(true);
    setChecked(true);
  };

  return (
    <Checkbox
      style={styles}
      isChecked={checked}
      uncheckedCheckBoxColor={ThemeColors.black}
      checkedCheckBoxColor={ThemeColors.main}
      onClick={() => handleChecked()}/>
    )
};

export default ICheckbox;
