export type CheckboxProps = {
  isChecked: boolean;
  style?: any;
  onPress: (val?: string) => void;
};
