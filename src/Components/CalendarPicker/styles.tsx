import { StyleSheet, Dimensions } from 'react-native';

export default StyleSheet.create({
  wrapper: {
    width: Dimensions.get('screen').width,
  },
});
