import React from 'react';
import { View } from 'react-native';
import { Calendar } from 'react-native-calendars';
import Styles from './styles';
import { CalendarPickerProps as Props } from './types';
import ThemeColors from '../../Theme/Colors';

export const CalendarPicker = ({
  style,
  isHidden,
  currentYear,
  currentMonth,
  currentMonday,
  onChangeDay,
  onChangeMonth,
}: Props): React.ReactElement => {
  const styles = [].concat(style as any);
  const white = ThemeColors.white;
  const primary = ThemeColors.primary;
  if (isHidden) return <View />;
  const markDates = {};
  if (currentMonday) {
    [0, 1, 2, 3, 4, 5, 6]
    .forEach((date: number) => {
      let _date = currentMonday + date;
      if (_date  < 10) { _date = `0${_date}` as never; }
      markDates[`${currentYear}-${currentMonth}-${_date}`] = {
        startingDay: date === 0 ? true : false,
        color: primary,
        text: white,
        selected: true,
        endingDay: date === 6 ? true : false,
        marked: true, selectedColor: 'red',
      };
    });
  }

  const handleMonthChanged = (val: string) => {
    const payload = {
      year: val.split('-')[0],
      month: val.split('-')[1],
    };
    onChangeMonth(payload);
  };

  return <Calendar
    minDate={'2012-05-10'}
    style={[Styles.wrapper, ...styles]}
    // enableSwipeMonths={true}
    theme={{
      textSectionTitleDisabledColor: '#d9e1e8',
    }}
    hideExtraDays={false}
    disableAllTouchEventsForDisabledDays={true}
    markedDates={markDates}
    markingType={'period'}
    firstDay={1}
    onDayPress={(val: any) => onChangeDay(val.timestamp) }
    onMonthChange={(val: any) => handleMonthChanged(val.dateString)}/>;
};

export default CalendarPicker;
