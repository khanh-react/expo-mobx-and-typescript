export type CalendarPickerProps = {
  style?: unknown;
  isHidden?: boolean;
  currentMonday?: number;
  currentMonth?: number;
  currentYear?: number;
  onChangeDay: (val: any) => void;
  onChangeMonth: (val: any) => void;
};
