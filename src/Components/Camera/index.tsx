import React, { useState } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { Camera } from 'expo-camera';

type AppCameraProps = {
  isVisible: boolean;
};

export const AppCamera = ({ isVisible }: AppCameraProps): React.ReactElement => {
  const [type, setType] = useState(Camera.Constants.Type.back);

  if (!isVisible) return null;

  return (
    <Camera style={{ flex: 1 }} type={type}>
      <View
        style={{
          flex: 1,
          backgroundColor: 'transparent',
          flexDirection: 'row',
        }}>
        <TouchableOpacity
          style={{
            flex: 0.1,
            alignSelf: 'flex-end',
            alignItems: 'center',
          }}
          onPress={() => {
            setType(
              type === Camera.Constants.Type.back
                ? Camera.Constants.Type.front
                : Camera.Constants.Type.back,
            );
          }}>
          <Text style={{ fontSize: 18, marginBottom: 10, color: 'white' }}> Flip </Text>
        </TouchableOpacity>
      </View>
    </Camera>
  );
};

export default AppCamera;
