import { StyleSheet } from 'react-native';

export const Styles = StyleSheet.create({
    wrapper: {
        paddingLeft: 5,
        paddingRight: 5,
        paddingBottom: 3,
        paddingTop: 2,
        backgroundColor: '#FFF',
        borderRadius: 5,
    },
    text: {
        textTransform: 'uppercase',
        fontSize: 10,
    }
})