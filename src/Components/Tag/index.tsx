import React from 'react';
import { View } from 'react-native';
import Text from '../Text';
import { TagProps as Props } from './types';
import { Styles } from './styles';
import Colors from '../../Theme/Colors';

export const Tag = ({
    value, type = 'default', size = 'small', style, bgColor, isHidden
}: Props) => {
    if (isHidden) return null;
    const styles = [].concat(style);
    const bg = bgColor ? { backgroundColor: bgColor } : null;
    return (
        <View style={[Styles.wrapper, ...styles, bg]}>
            <Text style={Styles.text} weight={500} color={Colors.main} value={value}/>
        </View>
    )
}

export default Tag;
