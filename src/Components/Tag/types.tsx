export type TagProps = {
    value: string;
    type?: 'default' | 'primary' | 'danger';
    size?: 'small' | 'medium' | 'large';
    color?: string;
    style?: any;
    bgColor?: string;
    isHidden?: boolean;
}