import React from 'react';
import { View } from 'react-native';
import PaginationStyles from './styles';
import { PaginationProps as Props } from './types';

export const Pagination = (props: Props) => {
  const ListPages = [];
  for (let page = 1; page <= props.pages; page++) {
    let paginateStyle = PaginationStyles.itemActive;
    if (props.currentPage !== page) { paginateStyle = PaginationStyles.itemInactive; }
    ListPages.push(
      <View key={page} style={[PaginationStyles.item, paginateStyle]}/>,
    );
  }
  return (
    <View style={PaginationStyles.wrapper}>{ListPages}</View>
  );
};

export default Pagination;
