import { StyleSheet } from 'react-native';
import ThemeColors from '../../Theme/Colors';
import ThemeHelpers from '../../Theme/Helpers';

export default StyleSheet.create({
  wrapper: {
    ...ThemeHelpers.rowCenter,
    width: '100%',
    height: 30,
  },
  item: {
    width: 10,
    height: 10,
    marginLeft: 10,
    marginRight: 10,
    borderRadius: 50,
  },
  itemInactive: {
    backgroundColor: ThemeColors.silver,
  },
  itemActive: {
    backgroundColor: ThemeColors.main,
  },
});
