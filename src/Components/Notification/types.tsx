export type NotificationProps = {
    openAt: number;
    status: 200 | 500;
    message?: string;
}