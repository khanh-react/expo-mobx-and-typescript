import { StyleSheet, Dimensions } from 'react-native';
import Helpers from '../../Theme/Helpers';

const height = Dimensions.get('screen').height;

export const Styles = StyleSheet.create({
    wrapper: {
        ...Helpers.center,
        position: 'absolute',
        width: '100%',
        top: (height / 2) - 100,
    },
    messageWrapper: {
        ...Helpers.rowCenter,
        backgroundColor: '#000',
        borderRadius: 8,
        paddingTop: 3,
        paddingBottom: 5,
        paddingLeft: 12,
        paddingRight: 12,
        position: 'relative',
    },
    icon: {
        width: 15,
        height: 15,
    },
    mgr4: {
        marginRight: 4,
    },
    mrl5: {
        marginLeft: 5,
    }
});

export default Styles;
