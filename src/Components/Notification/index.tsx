import React, { useEffect, useState } from 'react';
import { View, Image } from 'react-native';
import { Styles } from './styles';
import { NotificationProps as Props } from './types';
import Colors from '../../Theme/Colors';
import { IText as Text } from '../../Components/Text';
import { useStores } from '../../Helpers/UseStores';
import SuccessIcon from '../../Assets/icons/success';

export const Notification = () => {
    const {
        errorShiftNotify,
        successShiftNotify,
        clearShiftNotify,

        successCalendarNotify,
        errorCalendarNotify,
        clearCalendarNotify,
    } = useStores(store => ({
        status: store.toggle.notificationStatus,
        message: store.toggle.notificationMessage,
        openAt: store.toggle.currentOpenAt,
        onClear: store.toggle.clearNotification,
        errorShiftNotify: store.shift.errorNotify,
        successShiftNotify: store.shift.successNotify,
        clearShiftNotify: store.shift.clearNotify,

        successCalendarNotify: store.calendar.successNotify,
        errorCalendarNotify: store.calendar.errorNotify,
        clearCalendarNotify: store.calendar.clearNotify,
    } as any));
    const icon = require('../../Assets/noti_success.png');
    // const _message = message ? message : `${status === 200 ? 'Thành công' : 'Thất bại'}`;
    const successMessage = successShiftNotify || successCalendarNotify;
    const errorMessage = errorShiftNotify || errorCalendarNotify;

    useEffect(() => {
        setTimeout(() => {
            clearShiftNotify();
            clearCalendarNotify();
        }, 3000)
    }, [successShiftNotify, errorShiftNotify, successCalendarNotify, errorCalendarNotify]);

    if (successMessage || errorMessage) {
        return (
            <View style={Styles.wrapper}>
                <View style={Styles.messageWrapper}>
                    { successMessage ? <SuccessIcon /> : <Text  value="X" color={Colors.red} /> }
                    <Text style={Styles.mrl5} color="#FFF" value={successMessage || errorMessage}/>
                </View>
            </View>
        )
    }

    return null;
}

export default Notification;
