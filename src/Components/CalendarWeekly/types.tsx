export type CalendarWeeklyProps = {
  statingDate?: number;
  onWeekChanged: (val?) => void,
  onDateSelected: (val?) => void,
  onSwipeLeft?: () => void,
  onSwipeRight?: () => void,
};
