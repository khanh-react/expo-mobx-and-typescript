import { StyleSheet } from 'react-native';
import ThemeColors from '../../Theme/Colors';

export default StyleSheet.create({
  wrapper: {
    width: '100%',
  },
  container: {
    width: '100%',
    height: 70,
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 0,
  },
  textHeader: {
    color: ThemeColors.black,
    fontSize: 12,
    textTransform: 'capitalize',
    display: 'none',
  },
});
