import React, { useState } from 'react';
import CalendarStrip from 'react-native-calendar-strip';
import GestureRecognizer from 'react-native-swipe-gestures';
import styles from './styles';
import { CalendarWeeklyProps as Props } from './types';
import weekConfigs from './configs';
import ThemeColors from '../../Theme/Colors';
import moment from 'moment';
import 'moment/locale/vi';

const locale = {
  name: 'vi',
  config: weekConfigs,
};

const WeekCalendar = ({
  statingDate,
  onWeekChanged,
  onDateSelected,
  onSwipeLeft,
  onSwipeRight,
}: Props): React.ReactElement => {
  const customDatesStyles = [];
  // const customDatesStyles = [{
  //   startDate: statingDate, // Single date since no endDate provided
  //   dateContainerStyle: {
  //     height: 43, width: 43,
  //     borderWidth: 1,
  //     borderColor: ThemeColors.main,
  //   },
  // }];

  return (
    <GestureRecognizer style={styles.wrapper}
      // onSwipeRight={() => onSwipeRight && onSwipeRight()}
      // onSwipeLeft={() => onSwipeLeft && onSwipeLeft()}
      >
      <CalendarStrip
        style={styles.container}
        calendarHeaderStyle={styles.textHeader}
        calendarColor={'white'}
        dateNumberStyle={{ color: 'black', fontSize: 13 }}
        dateNameStyle={{ color: 'black', fontSize: 11 }}
        highlightDateNameStyle={{ color: ThemeColors.main, fontSize: 11 }}
        highlightDateNumberStyle={{ color: ThemeColors.main, fontSize: 13 }}
        iconLeftStyle={{ width: 15, opacity: 1 }}
        iconRightStyle={{ width: 15, opacity: 1 }}
        locale={locale}
        customDatesStyles={customDatesStyles}
        startingDate={statingDate ? moment(new Date(statingDate)) : moment()}
        onWeekChanged={onWeekChanged}
        onDateSelected={onDateSelected}
        useIsoWeekday={true}
        selectedDate={moment()}
      />
    </GestureRecognizer>
  );
};

export default WeekCalendar;
