import React, { useEffect } from 'react';
import { View, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import Styles from './styles';
import { FooterProps } from './types';
import Icon from '../Icon';
import HomeSvg from '../../Assets/icons/home';
import HomeShadowSvg from '../../Assets/icons/home-shadow';
import CalendarSvg from '../../Assets/icons/calendar';
import CalendarShadowSvg from '../../Assets/icons/calendar-shadow';

export const Footer = ({ style, currentRoute, isHidden, onChangeRoute }: FooterProps): React.ReactElement => {
  let homeIcon = require('../../Assets/home.png');
  let calendarIcon = require('../../Assets/calendar.png');
  const chartIcon = require('../../Assets/line_chart_up.png');

  const navigation = useNavigation();

  if (isHidden) return <View />;

  if (currentRoute === 'Home') {
    homeIcon = require('../../Assets/home_active.png');
    calendarIcon = require('../../Assets/calendar.png');
  }

  if (currentRoute === 'Calendar') {
    homeIcon = require('../../Assets/home.png');
    calendarIcon = require('../../Assets/calendar_active.png');
  }

  const styles = [].concat(style as any);

  const handleNavigate = (_route: string) => {
    onChangeRoute(_route);
    navigation.navigate(_route);
  };

  return (
    <View style={[Styles.wrapper, ...styles]}>
      <View style={Styles.items}>
        <View style={Styles.footItem}>
          <TouchableOpacity style={Styles.footItemInside} onPress={() => handleNavigate('Home')}>
          {
            <>
              <View style={Styles.iconWrap}>
                <HomeSvg fill={currentRoute === 'Home' ? '#28A0F9' : undefined}/>
              </View>
              {currentRoute === 'Home' && <View style={Styles.iconWrapShadow}><HomeShadowSvg /></View>}
            </>
          }
          </TouchableOpacity>
        </View>
        <View style={Styles.footItem}>
        <TouchableOpacity style={Styles.footItemInside} onPress={() => handleNavigate('Calendar')}>
          {
            <>
              <View style={Styles.iconWrap}>
                <CalendarSvg fill={currentRoute === 'Calendar' ? '#28A0F9' : undefined}/>
              </View>
              {currentRoute === 'Calendar' && <View style={Styles.iconWrapShadow}><CalendarShadowSvg /></View>}
            </>
          }
          </TouchableOpacity>
          {/* <Icon style={Styles.icon} source={calendarIcon} onPress={() => handleNavigate('Calendar')}/> */}
        </View>
        {/* <View style={Styles.footItem}>
          <Icon source={chartIcon} onPress={() => handleNavigate('Speech')}/>
        </View> */}
      </View>
    </View>
  );
};

export default Footer;
