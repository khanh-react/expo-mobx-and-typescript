import { StyleSheet } from 'react-native';
import ThemeHelpers from '../../Theme/Helpers';

export default StyleSheet.create({
  wrapper: {
    ...ThemeHelpers.center,
    width: '100%',
    height: 50,
    bottom: 10,
    position: 'absolute',
  },
  items: {
    ...ThemeHelpers.rowCenter,
    ...ThemeHelpers.shadow,
    width: '80%',
    height: '100%',
    borderRadius: 30,
    backgroundColor: '#FFF',
    borderColor: '#EEE',
    borderWidth: 1,
  },
  footItem: {
    ...ThemeHelpers.colCenter,
    width: '50%',
    height: '100%',
  },
  footItemInside: {
    ...ThemeHelpers.center,
    width: 25,
    height: 25,
    position: 'relative',
  },
  iconWrap: {
    position: 'absolute',
    right: 0,
    top: 0,
    zIndex: 1,
  },
  iconWrapShadow: {
    marginTop: 3,
  },
  icon: {
    width: 24,
    height: 24,
  }
});
