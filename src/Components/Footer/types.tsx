export type FooterProps = {
  currentRoute?: string;
  style?: unknown;
  init?: string;
  isHidden?: boolean;
  onChangeRoute: (route: string) => void;
};
