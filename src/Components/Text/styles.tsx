import { StyleSheet } from 'react-native';
import ThemeHelpers from '../../Theme/Helpers';

export default StyleSheet.create({
  wrapper: {
    ...ThemeHelpers.row,
  },
});
