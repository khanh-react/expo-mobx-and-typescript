import React from 'react';
import { array } from 'prop-types';
import { Text } from 'react-native';
import { TextProps as Props } from './types';
import Styles from './styles';

export const IText = ({
  style,
  size = 'small',
  weight = 400,
  value = '',
  type,
  color,
  numberOfLine,
  children,
}: Props) => {
  if (!value && !children) return null;
  // tslint:disable-next-line:one-variable-per-declaration
  let _size = 20, _fontFamily = 'Barlow_400Regular', _color = { color }, styles = style;
  if (size === 'smaller') { _size = 12; }
  if (size === 'small') { _size = 14; }
  if (size === 'xsmall') { _size = 16; }
  if (size === 'medium') { _size = 20; }
  if (size === 'large') { _size = 24; }
  if (size === 'xlarge') { _size = 30; }
  if (size === 'xxlarge') { _size = 34; }

  if (type === 'bold') { _fontFamily = 'Barlow_600SemiBold'; }
  if (type === 'xbold') { _fontFamily = 'Barlow_700Bold'; }
  if (type === 'bolder') { _fontFamily = 'Barlow_800ExtraBold'; }
  if (type === 'bold_italic') { _fontFamily = 'Bold_Italic'; }

  if (weight === 500) { _fontFamily = 'Barlow_500Medium'; }

  const _style = {
    fontFamily: _fontFamily,
    fontSize: _size,
  };

  if (style && style !== array) styles = [style];
  if (!styles) styles = [];
  if (!color) _color = { color: '#000000' };
  return <Text style={[
    Styles.wrapper,
    _style,
    _color,
    ...styles,
  ]}
    numberOfLines={numberOfLine || 1000}>
    {value || children}
  </Text>;
};

export default IText;
