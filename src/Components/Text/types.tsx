export type TextProps = {
  value?: string;
  style?: any;
  size?: 'small' | 'smaller' | 'xsmall' | 'medium' | 'large' | 'xlarge' | 'xxlarge' | '';
  type?: 'bold' | 'xbold' | 'bolder' | 'bold_italic' | '';
  weight?: number;
  color?: any;
  numberOfLine?: number;
  isBold?: boolean;
  children?: React.ReactNode;
};
