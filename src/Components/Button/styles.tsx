import { StyleSheet } from 'react-native';
import ThemeHelpers from '../../Theme/Helpers';
import ThemeColors from '../../Theme/Colors';

export default StyleSheet.create({
  wrapper: {
    ...ThemeHelpers.rowCenter,
    width: '100%',
    height: 55,
    borderRadius: 30,
    backgroundColor: '#28A0F9',
    justifyContent: 'center',
    alignItems: 'center',
  },
  mediumHeight: {
    height: 35,
  },
  disabledWrapper: {
    width: '100%',
    height: 55,
    borderRadius: 30,
    backgroundColor: '#F6F6F6',
    borderColor: '#F0F0F0',
    justifyContent: 'center',
    alignItems: 'center',
  },
  defaultWrapper: {
    backgroundColor: '#ffffff',
    borderColor: ThemeColors.primary,
    borderWidth: 1,
  },
  outlinePrimaryWrapper: {
    backgroundColor: '#ffffff',
    borderColor: ThemeColors.main,
    borderWidth: 1,
  },
  outlineDangerWrapper: {
    backgroundColor: '#ffffff',
    borderColor: ThemeColors.red,
    borderWidth: 1,
  },
  title: {
    color: '#ffffff',
    fontFamily: 'Barlow_600SemiBold',
  },
  disabledTitle: {
    color: 'silver',
    fontSize: 20,
  },
  defaultTitle: {
    color: ThemeColors.primary,
  },
  defaultTitleSize: {
    fontSize: 20,
  },
  mediumTitleSize: {
    fontSize: 15,
  },
  notBold: {
    fontWeight: '100',
  },
  dangerTitle: {
    color: ThemeColors.red,
  },
  primaryTitle: {
    color: ThemeColors.main,
  },
  icon: {
    maxWidth: 24,
    maxHeight: 24,
    marginRight: 3,
    marginTop: 3,
  },
  mediumIcon: {
    maxWidth: 20,
    maxHeight: 20,
    marginRight: 3,
    marginTop: 2,
  }
});
