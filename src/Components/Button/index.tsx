import React from 'react';
import { TouchableOpacity, View, Text, Image } from 'react-native';
import Styles from './styles';
import { ButtonProps as Props } from './types';
import { Icon } from '../Icon';

export const Button = ({
  style,
  size = 'large',
  text,
  type = 'primary',
  icon,
  disabled,
  isLoading,
  onPress,
}: Props): React.ReactElement => {
  const _styles = [].concat(style);
  const loadingIcon = require('../../Assets/loading.gif');

  if (disabled) {
    return (
      <View style={[Styles.disabledWrapper, ..._styles]}>
        {
          isLoading ? <Icon source={loadingIcon} /> :
            <Text style={Styles.disabledTitle}>{text}</Text>
        }
      </View>
    );
  }
  return (
    <TouchableOpacity
      style={[
        Styles.wrapper,
        size === 'medium' && Styles.mediumHeight,
        ..._styles,
        type === 'default' && Styles.defaultWrapper,
        type === 'outline-primary' && Styles.outlinePrimaryWrapper,
        type === 'outline-danger' && Styles.outlineDangerWrapper,
      ]}
      onPress={e => onPress && onPress(e)}>
      { icon && <Image style={size === 'large' ? Styles.icon : Styles.mediumIcon} source={icon}/> }
      {
        isLoading ?
        <Icon source={loadingIcon} /> :
        <Text
          style={[
            Styles.title, Styles.notBold,
            type === 'default' && Styles.defaultTitle,
            type === 'outline-danger' && Styles.dangerTitle,
            type === 'outline-primary' && Styles.primaryTitle,
            size === 'large' ? Styles.defaultTitleSize : Styles.mediumTitleSize,
          ]}>{text}</Text>
      }
    </TouchableOpacity>
  );
};

export default Button;
