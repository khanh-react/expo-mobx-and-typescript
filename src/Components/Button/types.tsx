export type ButtonProps = {
  style?: any;
  text?: string;
  size?: 'medium' | 'large';
  type?: 'default' | 'primary' | 'danger' | 'outline' | 'outline-primary' | 'outline-danger';
  icon?: string;
  disabled?: boolean;
  isLoading?: boolean;
  onPress?: (e?: any) => void;
};
