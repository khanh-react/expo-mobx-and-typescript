import { StyleSheet } from 'react-native';
import Helpers from '../../Theme/Helpers';

export const Styles = StyleSheet.create({
    wrapper: {
        ...Helpers.rowCross,
    },
    roleIcon: {
        width: 8,
        height: 8,
        marginRight: 5,
        backgroundColor: 'purple',
        borderRadius: 7,
        paddingTop: 0,
    },
})