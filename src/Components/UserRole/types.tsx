export type UserRoleProps = {
    roleName: 'Phục vụ' | 'Thu ngân' | 'Bảo vệ' | 'Giữ xe';
    style?: any;
}