import React from 'react';
import { View } from 'react-native';
import { Styles } from './styles';
import { UserRoleProps as Props } from './types';
import { IText as Text } from '../Text';

export const UserRole = ({ roleName, style }: Props) => {
    const styles = [].concat(style);
    let color = 'purple';
    if (roleName === 'Thu ngân') color = 'green';
    if (roleName === 'Giữ xe') color = 'black';
    if (roleName === 'Bảo vệ') color = 'blue';
    return (
        <View style={[Styles.wrapper, ...styles]}>
            <View style={Styles.roleIcon} />
            <Text style={{ marginRight: 5, color: color }} size="xsmall" value={roleName} />
        </View>
    )
}