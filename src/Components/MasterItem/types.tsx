export type MasterItemProps = {
  icon?: any;
  label?: string;
  value?: string;
  color?: string;
  type?: 'default' | 'phone' | 'email' | 'password' | 'id' | 'gender' | 'birthday' | 'location' | 'position' | 'setting';
  isValid?: boolean;
  isRequired?: boolean;
  isLabel?: boolean;
  isBlocked?: boolean;
  onPress?: (e?: any) => void;
};
