import { StyleSheet } from 'react-native';
import ThemeHelpers from '../../Theme/Helpers';

export default StyleSheet.create({
  wrapper: {
    ...ThemeHelpers.row,
    width: '100%',
    height: 50,
  },
  iconWrap: {
    ...ThemeHelpers.colCenter,
    width: 60,
  },
  warn: {
    position: 'absolute',
    zIndex: 1,
    bottom: 0,
    right: 7,
  },
  contentWrap: {
    ...ThemeHelpers.colMain,
    width: '80%',
    borderBottomWidth: 1,
    borderBottomColor: '#e0e0e0',
  },
  content: {
    width: 40,
    height: 40,
  },
  tag: {
    position: 'absolute',
    right: 0,
  }
});
