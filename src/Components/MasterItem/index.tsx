import React from 'react';
import { View, Image, TouchableOpacity } from 'react-native';
import Styles from './styles';
import { MasterItemProps as Props } from './types';
import Text from '../Text';
import { Icon } from '../Icon';
import { Tag } from '../Tag';

const MasterItem = ({
  icon, label, value, type = 'default', color,
  isLabel, isRequired, isBlocked = false,
  onPress,
}: Props) => {
  const isValid = !value ? false : true;
  let _value = type !== 'password' ? value : '**********';
  if (!value) { _value = `Cập nhật ${label}`; }

  if (isLabel) {
    return (
      <View style={Styles.wrapper}>
        <View style={Styles.iconWrap}>
          { icon && icon }
          { !value && isRequired && <Icon style={Styles.warn} size="small" source={require('../../Assets/warn.png')}/>}
        </View>
        <View style={Styles.contentWrap}>
          <Text size="smaller" value={value ? label : ''} numberOfLine={1}/>
          <Text size="xsmall" weight={500} color={!isValid && '#31A7FF'} value={_value} numberOfLine={1}/>
        </View>
      </View>
    );
  }

  return (
    <TouchableOpacity style={Styles.wrapper} onPress={e => onPress && onPress(e)}>
      <View style={Styles.iconWrap}>
        { icon && icon }
        { !value && isRequired && <Icon style={Styles.warn} size="small" source={require('../../Assets/warn.png')}/>}
      </View>
      <View style={Styles.contentWrap}>
        {
          type === 'setting' ?
          <Text size="xsmall" weight={500} color={color || (!isValid && '#31A7FF')} value={_value} numberOfLine={1}/>:
          <Text size="smaller" color={color} value={value ? label : ''} numberOfLine={1}/>
        }
        {
          type === 'setting' ?
          <Text size="smaller" color={color} value={value ? label : ''} numberOfLine={1}/>:
          <Text size="xsmall" weight={500} color={color || (!isValid && '#31A7FF')} value={_value} numberOfLine={1}/>
        }
        <Tag isHidden={!isBlocked} style={Styles.tag} value="Sắp ra mắt" bgColor="#eeeeee"/>
      </View>
    </TouchableOpacity>
  );
};

export default MasterItem;
