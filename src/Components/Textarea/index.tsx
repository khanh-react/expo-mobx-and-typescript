import React, { useEffect, useState } from 'react';
import { TextInput, Platform, Keyboard } from 'react-native';
import { Styles } from './styles';
import { TextareaProps as Props } from './types';

export const Textarea = ({
  style,
  value,
  size = 'default',
  placeholder,
  numberOfLine,
  onChange,
}: Props) => {
    const isIOS = Platform.OS === 'ios';
    const [keyboardHeight, setKeyboardHeight] = useState(0);
    const _style = [].concat(style);

    useEffect(() => {
      if (isIOS) {
        Keyboard.addListener('keyboardDidShow', e => setKeyboardHeight(e.endCoordinates.height + 230))
        Keyboard.addListener('keyboardDidHide', () => setKeyboardHeight(0))
      }
      return () => setKeyboardHeight(0);
    }, []);

    return (
      <TextInput
        style={[
          Styles.wrapper,
          isIOS && {marginTop: -keyboardHeight},
          ..._style,
          size === 'default' && Styles.default,
          size === 'small' && Styles.small,
        ]}
        placeholder={placeholder}
        numberOfLines={numberOfLine || 1000}
        onChangeText={onChange}/>
    )
}