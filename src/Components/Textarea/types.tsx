export type TextareaProps = {
    value?: string;
    style?: any;
    size?: 'default' | 'small',
    placeholder?: string;
    numberOfLine?: number;
    onChange?: (val: string) => void;
}