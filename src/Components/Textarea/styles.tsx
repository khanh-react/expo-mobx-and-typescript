import { StyleSheet } from 'react-native';

export const Styles = StyleSheet.create({
    wrapper: {
        width: '100%',
        borderWidth: 1,
        borderColor: 'silver',
        borderRadius: 10,
        textAlignVertical: 'top',
        padding: 10,
        backgroundColor: '#F6F6F6'
    },
    default: {
        minHeight: 150,
    },
    small: {
        minHeight: 55,
    }
})