import React from 'react';
import InnerHTML from 'react-native-render-html';
import { HtmlProps as Props } from './types';

const HTML = ({ content }: Props): React.ReactElement => <InnerHTML html={content} />;

export default HTML;
