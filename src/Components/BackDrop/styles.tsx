import { StyleSheet, Dimensions } from 'react-native';
import ThemeHelpers from '../../Theme/Helpers';

export default StyleSheet.create({
  wrapper: {
    ...ThemeHelpers.fill,
    flex: 1,
    width: Dimensions.get('screen').width,
    height: '100%',
    backgroundColor: '#000000',
    position: 'absolute',
    opacity: 0.8,
    top: 0,
    left: 0,
    zIndex: 100,
  },
  container: {
    height: 100,
  },
});
