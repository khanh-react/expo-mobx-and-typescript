import React from 'react';
import { View } from 'react-native';
import BackDropStyles from './styles';

type BackDropProps = {
  isOpened: boolean;
  marginTop?: number;
  onPress?: (e: any) => void;
  children?: React.ReactNode;
};

export const BackDrop = ({
  children,
  isOpened,
  marginTop,
  onPress,
}: BackDropProps): React.ReactElement => {
  if (isOpened) {
    return (
      <View style={[BackDropStyles.wrapper, { marginTop : marginTop ? marginTop : 60 }]}
        onTouchEnd={event => onPress && onPress(event)}>
        {children}
      </View>
    );
  }
  return <View />;
};

export default BackDrop;
