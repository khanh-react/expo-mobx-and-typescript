import { StyleSheet } from 'react-native';
import ThemeColors from '../../Theme/Colors';

export default StyleSheet.create({
  wrapper: {
    width: '100%',
    height: 55,
    borderWidth: 1,
    borderRadius: 30,
    borderColor: '#EFEFEF',
    backgroundColor: '#F6F6F6',
    justifyContent: 'center',
    position: 'relative',
    marginTop: 5,
    marginBottom: 5,
  },
  text: {
    paddingLeft: 20,
    paddingTop: 5,
  },
  alignLeft: {
    textAlign: 'left',
  },
  alignRight: {
    textAlign: 'right',
  },
  password: {},
  eye: {
    position: 'absolute',
    right: 10,
  },
  primary: {
    borderColor: ThemeColors.main,
    borderWidth: 1,
    borderRadius: 20,
  },
});
