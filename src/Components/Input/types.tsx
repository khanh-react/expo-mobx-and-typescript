import React from 'react';

export type InputProps = {
  value: string;
  ref?: any;
  placeholder?: string;
  style?: any;
  type?: 'password' | 'number';
  isHidden?: boolean;
  isAlignRight?: boolean;
  isAlignLeft?: boolean;
  isPrimary?: boolean;
  isFocused?: boolean;
  isDynamicHeight?: boolean;
  onChangeText?: (e: any) => void;
  onPress?: (e: any) => void;
  children?: React.ReactChildren;
};
