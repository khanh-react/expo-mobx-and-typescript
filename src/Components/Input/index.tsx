import React, {useState, useEffect } from 'react';
import { TextInput, View, Image, Keyboard, Platform } from 'react-native';
import Styles from './styles';
import { InputProps as Props } from './types';

export const Input = ({
  value,
  style,
  ref,
  type,
  placeholder,
  isHidden,
  isAlignRight,
  isAlignLeft,
  isPrimary,
  isFocused,
  isDynamicHeight,
  onChangeText,
  onPress,
  children,
}: Props)=> {
  const isIOS = Platform.OS === 'ios';
  const [visibled, setVisibled] = useState(false);
  const [keyboardHeight, setKeyboardHeight] = useState(0);
  const favIcon = require('../../Assets/eye_icon.png');
  useEffect(() => {
    if (isIOS) {
      Keyboard.addListener('keyboardDidShow', e => setKeyboardHeight(e.endCoordinates.height + 230))
      Keyboard.addListener('keyboardDidHide', () => setKeyboardHeight(0))
    }
    return () => setKeyboardHeight(0);
  }, [])
  if (isHidden) return null;
  const styles = [].concat(style);
  return (
    <View style={[Styles.wrapper, isDynamicHeight && {marginTop: -keyboardHeight}]}>
      <TextInput
        value={value}
        ref={ref}
        keyboardType={type === 'number' ? 'numeric' : 'default'}
        placeholderTextColor="#BDBDBD"
        // eslint-disable-next-line react/no-children-prop
        children={children}
        secureTextEntry={type === 'password' && !visibled}
        style={[
          ...styles,
          Styles.text,
          isAlignRight && Styles.alignRight,
          isAlignLeft && Styles.alignLeft,
          isPrimary && Styles.primary,
        ]}
        onChangeText={val => onChangeText && onChangeText(val)}
        autoFocus={isFocused}
        // onTouchStart={() => onPress()}
        placeholder={placeholder} />
      { type === 'password' && <View
        onTouchStart={() => setVisibled(true)}
        onTouchEnd={() => setVisibled(false)}
        style={Styles.eye}>
        <Image source={favIcon} width={20} height={20} />
      </View>}
    </View>
  );
};

export default Input;
