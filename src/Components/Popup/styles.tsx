import { StyleSheet, Dimensions } from 'react-native';
import ThemeHelpers from '../../Theme/Helpers';

const width = Dimensions.get('screen').width;

export const Styles = StyleSheet.create({
    wrapper: {
        ...ThemeHelpers.row,
        width: width,
        height: 350,
        padding: 10,
        borderTopRightRadius: 30,
        borderTopLeftRadius: 30,
        borderWidth: 1,
        borderColor: 'silver',
        backgroundColor: '#FFF',
        position: 'absolute',
        bottom: 0,
        zIndex: 11,
    },
    backdrop: {
        flex: 1,
        width: width,
        height: '100%',
        position: 'absolute',
        backgroundColor: '#000',
        opacity: 0.7,
        top: 0,
        right: 0,
        zIndex: 10,
    },
})

export default Styles;
