import React from 'react';

export type PopupProps = {
    openAt: number;
    type?: 'default' | 'primary',
    size?: 'default' | 'small' | 'medium' | 'large',
    children?: React.ReactNode;
    onClosed?: () => void;
}