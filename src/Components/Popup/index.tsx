import React, { useState, useEffect } from 'react';
import { View, TouchableOpacity } from 'react-native';
import { Styles } from './styles';
import { PopupProps as Props } from './types';

export const Popup = ({ openAt, onClosed, children }: Props) => {
    const [isHidden, setHidden] = useState(false);

    useEffect(() => {
        setHidden(false);
    }, [openAt])

    if (isHidden) return null;

    const handleClose = () => {
        setHidden(true);
        if (onClosed) onClosed();
    }

    return <>
        <View style={Styles.wrapper}>{children}</View>
        <TouchableOpacity style={Styles.backdrop} onPress={() => handleClose() }/>
    </>
}

export default Popup;
