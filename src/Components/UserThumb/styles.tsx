import { StyleSheet } from 'react-native';
import Helpers from '../../Theme/Helpers';
import Colors from '../../Theme/Colors';

export const Styles = StyleSheet.create({
    wrapper: {
        ...Helpers.center,
        width: 32,
        height: 32,
        padding: 3,
        borderRadius: 16,
        borderWidth: 1,
        marginRight: 2,
        backgroundColor: '#FFF',
    },
    avatarWrapper: {
        ...Helpers.center,
        width: 26,
        height: 26,
        backgroundColor: '#FFF',
        borderRadius: 14,
    },
    avatar: {
        width: 26,
        height: 26,
        borderRadius: 14,
    },
    showNameWrapper: {
        ...Helpers.rowCross,
    },
    default: {
        borderColor: '#e0e0e0',
    },
    nameForAvatar: {
        fontSize: 14,
    },
    me: {
        borderColor: Colors.main,
    }
})

export default Styles;