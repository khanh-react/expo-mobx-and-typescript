export type UserThumbProps = {
    id?: number;
    style?: any;
    avatar?: string;
    uri?: string;
    name?: string;
    isShowShortName?: boolean;
    isShowFullName?: boolean;
};
