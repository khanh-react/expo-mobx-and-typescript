import React from 'react';
import { View, Image } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { Styles } from './styles';
import { UserThumbProps as Props } from './types';
import { IText as Text } from '../Text';

const AvatarWrapper = ({ style, children }: any) => {
    const _style = [].concat(style);
    return (
        <LinearGradient style={[Styles.wrapper, ..._style]} colors={['purple', '#28A0F9']}>
            <View style={Styles.avatarWrapper}>{children}</View>
        </LinearGradient>
    )
}

export const UserThumb = ({ avatar, uri, name = '', isShowShortName, isShowFullName, style }: Props) => {
    const styles = [].concat(style);
    const splited = name.split(' ');
    const length = splited.length;
    let fullName = name;
    let shortName = '';
    let firstName = '';
    let secondName = '';

    if (length === 1) { shortName =  splited[0]}

    if (length >= 2) {
        firstName = splited[length - 1].slice(0,1);
        secondName = splited[length - 2].slice(0,1);
        splited.map((e, index) => {
            if (isShowShortName) {
                if (index < splited.length - 1) {
                    shortName += `${e.slice(0, 1)}.`;
                } else {
                    shortName += e;
                }
            }
        });
    };

    if (avatar || uri) {
        if (!isShowShortName && !isShowFullName) {
            return (
                <AvatarWrapper style={styles}>
                    { avatar && <Image style={Styles.avatar} source={{ uri: avatar}}/> }
                    { uri && <Image style={Styles.avatar} source={{ uri }}/> }
                </AvatarWrapper>
            )
        }
        if (isShowFullName || isShowShortName) {
            return (
                <View style={Styles.showNameWrapper}>
                    <AvatarWrapper>
                        { avatar && <Image style={Styles.avatar} source={{ uri: avatar}}/> }
                        { uri && <Image style={Styles.avatar} source={{ uri }}/> }
                    </AvatarWrapper>
                    <Text value={isShowFullName ? fullName : shortName}/>
                </View>
            );
        }
    }

    if (!avatar || !uri) {
        if (length === 1) return (
            <View><Text value={isShowFullName ? fullName : shortName}/></View>
        );
        if (length >= 2) {
            if (!isShowShortName && !isShowFullName) {
                return (
                    <AvatarWrapper style={[Styles.wrapper, ...styles]}>
                        <Text size="smaller" value={firstName + secondName}/>
                    </AvatarWrapper>
                );
            }

            return (
                <View style={Styles.showNameWrapper}>
                    <AvatarWrapper>
                        <Text style={Styles.nameForAvatar} value={firstName + secondName}/>
                    </AvatarWrapper>
                    <Text value={isShowFullName ? fullName : shortName}/>
                </View>
            );
        }
    }

    return null;
}