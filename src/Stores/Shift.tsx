import PublicIP from 'react-native-public-ip';
import { types, Instance, flow } from 'mobx-state-tree';
import * as _ from 'lodash';
import {
  fetchShifts,
  fetchMyShifts,
  fetchLackShifts,
  checkIn,
  checkOut,
  remind,
  switches,
  find,
} from '../APIs/Shift';
import { setCurrentUserInfo, getCurrentUserInfo } from '../Helpers/LocalStorage';
import { getDayNameFromDayNumber } from '../Helpers/Converter';
import { updateBusyTimesHelper } from '../Containers/CalendarScreen/CreateUnavailable/helpers';

export const ShiftStore = types
  .model('ShiftModel', {
    checkInLoading: types.optional(types.boolean, false),
    allUsers: types.optional(types.frozen(), []),
    allShifts: types.optional(types.frozen(), []),
    myShifts: types.optional(types.frozen(), []),
    lackShifts: types.optional(types.frozen(), []),
    currentShift: types.optional(types.frozen(), null),
    switchShifts: types.optional(types.frozen(), []),
    shiftHours: types.optional(types.frozen(), []),
    isNextWeek: types.optional(types.boolean, false),
    currentBusyTimes: types.optional(types.frozen(), []),
    currentCheckInStatus: types.optional(types.string, ''),
    errorNotify: types.optional(types.string, ''),
    successNotify: types.optional(types.string, ''),
    currentNotity: types.optional(types.number, 0),
  })
  .actions(self => {
    const getShifts = flow(function*() {
      self.isNextWeek = false;
      const res = yield fetchShifts();
      if (res.data) {
        const { result } = res.data;
        self.allShifts = result;
        const _allUsers = [] as any;
        result.map((e: any) => {
          if (e.data_item.length) {
            e.data_item.map((ee: any) => {
              ee.data_user.map((eee: any) => {
                const index = _allUsers.findIndex((u: any) => u.id === eee.user_id);
                if (index < 0 && eee.user_id) {
                  _allUsers.push({ id: eee.user_id, name: eee.full_name });
                }
              })
            })
          }
        });
        self.allUsers = _allUsers;
        return;
      }
      return self.errorNotify = 'Đã xảy ra lỗi!';
    });
    const getShiftsNextWeek = flow(function* (data) {
      const res = yield find(data);
      if (res.data) {
        const { result } = res.data;
        const list = [] as any;
        result.map((e: any) => {
          const index = list.findIndex((_e: any) => _e.day === e.day);
          if (e.data_user.length) {
            const data_item = [
              {
                start_time: e.start_time,
                end_time: e.end_time,
                data_user: e.data_user
              }
            ]
            if (index < 0) {
              list.push({
                day: e.date,
                name_day: e.name_day,
                data_item,
              })
            } else {
              list[index].data_item.push(data_item)
            }
          }
        })
        if (!result.length || !list.length) {
          self.successNotify = "Chưa có lịch tuần sau"
          return;
        }
        self.allShifts = [...list];
        alert(JSON.stringify(list[0].data_item));
        self.isNextWeek = true;
        return;
      }
      return self.errorNotify = 'Đã xảy ra lỗi!';
    });
    const getMyShifts = flow(function* () {
      const res = yield fetchMyShifts();
      if (res.data) {
        self.myShifts = res.data.result;
        return;
      }
      return null;
    });
    const getLackShifts = flow(function*() {
      const res = yield fetchLackShifts();
      if (res.data) {
        return self.lackShifts = res.data.result;
      }
      return self.errorNotify = 'Đã xảy ra lỗi!';
    });
    const getSwitchShifts = flow(function* () {
      const res = yield switches();
      if (res.data) {
        self.switchShifts = res.data.datatable;
      }
    });
    const setCurrentShift = (index: number) => {
      self.currentShift = index;
    };
    const getIPAddress = async () => {
      const _IP = await PublicIP();
      if (!_IP) {
        self.errorNotify = 'Không thể lấy được vị trí của bạn!';
        return;
      }
      return (_IP as any).replace(/\./g, '');
    };
    const doCheckIn = flow(function* (data) {
      try {
        self.checkInLoading = true;
        const IP = yield getIPAddress();
        const request = {
          ip_address: IP,
          user_id: data.userId,
          branch_id: data.branchId,
          token: data.token,
          checkinByCode: 0,
          location: '',
        }
        yield checkIn({ ip_address: IP });
        yield checkOut(request);
        const now = new Date().getTime();
        self.checkInLoading = false;
        self.currentNotity = now;
        self.currentCheckInStatus = 'CHECKOUT';
        self.successNotify = 'Check In Thành Công!';
        yield setCurrentUserInfo(data.userId, [
          { key: 'CURRENT_CHECKIN_CHECKOUT', value: 'CHECKOUT' }
        ]);
      } catch (error) {
        const now = new Date().getTime();
        self.currentNotity = now;
        self.errorNotify = 'Check In Thất Bại!';
      }
    });
    const doCheckOut = flow(function* (data: any) {
      self.checkInLoading = true;
      const IP = yield getIPAddress();
      const request = {
        ip_address: IP,
        user_id: data.userId,
        branch_id: data.branchId,
        token: data.token,
        checkinByCode: 0,
        location: '',
      }
      const res = yield checkOut(request);
      self.checkInLoading = false;
      const now = new Date().getTime();
      if (res.status === 200) {
        self.currentNotity = now;
        self.successNotify = 'Check Out Thành Công!';
        self.currentCheckInStatus = 'CHECKIN';
        yield setCurrentUserInfo(data.userId, [
          { key: 'CURRENT_CHECKIN_CHECKOUT', value: 'CHECKIN' }
        ]);
      } else {
        self.currentNotity = now;
        self.errorNotify = 'Check Out Thất Bại!';
      }
    });
    const getCurrentCheckInStatus = flow(function* (userId) {
      const data = yield getCurrentUserInfo(userId);
      if (!data['CURRENT_CHECKIN_CHECKOUT']) {
        self.currentCheckInStatus = 'CHECKIN';
      } else {
        self.currentCheckInStatus = data['CURRENT_CHECKIN_CHECKOUT'];
      }
    });
    const getRemind = flow(function* () {
      const res = yield remind();
      if (res.data) {
        return res.data.result;
      }
      return [];
    });
    const clearNotify = () => {
      self.currentNotity = 0;
      self.errorNotify = '';
      self.successNotify = '';
    };
    return {
      getShifts,
      getShiftsNextWeek,
      getMyShifts,
      getLackShifts,
      getRemind,
      getSwitchShifts,
      setCurrentShift,
      doCheckIn,
      doCheckOut,
      getCurrentCheckInStatus,
      clearNotify,
    };
  });

export type ShiftModel = Instance<typeof ShiftStore>;