import { types, Instance, flow } from 'mobx-state-tree';
import { fetchPosts } from '../APIs/Post';

export const PostStore = types
  .model('PostModel', {
    posts: types.optional(types.frozen(), []),
    currentPost: types.optional(types.frozen(), null),
    error: types.optional(types.string, ''),
  })
  .actions(self => {
    const getPosts = flow(function*() {
      const res = yield fetchPosts();
      if (res.data) {
        self.posts = res.data.result;
      }
    });
    const setCurrentPost = function(index: number) {
      self.currentPost = self.posts[index];
    };
    return {
      getPosts,
      setCurrentPost,
    }
  });

export type PostModel = Instance<typeof PostStore>;