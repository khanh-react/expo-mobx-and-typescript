import { types, Instance, flow } from 'mobx-state-tree';
import { branchCalendar, busyTimes, addBusyTimes, myCalendar, calendarNextWeek } from '../APIs/Calendar';
import dayItem from '../Containers/CalendarScreen/Header/Timeline/DayItem';
import { getCurrentMondayFromToday, getFullDayNameFromShortDayName, _shortDayName } from '../Helpers/Converter';

export const CalendarStore = types
  .model('CalendarModel', {
    currentDay: types.optional(types.number, new Date().getDay()),
    currentMonth: types.optional(types.number, new Date().getMonth() + 1),
    currentYear: types.optional(types.number, new Date().getFullYear()),
    currentMonday: types.optional(types.number, getCurrentMondayFromToday()),
    currentStartingDate: types.optional(types.number, new Date().getTime()),
    isCreating: types.optional(types.boolean, false),
    isNextWeek: types.optional(types.boolean, false),
    workHours: types.optional(types.frozen(), []),
    myCalendar: types.optional(types.frozen(), []),
    errorNotify: types.optional(types.string, ''),
    successNotify: types.optional(types.string, ''),
    currentNotity: types.optional(types.number, 0),
  })
  .actions((self) => ({
    updateCurrentDay (day: number) { self.currentDay = day },
    updateCurrentMonday (day: number) { self.currentMonday = day },
    updateCurrentMonth (month: number) { self.currentMonth = month },
    updateCurrentYear (year: number) { self.currentYear = year },
    updateCurrentStartingDate (date: number) { self.currentStartingDate = date },
    updateCreatingStatus (isCreated: boolean) { self.isCreating = isCreated },
    getBranchCalendar () {
      const calendars = flow(function* () {
        const res = yield branchCalendar();
        if (res && res.data) {
          const workHours = res.data;
          const busyTimesRes = yield busyTimes();
          if (busyTimesRes.data && busyTimesRes.data.result) {
            busyTimesRes.data.result.map((item: any) => {
              item.data_item.map((time: any) => {
                const index = workHours[_shortDayName(item.name_day)].findIndex((e: any) => {
                  const startTime = e.start_time.slice(0,5);
                  const endTime = e.end_time.slice(0,5);
                  if (startTime === time.start && endTime === time.end) {
                    return true;
                  }
                  return false;
                });
                if (index >= 0) {
                  workHours[_shortDayName(item.name_day)][index]['isBusy'] = true;
                  workHours[_shortDayName(item.name_day)][index]['date'] = item.day;
                }
              })
            })
          }
          self.workHours = workHours;
        }
      })
      calendars();
    },
    getMyCalendar () {
      const fetchFlow = flow(function* () {
        const res = yield myCalendar();
        if (res.data) {
          self.myCalendar = res.data.result;
          self.isNextWeek = false;
        }
      });
      fetchFlow();
    },
    getCalendarOnNextWeek (params: any) {
      const fetchFlow = flow(function* () {
        const res = yield calendarNextWeek(params);
        if (res.data) {
          const { result } = res.data;
          const list = [] as any;
          result.map((e: any) => {
            const index = list.findIndex((_e: any) => _e.day === e.day);
            if (e.data_user.length) {
              const data_item = [
                {
                  start_time: e.start_time,
                  end_time: e.end_time,
                  data_user: e.data_user
                }
              ]
              if (index < 0) {
                list.push({
                  day: e.date,
                  name_day: e.name_day,
                  data_item,
                })
              } else {
                list[index].data_item.push(data_item)
              }
            }
          })
          if (!result.length || !list.length) {
            self.successNotify = "Chưa có lịch tuần sau"
            return;
          }
          self.myCalendar = list;
          self.isNextWeek = true;
          return;
        }
        return self.errorNotify = 'Đã xảy ra lỗi!';
      });
      fetchFlow();
    },
    updateBusyTime (payload: any) {
      const busyTimes = self.workHours;
      const index = busyTimes[payload.dayName].findIndex((e: any) => {
        const _startTime = e.start_time.slice(0,5);
        const _endTime = e.end_time.slice(0,5);
        if (payload.startTime === _startTime && payload.endTime === _endTime) {
          return true;
        }
        return false;
      });
      const { isBusy = false } = busyTimes[payload.dayName][index];
      busyTimes[payload.dayName][index]['isBusy'] = !isBusy;
      busyTimes[payload.dayName][index]['date'] = payload.date;
      self.workHours = { ...busyTimes };
    },
    onAddBusyTime () {
      const flowAddBusyTimes = flow(function*(data) {
        const res = yield addBusyTimes(data);
        if (res && res.data) {
          self.successNotify = 'Cập nhật thành công!'
        } else {
          self.errorNotify = 'Đã xảy ra lỗi vui lòng thử lại!';
        }
      });
      const list = [] as any;
      Object.keys(self.workHours).map(key => {
        self.workHours[key].map((hour: any) => {
          if (hour.isBusy) {
            list.push({
              start_time: hour.date + ' ' + hour.start_time,
              end_time: hour.date + ' ' + hour.end_time,
              note: '',
            })
          }
        })
      });
      flowAddBusyTimes(list);
    },
    clearNotify () {
      self.currentNotity = 0;
      self.errorNotify = '';
      self.successNotify = '';
    }
  }));

export type CalendarModel = Instance<typeof CalendarStore>;
