import { types, Instance, flow } from 'mobx-state-tree';
import { convertInitInput } from '../Helpers/Converter';
import { logout } from '../Helpers/LocalStorage';
import { checkInput, login, updateAvatar } from '../APIs/Auth';
import {
  getCurrentPassword,
  getCurrentUserID,
  getCurrentUserInfo,
  setCurrentPassword,
  setToken,
  setProfileImage,
  setCurrentUserInfo,
  setCurrentUserID,
} from '../Helpers/LocalStorage';

export const AuthStore = types
  .model('AuthModel', {
    id: types.optional(types.number, 0),
    token: types.optional(types.string, ''),
    initialed: types.optional(types.string, ''),
    today: types.optional(types.string, new Date().toString()),
    initInput: types.optional(types.string, ''),
    fullname: types.optional(types.string, ''),
    email: types.optional(types.string, ''),
    phone: types.optional(types.string, ''),
    password: types.optional(types.string, ''),
    branchId: types.optional(types.number, 0),
    branchName: types.optional(types.string, ''),
    branchAddress: types.optional(types.string, ''),
    currentPassword: types.optional(types.string, ''),
    avatar: types.optional(types.string, ''),
    isLoading: types.optional(types.boolean, false),
  })
  .actions((self) => {
    const setInitialized = () => {
      self.initialed = new Date().toString();
    };
    const onLoading = (isLoading = true) => self.isLoading = isLoading;
    const offLoading = () => self.isLoading = false;
    const doCheckInput = flow(function*(input) {
      self.isLoading = true;
      const converted = convertInitInput(input);
      const res = yield checkInput(input);
      self.isLoading = false;
      if (converted && converted.by === 'email') {
        self.email = converted.value;
        self.phone = '';
      }
      if (converted && converted.by === 'phone') {
        self.email = '';
        self.phone = converted.value;
      }
      if (res.data) {
        self.fullname = res.data['UserName'];
        return res.data['UserName'];
      } else {
        return '';
      }
    });
    const doLogin = flow(function* () {
      self.isLoading = true;
      const res = yield login(self.email || self.phone, self.password);
      self.isLoading = false;
      const { data } = res;
      if (data) {
        const infos = [
          { key: 'id', value: data['user_id'].toString() },
          { key: 'fullName', value: data['full_name'] },
          { key: 'avatar', value: data['avatar'] },
          { key: 'email', value: data['email'] },
          { key: 'phone', value: data['sdt'] },
          { key: 'branchId', value: data['branch_name'][0]['id'] },
          { key: 'branchName', value: data['branch_name'][0].name.split('-')[0] },
          { key: 'branchAddress', value: data['branch_name'][0].name.split('-')[1] },
          { key: 'token', value: data['token'] },
        ];
        self.id = parseFloat(data['user_id']);
        self.token = data['token'];
        self.fullname = data['full_name'];
        self.avatar = data['avatar'] || '';
        self.branchId = data['branch_name'][0].id;
        self.branchName = data['branch_name'][0].name.split('-')[0];
        self.branchAddress = data['branch_name'][0].name.split('-')[1];
        setCurrentUserInfo(data['user_id'].toString(), infos);
        setCurrentUserID(data['user_id'].toString());
        setToken(data['token']);
        if (self.avatar) {
          return 'SignInAfter';
        } else {
          return 'SignInSuccess';
        }
      } else {
        alert('Sai mật khẩu, vui lòng thử lại!');
      }
    });
    const doLogout = flow(function* () {
      self.id = 0;
      self.avatar = "";
      self.fullname = "";
      self.email = "";
      self.initInput = "";
      yield logout();
    });
    const updateEmail = (val: string) => self.email = val;
    const updatePhone = (val: string) => self.phone = val;
    const updatePassword = (val: string) => self.password = val;
    const updateFullname = (val: string) => self.fullname = val;
    const updateUserId = (val: string) => self.id = parseFloat(val);
    const doUpdateAvatar = flow(function* (link: string) {
      self.avatar = link;
      yield updateAvatar(link);
      yield setCurrentUserInfo(self.id.toString(), [{ key: 'avatar', value: ''}]);
      yield setCurrentUserInfo(self.id.toString(), [{ key: 'avatar', value: link}]);
    });
    const updateCurrentPassword = flow(function* (val?: string) {
      if (val) {
        yield setCurrentPassword(val);
        return val;
      } else {
        const currentPass = getCurrentPassword();
        return currentPass;
      }
    });
    const getCurrentUser = flow(function* () {
      const id = yield getCurrentUserID();
      if (!id) return;
      const data = yield getCurrentUserInfo(id);
      self.id = parseFloat(id);
      self.fullname = data.fullName;
      self.email = data.email;
      self.phone = data.phone;
      self.branchId = data.branchId;
      self.branchName = data.branchName;
      self.branchAddress = data.branchAddress;
      self.token = data.token;
      if (data.avatar) {
        self.avatar = data.avatar;
        return 'SignInAfter';
      } else {
        return 'Profile';
      }
    });
    return {
      setInitialized, onLoading, offLoading, doCheckInput, doLogin, doUpdateAvatar,
      updateEmail, updatePhone, updatePassword, updateFullname, updateCurrentPassword,
      updateUserId, doLogout,
      getCurrentUser,
    };
  });

export type InitModel = Instance<typeof AuthStore>;
