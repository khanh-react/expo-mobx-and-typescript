export type UserInfo = {
  phone?: number;
  email?: string;
  password?: string;
  fullname?: string;
};