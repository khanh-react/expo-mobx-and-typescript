import * as React from 'react';
// import makeInspectable  from 'mobx-devtools-mst';
import { useLocalObservable } from 'mobx-react-lite';
import { RootStoreType, getDetaultStore } from './index';

export const storeContext = React.createContext<RootStoreType | null>(null);

export const StoreProvider: React.FC = ({ children }: any) => {
  const store = useLocalObservable(getDetaultStore);
  //   makeInspectable(store);
  return (
    <storeContext.Provider value={store}>
      {children}
    </storeContext.Provider>
  );
};

export default StoreProvider;
