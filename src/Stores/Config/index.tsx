import { types, Instance } from 'mobx-state-tree';

import { AuthStore } from '../Auth';
import { CalendarStore } from '../Calendar';
import { ToggleStore } from '../Notification';
import { PostStore } from '../Post';
import { ShiftStore } from '../Shift';

const RootStore = types.model('RootStores', {
  auth: types.optional(AuthStore, {}),
  calendar: types.optional(CalendarStore, {}),
  toggle: types.optional(ToggleStore, {}),
  post: types.optional(PostStore, {}),
  shift: types.optional(ShiftStore, {}),
});

export type IRootStoreModel = Instance<typeof RootStore>;
export type RootStoreType = typeof RootStore.Type;

export const getDetaultStore = () => RootStore.create({});

export default RootStore;
