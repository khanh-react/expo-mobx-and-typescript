import { types, Instance, flow } from 'mobx-state-tree';

export type Notification = { status: number; message: string; };

export const ToggleStore = types
    .model('ToggleModel', {
        isVisiting: types.optional(types.boolean, false),
        isPopupShown: types.optional(types.boolean, false),
        notificationStatus: types.optional(types.number, 0),
        notificationMessage: types.optional(types.string, ''),
        currentOpenAt: types.optional(types.number, 0),
    })
    .actions(self => ({
        updateVisiting(isVisiting = true) { self.isVisiting = isVisiting; },
        updatePopup(isShow = true) { self.isPopupShown = isShow; },
        pushNotification({ status, message }: Notification) {
            self.notificationStatus = status;
            self.notificationMessage = message;
            self.currentOpenAt = new Date().getTime();
        },
        clearNotification() {
            self.notificationStatus = 0;
            self.notificationMessage = '';
        }
    }))

export type ToggleModel = Instance<typeof ToggleStore>;
