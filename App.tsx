import React from 'react';
import Container from './src/Containers';
import StoreProvider from './src/Stores/Config/createStores';
import { LogBox } from 'react-native';
import * as Firebase from 'firebase';
import { firebaseConfig } from './src/Helpers/FirebaseService';

LogBox.ignoreLogs([
    "source.uri should not be an empty string",
    "[mobx-react-lite] 'useObserver(fn)' is deprecated. Use `<Observer>{fn}</Observer>` instead, or wrap the entire component in `observer`",
    "You need to specify name or key when calling navigate with an object as the argument"
]); // Ignore log notification by message

// Initialize Firebase
try {
    Firebase.initializeApp(firebaseConfig)
} catch (error) {
    console.log('Firebase has initialized already!')
}

export const App = () => <StoreProvider><Container /></StoreProvider>;

export default App;
